osu!fpga
========

An [osu!](https://osu.ppy.sh) like rhythm game on DE2-115 FPGA.

*Fang Lu, Xutao Jumbo Jiang*

Overview
--------

This project reproduces the major gameplay experience of the desktop rhythm game [osu!](https://osu.ppy.sh) on the DE2-115 FPGA development board. The user plays the game using a pointing device such as a USB mouse and a keyboard device such as a PS/2 keyboard. The graphics will be displayed using a VGA-capable monitor. It supports all the fundamental gameplay elements, including hit-circles, sliders, spinners and judgement system.

Features
--------

**Game Functionalities**

* [ ] **Song selection view**
* [ ] **Gameplay view**
* [ ] **Score report view**
* [ ] **Beatmap parsing**
* [ ] Animated hit objects
* [ ] Song preview
* [ ] Scoreboard
* [ ] Pause
* [ ] Mods
* [ ] Settings

**Input devices**

* [x] **USB Mouse driver**
* [x] **PS/2 Keyboard driver**
* [ ] ~~Wacom tablet driver~~
* [ ] ~~USB hub support~~

**Graphics**

* [x] **VGA output driver**
* [x] **Graphics service & task distributor**
* [ ] **Bitmap renderer**
	* [ ] **Bitmap resizable copy**
	* [ ] **Layer blending**
* [ ] **Vector renderer**
	* [ ] **Line**
	* [x] ~~**Elliptic Curve**~~
	* [x] ~~**Bezier Curve**~~
	* [x] ~~SVG paths~~
* [ ] **Font renderer**
	* [ ] **Fixed-width bitmap font**
	* [ ] Variable-width bitmap font
	* [ ] ~~Variable-width vector font~~
* [ ] Effects renderer
	* [ ] Particles
	* [ ] Pointer trails

**Audio**

* [x] **Audio driver**
* [ ] **Multitrack mixdown**

**Miscellaneous/Utilities**

* [ ] **Timing**
* [x] ~~**PNG Parsing**~~
* [x] ~~**MP3 Parsing**~~
* [x] ~~Zip parsing~~

