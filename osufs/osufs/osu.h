//
//  osu.h
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef osu_h
#define osu_h


#include <stdio.h>
#include <dirent.h>
#include <map>
#include <string>
#include "osufs.h"

//#include "osu_object_parser.h"

typedef struct osu_dir_t {
	char dirname[512];
	DIR *dir;
	std::map< std::string, std::pair<uint32_t,uint32_t> > fs;
} osu_dir;

void osu_install_skin(const char *dirname, mdev *dev, osu_meta *meta);

void osu_install_font(const char *filename, mdev *dev, osu_meta *meta);

osu_song osu_read_osu(const char *filename, mdev *dev, osu_meta *meta,
					  osu_dir *cont);
osu_dir osu_open_dir(const char *dirname);
const char *osu_read_dir(osu_dir dir);

typedef int (*osu_ini_proc)(char*, osu_song*, mdev*, osu_meta*, osu_dir*);

int osu_parse_general(char *line, osu_song *song,
					  mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_editor(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_metadata(char *line, osu_song *song,
					   mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_difficulty(char *line, osu_song *song,
						 mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_events(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_timing(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_colors(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont);
int osu_parse_hitobjs(char *line, osu_song *song,
					  mdev *dev, osu_meta *meta, osu_dir *cont);

char *osu_util_str_split(char *line, char delim);
void osu_check_beatmap_sanity(osu_song *bm);

#endif /* osu_h */
