//
//  osu.cpp
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "osu.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>

#include "imgenc.h"
#include "wavenc.h"
#include "fontenc.h"

#include "osu_object_parser.h"

#define ISHOT(ptr) (((uint8_t*)ptr)[0]==0xcc)

using namespace std;

void osu_install_skin(const char *dirname, mdev *dev, osu_meta *meta) {

#define SKIN_IMPORT_AUDIO(entry, filename) \
strcpy(fname, filename);\
meta-> entry ## _begin = addr;\
addr = wav_write(cbuf, dev, addr);\
meta-> entry ## _len = addr - meta-> entry ## _begin;\

#define SKIN_IMPORT_IMAGE(entry, filename, w, h)\
strcpy(fname, filename);\
meta-> entry = addr;\
meta-> entry ## _w = w;\
meta-> entry ## _h = h;\
addr = img_write_rgb16(cbuf, dev, addr, &(meta-> entry ## _w), &(meta-> entry ## _h));
	
#define SKIN_IMPORT_IMAGE_DITHER64(entry, filename, w, h)\
strcpy(fname, filename);\
meta-> entry = addr;\
meta-> entry ## _w = w;\
meta-> entry ## _h = h;\
addr = img_write_dither64(cbuf, dev, addr, &(meta-> entry ## _w), &(meta-> entry ## _h));
	
	char cbuf[1024], *fname;
	uint32_t addr = meta->available_block;

	strcpy(cbuf, dirname);
	fname = cbuf + strlen(cbuf);
	assert(*fname == '\0');
	*fname = '/';
	fname++;

	// Audio
	
	SKIN_IMPORT_AUDIO(applause, "applause.mp3")
	SKIN_IMPORT_AUDIO(failsnd, "failsound.mp3")
	SKIN_IMPORT_AUDIO(combobreak, "combobreak.mp3")
	SKIN_IMPORT_AUDIO(spinnerspin, "spinnerspin.wav")
	SKIN_IMPORT_AUDIO(spinnerbonus, "spinnerbonus.wav")
	SKIN_IMPORT_AUDIO(menuclick, "menuclick.wav")
	SKIN_IMPORT_AUDIO(menuhit, "menuhit.wav")
	SKIN_IMPORT_AUDIO(menuback, "menuback.wav")

	SKIN_IMPORT_AUDIO(drum_hitclap, "drum-hitclap.wav")
	SKIN_IMPORT_AUDIO(drum_hitfinish, "drum-hitfinish.wav")
	SKIN_IMPORT_AUDIO(drum_hitnormal, "drum-hitnormal.wav")
	SKIN_IMPORT_AUDIO(drum_hitwhistle, "drum-hitwhistle.wav")
	SKIN_IMPORT_AUDIO(drum_sliderslide, "drum-sliderslide.wav")
	SKIN_IMPORT_AUDIO(drum_slidertick, "drum-slidertick.wav")
	SKIN_IMPORT_AUDIO(drum_sliderwhistle, "drum-sliderwhistle.wav")

	SKIN_IMPORT_AUDIO(normal_hitclap, "normal-hitclap.wav")
	SKIN_IMPORT_AUDIO(normal_hitfinish, "normal-hitfinish.wav")
	SKIN_IMPORT_AUDIO(normal_hitnormal, "normal-hitnormal.wav")
	SKIN_IMPORT_AUDIO(normal_hitwhistle, "normal-hitwhistle.wav")
	SKIN_IMPORT_AUDIO(normal_sliderslide, "normal-sliderslide.wav")
	SKIN_IMPORT_AUDIO(normal_slidertick, "normal-slidertick.wav")
	SKIN_IMPORT_AUDIO(normal_sliderwhistle, "normal-sliderwhistle.wav")

	SKIN_IMPORT_AUDIO(soft_hitclap, "soft-hitclap.wav")
	SKIN_IMPORT_AUDIO(soft_hitfinish, "soft-hitfinish.wav")
	SKIN_IMPORT_AUDIO(soft_hitnormal, "soft-hitnormal.wav")
	SKIN_IMPORT_AUDIO(soft_hitwhistle, "soft-hitwhistle.wav")
	SKIN_IMPORT_AUDIO(soft_sliderslide, "soft-sliderslide.wav")
	SKIN_IMPORT_AUDIO(soft_slidertick, "soft-slidertick.wav")
	SKIN_IMPORT_AUDIO(soft_sliderwhistle, "soft-sliderwhistle.wav")

	// Image
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Waddress-of-packed-member"

	SKIN_IMPORT_IMAGE(hit_0, "hit0.png", 48, -1)
	SKIN_IMPORT_IMAGE(hit_100, "hit100.png", 48, -1)
	SKIN_IMPORT_IMAGE(hit_300, "hit300.png", 48, -1)
	SKIN_IMPORT_IMAGE(hit_100k, "hit100k.png", 48, -1)
	SKIN_IMPORT_IMAGE(hit_300k, "hit300k.png", 48, -1)
	SKIN_IMPORT_IMAGE(hit_300g, "hit300g.png", 48, -1)

	SKIN_IMPORT_IMAGE(pause_back, "pause-back.png", 235, -1)
	SKIN_IMPORT_IMAGE(pause_continue, "pause-continue.png", 235, -1)
	SKIN_IMPORT_IMAGE(pause_retry, "pause-retry.png", 235, -1)

	SKIN_IMPORT_IMAGE_DITHER64(ranking_ss, "ranking-X.png", 160, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_s, "ranking-S.png", 160, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_a, "ranking-A.png", 160, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_b, "ranking-B.png", 160, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_c, "ranking-C.png", 160, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_d, "ranking-D.png", 160, -1)
	
	SKIN_IMPORT_IMAGE_DITHER64(ranking_title, "ranking-title.png", 240, -1)
	SKIN_IMPORT_IMAGE_DITHER64(ranking_panel, "ranking-panel.png", 378, -1)
	
	meta->selection_w = 48;
	meta->selection_h = 56;
	meta->selection_mod = addr;
	strcpy(fname, "selection-mods.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->selection_mod_over = addr;
	strcpy(fname, "selection-mods-over.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->selection_random = addr;
	strcpy(fname, "selection-random.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->selection_random_over = addr;
	strcpy(fname, "selection-random-over.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	assert(meta->selection_w == 48 && meta->selection_h == 56 && "Resize failed");

	meta->mod_w = 40;
	meta->mod_h = 40;
	meta->mod_auto = addr;
	strcpy(fname, "selection-mod-autoplay.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->mod_nofail = addr;
	strcpy(fname, "selection-mod-nofail.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->mod_easy = addr;
	strcpy(fname, "selection-mod-easy.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->mod_hardrock = addr;
	strcpy(fname, "selection-mod-hardrock.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->mod_hidden = addr;
	strcpy(fname, "selection-mod-hidden.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	meta->mod_suddendeath = addr;
	strcpy(fname, "selection-mod-suddendeath.png");
	addr = img_write_rgb16(cbuf, dev, addr, &(meta->selection_w), &(meta->selection_h));
	assert(meta->mod_w == 40 && meta->mod_h == 40 && "Resize failed");

	#pragma clang diagnostic pop
	
	// Score fonts
	addr = font_score_write(dirname, dev, addr);
	meta->font_score = addr-1;
	
	// Update meta addr
	meta->available_block = addr;
}

void osu_install_font(const char *filename, mdev *dev, osu_meta *meta) {
	uint32_t addr = meta->available_block;
	addr = font_write(filename, 12, dev, addr);
	meta->font_12 = addr-1;
	addr = font_write(filename, 16, dev, addr);
	meta->font_16 = addr-1;
	addr = font_write(filename, 24, dev, addr);
	meta->font_24 = addr-1;
	meta->available_block = addr;
}

osu_dir osu_open_dir(const char *dirname) {
	osu_dir ret;
	ret.dir = opendir(dirname);
	if (!ret.dir) {
		perror("Failed to open directory");
	}
	strncpy(ret.dirname, dirname, 511);
	return ret;
}

const char *osu_read_dir(osu_dir dir) {
	if (!dir.dir)
		return NULL;
	struct dirent *dp;
	do {
		dp = readdir(dir.dir);
		if (dp == NULL) {
			// End of Directory
			closedir(dir.dir);
			return NULL;
		}
	} while (strcasecmp(dp->d_name + strlen(dp->d_name)-4, ".osu") != 0);
	return dp->d_name;
}

osu_song osu_read_osu(const char *filename, mdev *dev, osu_meta *meta,
					  osu_dir *cont) {
	osu_song ret;
	char line[1024];

	memset(&ret, 0xcc, sizeof(ret));
	strncpy(ret.magic, "DEAD", 4);
	ret.combo_colors_start = 0;
	ret.storyboard = NULL;

	sprintf(line, "%s/%s", cont->dirname, filename);

	FILE *fp = fopen(line, "r");
	if (!fp) {
		perror("Failed to open osu file");
		return ret;
	}

	// Parse osu file
	osu_ini_proc proc = &osu_parse_general;

	while (fgets(line, 1023, fp)) {
		// Trim trailing whitespace
		char *endc = line + strlen(line) - 1;
		while (isspace(*endc))
			endc--;
		endc[1] = '\0';

		if (line[0] == '\0') {
			goto osu_parse_end;
		}
		if (strncmp(line, "osu file format", 15) == 0) {
			printf("%s\n", line);
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[General]", 9) == 0) {
			proc = &osu_parse_general;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[Editor]", 8) == 0) {
			proc = &osu_parse_editor;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[Metadata]", 10) == 0) {
			proc = &osu_parse_metadata;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[Difficulty]", 12) == 0) {
			proc = &osu_parse_difficulty;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[Events]", 8) == 0) {
			proc = &osu_parse_events;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[TimingPoints]", 14) == 0) {
			proc = &osu_parse_timing;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[Colours]", 9) == 0) {
			proc = &osu_parse_colors;
			goto osu_parse_end;
		}
		if (strncasecmp(line, "[HitObjects]", 12) == 0) {
			proc = &osu_parse_hitobjs;
			goto osu_parse_end;
		}

		// Send content to current section parser
		if ((*proc)(line, &ret, dev, meta, cont) != 0) {
			fprintf(stderr, "Parsing terminated due to error\n");
			return ret;
		}
	osu_parse_end:
		void(0);
	}

	uint32_t addr = meta->available_block;
	uint8_t *sbbuf = (uint8_t *) ret.storyboard;
	ret.beatmap_begin = addr;
	ret.beatmap_len = 4096;
	for (int i=0; i<4096; i++) {
		blkio_write(dev, addr++, sbbuf+512*i);
	}
	meta->available_block = addr;
	
	strncpy(ret.magic, "BEAT", 4);

	printf("Parsing done. Performing sanity check...\n");
	osu_check_beatmap_sanity(&ret);

	return ret;
}

int osu_parse_general(char *line, osu_song *song,
					  mdev *dev, osu_meta *meta, osu_dir *cont) {
	char *val = osu_util_str_split(line, ':'), *key = line;
	if (!val) {
		fprintf(stderr, "General Section: field has no value\n");
		return 1;
	}
	// AudioFilename (String) specifies the location of the audio file relative to the current folder.
	if (strcasecmp(key, "AudioFilename") == 0) {
		if (cont->fs.find(val) == cont->fs.end()) {
			printf("Allocating storage for audio %s\n", val);
			uint32_t addr = meta->available_block;
			song->audio_begin = addr;

			char path[1024];
			sprintf(path, "%s/%s", cont->dirname, val);
			addr = wav_write(path, dev, addr);

			song->audio_len = addr - song->audio_begin;
			meta->available_block = addr;
			cont->fs[val] = make_pair(song->audio_begin, song->audio_len);
		} else {
			auto finfo = cont->fs[val];
			printf("Reusing storage (%x) for audio %s\n", finfo.first, val);
			song->audio_begin = finfo.first;
			song->audio_len = finfo.second;
		}
		return 0;
	}

	// AudioLeadIn (Integer, milliseconds) is the amount of time added before the audio file begins playing. Useful for audio files that begin immediately.
	if (strcasecmp(key, "AudioLeadIn") == 0) {
		int leadin;
		if (sscanf(val, "%d", &leadin) != 1) {
			fprintf(stderr, "Could not parse AudioLeadIn string `%s'\n", val);
			return 1;
		}
		song->audio_leadin = leadin;
		return 0;
	}

	// PreviewTime (Integer, milliseconds) defines when the audio file should begin playing when selected in the song selection menu.
	if (strcasecmp(key, "PreviewTime") == 0) {
		uint32_t msprev;
		if (sscanf(val, "%u", &msprev) != 1) {
			fprintf(stderr, "Could not parse PreviewTime string `%s'\n", val);
			return 1;
		}
		uint32_t blkprev = msprev * 44100*4 / 1000 / 512;
		song->preview_blk_offset = blkprev;
		return 0;
	}

	// StackLeniency (Decimal) is how often closely placed hit objects will be stacked together.
	if (strcasecmp(key, "StackLeniency") == 0) {
		float slen;
		if (sscanf(val, "%f", &slen) != 1) {
			fprintf(stderr, "Could not parse StackLeniency string `%s'\n", val);
			return 1;
		}
		song->stack_leniency = round(slen * 0x100);
		return 0;
	}

	return 0;
}

int osu_parse_editor(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont) {
	// Editor configs are completely ignored.
	return 0;
}

int osu_parse_metadata(char *line, osu_song *song,
					   mdev *dev, osu_meta *meta, osu_dir *cont) {
	char *val = osu_util_str_split(line, ':'), *key = line;
	if (!val) {
		fprintf(stderr, "Metadata Section: field has no value\n");
		return 1;
	}

	if (strcasecmp(key, "Title") == 0) {
		strncpy(song->title, val, 63);
		return 0;
	}

	if (strcasecmp(key, "Artist") == 0) {
		strncpy(song->artist, val, 63);
		return 0;
	}

	if (strcasecmp(key, "Creator") == 0) {
		strncpy(song->creator, val, 31);
		return 0;
	}

	if (strcasecmp(key, "Version") == 0) {
		strncpy(song->version, val, 31);
		return 0;
	}

	if (strcasecmp(key, "BeatmapID") == 0) {
		uint32_t beatmap_id;
		if (sscanf(val, "%uld", &beatmap_id) != 1) {
			fprintf(stderr, "Could not parse BeatmapID string `%s'\n", val);
			return 1;
		}
		song->beatmap_id = beatmap_id;
		return 0;
	}

	if (strcasecmp(key, "BeatmapSetID") == 0) {
		uint32_t beatmapset_id;
		if (sscanf(val, "%uld", &beatmapset_id) != 1) {
			fprintf(stderr, "Could not parse BeatmapSetID string `%s'\n", val);
			return 1;
		}
		song->beatmap_setid = beatmapset_id;
		return 0;
	}

	return 0;
}

int osu_parse_difficulty(char *line, osu_song *song,
						 mdev *dev, osu_meta *meta, osu_dir *cont) {
	char *val = osu_util_str_split(line, ':'), *key = line;
	if (!val) {
		fprintf(stderr, "Difficulty Section: field has no value\n");
		return 1;
	}

	// HPDrainRate (Decimal) specifies the HP Drain difficulty.
	if (strcasecmp(key, "HPDrainRate") == 0) {
		float drain;
		if (sscanf(val, "%f", &drain) != 1) {
			fprintf(stderr, "Could not parse HPDrainRate string `%s'\n", val);
			return 1;
		}
		song->hp_drain = round(drain * 0x100);
		return 0;
	}

	// CircleSize (Decimal) specifies the size of hit object circles.
	if (strcasecmp(key, "CircleSize") == 0) {
		float circlesize;
		if (sscanf(val, "%f", &circlesize) != 1) {
			fprintf(stderr, "Could not parse CircleSize string `%s'\n", val);
			return 1;
		}
		song->circle_size = round(circlesize * 0x100);
		return 0;
	}

	// OverallDifficulty (Decimal) specifies the amount of time allowed to click
	// a hit object on time.
	if (strcasecmp(key, "OverallDifficulty") == 0) {
		float diff;
		if (sscanf(val, "%f", &diff) != 1) {
			fprintf(stderr, "Could not parse OverallDifficulty string `%s'\n", val);
			return 1;
		}
		song->difficulty = round(diff * 0x100);
		return 0;
	}

	// ApproachRate (Decimal) specifies the amount of time taken for the
	// approach circle and hit object to appear.
	if (strcasecmp(key, "ApproachRate") == 0) {
		float approach;
		if (sscanf(val, "%f", &approach) != 1) {
			fprintf(stderr, "Could not parse ApproachRate string `%s'\n", val);
			return 1;
		}
		song->approach_rate = round(approach * 0x100);
		return 0;
	}

	// SliderMultiplier (Decimal) specifies a multiplier for the slider velocity
	// Default value is 1.4
	if (strcasecmp(key, "SliderMultiplier") == 0) {
		float slider_mult;
		if (sscanf(val, "%f", &slider_mult) != 1) {
			fprintf(stderr, "Could not parse SliderMultiplier string `%s'\n", val);
			return 1;
		}
		song->slider_mult = round(slider_mult * 0x100);
		return 0;
	}

	// SliderTickRate (Decimal) specifies how often slider ticks appear
	// Default value is 1
	if (strcasecmp(key, "SliderTickRate") == 0) {
		float slider_tick;
		if (sscanf(val, "%f", &slider_tick) != 1) {
			fprintf(stderr, "Could not parse SliderTickRate string `%s'\n", val);
			return 1;
		}
		song->slider_tick = round(slider_tick * 0x100);
		return 0;
	}

	return 0;
}

int osu_parse_events(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont) {
	// Events section is in CSV format
	switch (line[0]) {
		case '0': {
			// Background image
			if (strncmp(line, "0,0,\"", 5) != 0) {
				fprintf(stderr, "Warning: bad background image: %s\n", line);
				return 1;
			}
			char *fname = line + 5, *c = fname;
			for (; *c; c++) {
				if (*c == '"')
					break;
			}
			if (*c != '"') {
				fprintf(stderr, "Warning: background filename not quoted: %s\n", line);
				return 1;
			}
			*c = '\0';
			if (cont->fs.find(fname) == cont->fs.end()) {
				printf("Allocating storage for image %s\n", fname);
				uint32_t addr = meta->available_block;
				song->cover_begin = addr;

				char path[1024];
				sprintf(path, "%s/%s", cont->dirname, fname);
				addr = img_write_dither128(path, dev, addr);

				song->cover_len = addr - song->cover_begin;
				meta->available_block = addr;
				cont->fs[fname] = make_pair(song->cover_begin, song->cover_len);
			} else {
				auto finfo = cont->fs[fname];
				printf("Reusing storage (%x) for image %s\n", finfo.first, fname);
				song->cover_begin = finfo.first;
				song->cover_len = finfo.second;
			}
			return 0;
		}

		case '2': {
			// Break
			return 0;
		}
	}
	return 0;
}

int osu_parse_timing(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont) {
	return 0;
}

int osu_parse_colors(char *line, osu_song *song,
					 mdev *dev, osu_meta *meta, osu_dir *cont) {
	char *val = osu_util_str_split(line, ':'), *key = line;
	if (!val) {
		fprintf(stderr, "ComboColours Section: field has no value\n");
		return 1;
	}

	int r, g, b;
	
	if (strncasecmp(key, "SliderBody", 10) == 0) {
		if (sscanf(val, "%d,%d,%d", &r, &g, &b) != 3) {
			fprintf(stderr, "Warning: bad SliderBody color: %s\n", val);
			return 1;
		}
		song->combo_slider_color[0] = r;
		song->combo_slider_color[1] = g;
		song->combo_slider_color[2] = b;
		return 0;
	}
	
	if (strncasecmp(key, "Combo", 5) != 0) {
		return 0;
	}

	int n = key[5] - '1';
	if (sscanf(val, "%d,%d,%d", &r, &g, &b) != 3) {
		fprintf(stderr, "Warning: bad combo color: %s\n", val);
		return 1;
	}
	song->combo_colors[n][0] = r;
	song->combo_colors[n][1] = g;
	song->combo_colors[n][2] = b;

	song->combo_colors_count = n + 1;
	song->combo_colors_start = n;
	return 0;
}

int osu_parse_hitobjs(char *line, osu_song *song,
					  mdev *dev, osu_meta *meta, osu_dir *cont) {
	return osu_parse_hit_object(line,song);
}

char *osu_util_str_split(char *line, char delim) {
	char *pdelim, *pend, *pval;
	for (pdelim = line; *pdelim; pdelim++) {
		if (*pdelim == delim)
			break;
	}
	if (!*pdelim)
		return NULL;
	// Remove whitespace
	for (pend = pdelim-1; pend != line; pend--) {
		if (!isspace(*pend)) {
			pend[1] = '\0';
			break;
		}
	}
	for (pval = pdelim+1; *pval; pval++) {
		if (!isspace(*pval)) {
			return pval;
		}
	}
	return pval;
}


void osu_check_beatmap_sanity(osu_song *bm) {

#define TEST_ENTRY_HOT(entryname) \
if (ISHOT(&(bm->entryname))) {\
fprintf(stderr, "Warning: Beatmap has no " #entryname "\n");\
}
#define TEST_ENTRY_ZERO(entryname) \
if (!bm->entryname) {\
fprintf(stderr, "Warning: Beatmap entry " #entryname "is zero\n");\
}

	// Check header magic
	if (strncmp(bm->magic, "BEAT", 4) != 0) {
		fprintf(stderr, "CRITICAL: Beatmap has invalid magic\n");
		return;
	}

	// Check mandatory fields
	TEST_ENTRY_HOT(cover_begin)
	TEST_ENTRY_HOT(cover_len)
	TEST_ENTRY_ZERO(cover_len)
	TEST_ENTRY_HOT(beatmap_begin)
	TEST_ENTRY_HOT(beatmap_len)
	TEST_ENTRY_ZERO(beatmap_len)
	TEST_ENTRY_HOT(audio_begin)
	TEST_ENTRY_HOT(audio_len)
	TEST_ENTRY_ZERO(audio_len)
	TEST_ENTRY_HOT(difficulty)
	TEST_ENTRY_HOT(hp_drain)
	TEST_ENTRY_HOT(circle_size)
	TEST_ENTRY_HOT(approach_rate)
	TEST_ENTRY_HOT(slider_mult)
	TEST_ENTRY_HOT(slider_tick)
	TEST_ENTRY_HOT(preview_blk_offset)
	TEST_ENTRY_HOT(beatmap_id)
	TEST_ENTRY_HOT(beatmap_setid)
	TEST_ENTRY_HOT(audio_leadin)
	TEST_ENTRY_HOT(stack_leniency)
	TEST_ENTRY_HOT(title)
	TEST_ENTRY_HOT(artist)
	TEST_ENTRY_HOT(creator)
	TEST_ENTRY_HOT(version)

	// Check combo
	TEST_ENTRY_HOT(combo_colors[0]);
	bool combo_defined = true;
	int ncombo = 1;
	for (int i=1; i<8; i++) {
		if (ISHOT(&(bm->combo_colors[i]))) {
			combo_defined = false;
		} else {
			if (!combo_defined) {
				fprintf(stderr, "Warning: extra combo color %d\n", i);
			} else {
				ncombo++;
			}
		}
	}
	printf("Combo colors defined: %d\n", ncombo);

	printf("Sanity check completed.\n");

}
