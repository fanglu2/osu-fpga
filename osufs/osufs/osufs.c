//
//  osufs.c
//  osufs
//
//  Created by Fang Lu on 11/28/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "osufs.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

osu_meta osu_read_meta(mdev *dev) {
	osu_meta ret;
	blkio_read(dev, 0, &ret);
	return ret;
}

osu_song osu_read_song(mdev *dev, int idx) {
	osu_song ret;
	blkio_read(dev, 1+idx, &ret);
	return ret;
}

void osu_write_meta(mdev *dev, osu_meta *meta) {
	blkio_write(dev, 0, meta);
}

void osu_write_song(mdev *dev, int idx, osu_song *song) {
	blkio_write(dev, idx+1, song);
}

void osu_init_device(mdev *dev) {
	osu_meta header;
	strncpy(header.magic, "osu!", 4);
	header.songs = 0;
	header.available_block = 64;
	osu_write_meta(dev, &header);
}

