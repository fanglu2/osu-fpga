//
//  imgenc.c
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "imgenc.h"

#include <stdio.h>
#include <string.h>
#include "libpng-wrapper.h"

uint32_t img_write_dither128(const char *filename, mdev *dev, uint32_t addr) {
	char cbuf[1024];
	int ret;
	
	system("rm -f /tmp/source.png /tmp/plt.png /tmp/dither.png");
	
	// Resize
	sprintf(cbuf, "ffmpeg -loglevel error -i \"%s\" -vf \"scale=640:480\" /tmp/source.png", filename);
	ret = system(cbuf);
	if (ret != 0) {
		fprintf(stderr, "ffmpeg resize failed with exit code: %d\n", ret);
		return addr;
	}

	// PaletteGen
	ret = system("ffmpeg -loglevel error -i /tmp/source.png -vf \"palettegen=max_colors=128\" /tmp/plt.png");
	if (ret != 0) {
		fprintf(stderr, "ffmpeg palettegen failed with exit code: %d\n", ret);
		return addr;
	}
	
	// Dithering
	ret = system("ffmpeg -loglevel error -i /tmp/source.png -i /tmp/plt.png -lavfi \"paletteuse=dither=floyd_steinberg\" -y /tmp/dither.png");
	if (ret != 0) {
		fprintf(stderr, "ffmpeg dithering failed with exit code: %d\n", ret);
	}
	
	
	PNGImage img = pngimg_read("/tmp/dither.png");
	if (img.color_type != PNG_COLOR_TYPE_PALETTE) {
		fprintf(stderr, "Internal Error: dithered image is not index!\n");
		return addr;
	}
	
	png_colorp plt;
	int nplt;
	if (!png_get_PLTE(img.png_ptr, img.info_ptr, &plt, &nplt)) {
		fprintf(stderr, "Internal Error: failed to read palette!\n");
		return addr;
	}

	// Dump palette
	uint8_t buf[512], *pltbuf = buf + 3;
	buf[0] = 'P';
	buf[1] = 'L';
	buf[2] = 'T';
	for (int i=0; i<128; i++) {
		pltbuf[i*3+0] = plt[i+128].red;
		pltbuf[i*3+1] = plt[i+128].green;
		pltbuf[i*3+2] = plt[i+128].blue;
	}
	blkio_write(dev, addr++, buf);
	
	// Dump image, row major
	printf("Saving pixels in %s\n", filename);
	ssize_t bufsize = png_get_rowbytes(img.png_ptr, img.info_ptr) * img.height;
	size_t bufptr = 0;
	while (bufsize > 0) {
		blkio_write(dev, addr++, img.row_pointers[0]+bufptr);
		bufptr += 512;
		bufsize -= 512;
	}
	
	pngimg_release(&img);

	return addr;
}

uint32_t img_write_dither64(const char *filename, mdev *dev, uint32_t addr,
							uint16_t *w, uint16_t *h) {
	char cbuf[1024];
	int ret;
	
	int w_signed = *w==0xffff ? -1 : *w, h_signed = *h==0xffff ? -1 : *h;
	
	system("rm -f /tmp/source.png /tmp/plt.png /tmp/dither.png");
	
	// Resize
	sprintf(cbuf, "ffmpeg -loglevel error -i \"%s\" -vf \"scale=%d:%d\" /tmp/source.png", filename, w_signed, h_signed);
	ret = system(cbuf);
	if (ret != 0) {
		fprintf(stderr, "ffmpeg resize failed with exit code: %d\n", ret);
		return addr;
	}
	
	// PaletteGen
	ret = system("ffmpeg -loglevel error -i /tmp/source.png -vf \"palettegen=max_colors=64\" /tmp/plt.png");
	if (ret != 0) {
		fprintf(stderr, "ffmpeg palettegen failed with exit code: %d\n", ret);
		return addr;
	}
	
	// Dithering
	ret = system("ffmpeg -loglevel error -i /tmp/source.png -i /tmp/plt.png -lavfi \"paletteuse=dither=floyd_steinberg\" -y /tmp/dither.png");
	if (ret != 0) {
		fprintf(stderr, "ffmpeg dithering failed with exit code: %d\n", ret);
	}
	
	
	PNGImage img = pngimg_read("/tmp/dither.png");
	if (img.color_type != PNG_COLOR_TYPE_PALETTE) {
		fprintf(stderr, "Internal Error: dithered image is not index!\n");
		return addr;
	}
	
	png_colorp plt;
	int nplt;
	if (!png_get_PLTE(img.png_ptr, img.info_ptr, &plt, &nplt)) {
		fprintf(stderr, "Internal Error: failed to read palette!\n");
		return addr;
	}
	
	// Dump palette
	uint8_t buf[512], *pltbuf = buf + 3;
	buf[0] = 'P';
	buf[1] = 'L';
	buf[2] = 'T';
	for (int i=0; i<64; i++) {
		pltbuf[i*3+0] = plt[0xff-i].red;
		pltbuf[i*3+1] = plt[0xff-i].green;
		pltbuf[i*3+2] = plt[0xff-i].blue;
	}
	blkio_write(dev, addr++, buf);
	
	// Dump image, row major
	*w = img.width;
	*h = img.height;
	printf("Saving pixels in %s\n", filename);
	ssize_t bufsize = png_get_rowbytes(img.png_ptr, img.info_ptr) * img.height;
	size_t bufptr = 0;
	uint8_t pixbuf[512];
	while (bufsize > 0) {
		for (int i=0; i<512 && bufsize>0; i++) {
			pixbuf[i] = 0xff - (img.row_pointers[0]+bufptr)[i];
			bufsize--;
		}
		blkio_write(dev, addr++, pixbuf);
		bufptr += 512;
	}
	
	pngimg_release(&img);
	
	return addr;
}

uint32_t img_write_rgb16(const char *filename, mdev *dev, uint32_t addr,
						 uint16_t *w, uint16_t *h) {
	char cbuf[1024];
	int ret;
	
	system("rm -f /tmp/source.png");
	
	// Resize
	int w_signed = (*w == 0xffff) ? -1 : *w;
	int h_signed = (*h== 0xffff) ? -1 : *h;

	sprintf(cbuf, "ffmpeg -loglevel error -i \"%s\" -vf \"scale=%d:%d\" -pix_fmt rgba /tmp/source.png ", filename, w_signed, h_signed);
	ret = system(cbuf);
	if (ret != 0) {
		fprintf(stderr, "ffmpeg resize failed with exit code: %d\n", ret);
		return addr;
	}
	
	FILE *fp = fopen("/tmp/source.png", "rb");
	if (!fp) {
		perror("Failed to open resized image");
		return addr;
	}

	PNGImage img = pngimg_read("/tmp/source.png");
	if (img.color_type != PNG_COLOR_TYPE_RGBA) {
		fprintf(stderr, "Internal Error: resized image is not RGBA!\n");
		return addr;
	}
	
	// Dump image as RGB16 (R5 G6 B5), row major
	printf("Saving pixels in %s\n", filename);
	// Each 512 byte block holds 256 pixels
	uint16_t buf[256];
	size_t npixels = img.width * img.height;
	*w = img.width;
	*h = img.height;
	int bufptr, pixptr = 0;
	uint8_t r, g, b, a;
	png_bytep pixdata = img.row_pointers[0];
	while (pixptr < npixels) {
		bufptr = 0;
		while (bufptr < 256) {
			r = pixdata[pixptr*4+0]>>3;
			g = pixdata[pixptr*4+1]>>2;
			b = pixdata[pixptr*4+2]>>3;
			a = pixdata[pixptr*4+3];
			if (a < 96) {
				// Transparent
				buf[bufptr++] = 0x07e0;
			} else {
				buf[bufptr++] = (r<<11)|(g<<5)|b;
			}
			pixptr++;
			if (pixptr >= npixels)
				break;
		}
		// Dump buffer
		blkio_write(dev, addr++, buf);
	}
	
	pngimg_release(&img);
	
	fclose(fp);

	return addr;
}
