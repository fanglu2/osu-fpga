//
//  wavenc.h
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef wavenc_h
#define wavenc_h

#ifdef __cplusplus
extern "C" {
#endif
	
#include <stdlib.h>
#include "blkio.h"
	
	uint16_t wav_write(const char *filename, mdev *dev, uint16_t addr);
	
#ifdef __cplusplus
}
#endif

#endif /* wavenc_h */
