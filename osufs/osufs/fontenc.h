//
//  fontenc.h
//  osufs
//
//  Created by Fang Lu on 12/10/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef fontenc_h
#define fontenc_h

#ifdef __cplusplus
extern "C" {
#endif
	
#include <stdlib.h>
#include "blkio.h"
	
	uint32_t font_write(const char *filename, int ptsize, mdev *dev, uint32_t addr);
	uint32_t font_score_write(const char *dirname, mdev *dev, uint32_t addr);

#ifdef __cplusplus
}
#endif

#endif /* fontenc_h */
