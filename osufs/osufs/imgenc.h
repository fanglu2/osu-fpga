//
//  imgenc.h
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef imgenc_h
#define imgenc_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include "blkio.h"
	
	uint32_t img_write_dither128(const char *filename, mdev *dev, uint32_t addr);
	uint32_t img_write_dither64(const char *filename, mdev *dev, uint32_t addr,
								uint16_t *w, uint16_t *h);
	uint32_t img_write_rgb16(const char *filename, mdev *dev, uint32_t addr,
							 uint16_t *w, uint16_t *h);

#ifdef __cplusplus
}
#endif
	
#endif /* imgenc_h */
