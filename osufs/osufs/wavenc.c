//
//  wavenc.c
//  osufs
//
//  Created by Fang Lu on 12/3/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "wavenc.h"

#include <stdio.h>
#include <string.h>

uint16_t wav_write(const char *filename, mdev *dev, uint16_t addr) {
	char cbuf[1024];
	int ret;
	
	system("rm -f /tmp/resample.wav");
	
	// Resample and mixdown channels
	sprintf(cbuf, "ffmpeg -loglevel error -i \"%s\" -map_metadata -1 -vn -ac 2 -ar 44100 /tmp/resample.wav", filename);
	ret = system(cbuf);
	if (ret != 0) {
		fprintf(stderr, "ffmpeg resample failed with exit code: %d\n", ret);
		return addr;
	}
	
	FILE *fp = fopen("/tmp/resample.wav", "rb");
	if (!fp) {
		perror("Failed to open resampled wave");
		return addr;
	}

	// Read header
	fread(cbuf, 1, 12, fp);
	if (strncmp(cbuf, "RIFF", 4) != 0 || strncmp(cbuf+8, "WAVE", 4) != 0) {
		fprintf(stderr, "Internal error: ffmpeg output not readable\n");
		fclose(fp);
		return addr;
	}
	
	// Find data chunk
	uint32_t chunksize;
	size_t nread;
	while (1) {
		nread = fread(cbuf, 1, 4, fp);
		if (nread != 4) {
			perror("Peek output wave failed");
			fprintf(stderr, "Chould not find audio data chunk\n");
			return addr;
		}
		nread = fread(&chunksize, 4, 1, fp);
		if (nread != 1) {
			perror("Peek output wave failed");
			fprintf(stderr, "Could not find audio data chunk\n");
			return addr;
		}
		if (strncmp(cbuf, "data", 4) == 0)
			break;
		else {
			if (fseek(fp, chunksize, SEEK_CUR) != 0) {
				perror("Seek in output wave failed");
				return addr;
			}
		}
	}
	printf("Copying audio %s\n", filename);
	uint8_t buf[512];
	while (chunksize > 0) {
		nread = fread(buf, 1, 512, fp);
		chunksize -= nread;
		if (nread != 512 && chunksize != 0) {
			perror("Read output wave failed");
			return addr;
		}
		blkio_write(dev, addr++, buf);
	}
	
	fclose(fp);
	return addr;
}
