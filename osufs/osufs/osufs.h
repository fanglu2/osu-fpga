//
//  osufs.h
//  osufs
//
//  Created by Fang Lu on 11/28/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef osufs_h
#define osufs_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <inttypes.h>
#include "blkio.h"
#include "osu_object_analyze/osu_object_includes.h"

	typedef struct __attribute__((__packed__)) osu_meta_t {
		char magic[4];							// 0   - 3
		uint16_t songs;							// 4   - 5
		uint32_t available_block;				// 6   - 9
		char meta_padding[6];					// 10  - 15
		
		// Resources
		// Sounds
		uint32_t applause_begin;				// 16  - 19
		uint32_t failsnd_begin;					// 20  - 23
		uint16_t applause_len;					// 24  - 25
		uint16_t failsnd_len;					// 26  - 27

		uint32_t spinnerspin_begin;				// 28  - 31
		uint32_t spinnerbonus_begin;			// 32  - 35
		uint16_t spinnerspin_len;				// 36  - 37
		uint16_t spinnerbonus_len;				// 38  - 39

		uint32_t menuclick_begin;				// 40  - 43
		uint32_t menuback_begin;				// 44  - 47
		uint16_t menuclick_len;					// 48  - 49
		uint16_t menuback_len;					// 50  - 51

		uint32_t menuhit_begin;					// 52  - 55
		uint32_t combobreak_begin;				// 56  - 59
		uint16_t menuhit_len;					// 60  - 61
		uint16_t combobreak_len;				// 62  - 63

		uint32_t drum_hitclap_begin;			// 64  - 67
		uint32_t drum_hitfinish_begin;			// 68  - 71
		uint16_t drum_hitclap_len;				// 72  - 73
		uint16_t drum_hitfinish_len;			// 74  - 75

		uint32_t drum_hitnormal_begin;			// 76  - 79
		uint32_t drum_hitwhistle_begin;			// 80  - 83
		uint16_t drum_hitnormal_len;			// 84  - 85
		uint16_t drum_hitwhistle_len;			// 86  - 87

		uint32_t drum_sliderslide_begin;		// 88  - 91
		uint32_t drum_slidertick_begin;			// 92  - 95
		uint16_t drum_sliderslide_len;			// 96  - 97
		uint16_t drum_slidertick_len;			// 98  - 99

		uint32_t drum_sliderwhistle_begin;		// 100 - 103
		uint32_t drum_padding_begin;			// 104 - 107
		uint16_t drum_sliderwhistle_len;		// 108 - 109
		uint16_t drum_padding_len;				// 110 - 111

		uint32_t normal_hitclap_begin;			// 112 - 115
		uint32_t normal_hitfinish_begin;		// 116 - 119
		uint16_t normal_hitclap_len;			// 120 - 121
		uint16_t normal_hitfinish_len;			// 122 - 123

		uint32_t normal_hitnormal_begin;		// 124 - 127
		uint32_t normal_hitwhistle_begin;		// 128 - 131
		uint16_t normal_hitnormal_len;			// 132 - 133
		uint16_t normal_hitwhistle_len;			// 134 - 135

		uint32_t normal_sliderslide_begin;		// 136 - 139
		uint32_t normal_slidertick_begin;		// 140 - 143
		uint16_t normal_sliderslide_len;		// 144 - 145
		uint16_t normal_slidertick_len;			// 146 - 147

		uint32_t normal_sliderwhistle_begin;	// 148 - 151
		uint32_t normal_padding_begin;			// 152 - 155
		uint16_t normal_sliderwhistle_len;		// 156 - 157
		uint16_t normal_padding_len;			// 158 - 159

		uint32_t soft_hitclap_begin;			// 160 - 163
		uint32_t soft_hitfinish_begin;			// 164 - 167
		uint16_t soft_hitclap_len;				// 168 - 169
		uint16_t soft_hitfinish_len;			// 170 - 171

		uint32_t soft_hitnormal_begin;			// 172 - 175
		uint32_t soft_hitwhistle_begin;			// 176 - 179
		uint16_t soft_hitnormal_len;			// 180 - 181
		uint16_t soft_hitwhistle_len;			// 182 - 183

		uint32_t soft_sliderslide_begin;		// 184 - 187
		uint32_t soft_slidertick_begin;			// 188 - 191
		uint16_t soft_sliderslide_len;			// 192 - 193
		uint16_t soft_slidertick_len;			// 194 - 195

		uint32_t soft_sliderwhistle_begin;		// 196 - 199
		uint32_t soft_padding_begin;			// 200 - 203
		uint16_t soft_sliderwhistle_len;		// 204 - 205
		uint16_t soft_padding_len;				// 206 - 207

		// Image resources
		uint32_t hit_0;							// 208 - 211
		uint16_t hit_0_w;						// 212 - 213
		uint16_t hit_0_h;						// 214 - 215

		uint32_t hit_100;						// 216 - 219
		uint16_t hit_100_w;						// 220 - 221
		uint16_t hit_100_h;						// 222 - 223
		
		uint32_t hit_300;						// 224 - 227
		uint16_t hit_300_w;						// 228 - 229
		uint16_t hit_300_h;						// 230 - 231
		
		uint32_t hit_100k;						// 232 - 235
		uint16_t hit_100k_w;					// 236 - 237
		uint16_t hit_100k_h;					// 238 - 239
		
		uint32_t hit_300k;						// 240 - 243
		uint16_t hit_300k_w;					// 244 - 245
		uint16_t hit_300k_h;					// 246 - 247
		
		uint32_t hit_300g;						// 248 - 251
		uint16_t hit_300g_w;					// 252 - 253
		uint16_t hit_300g_h;					// 254 - 255
		
		uint32_t pause_back;					// 256 - 259
		uint16_t pause_back_w;					// 260 - 261
		uint16_t pause_back_h;					// 262 - 263
		
		uint32_t pause_continue;				// 264 - 267
		uint16_t pause_continue_w;				// 268 - 269
		uint16_t pause_continue_h;				// 270 - 271
		
		uint32_t pause_retry;					// 272 - 275
		uint16_t pause_retry_w;					// 276 - 277
		uint16_t pause_retry_h;					// 278 - 279
		
		uint32_t ranking_ss;					// 280 - 283
		uint16_t ranking_ss_w;					// 284 - 285
		uint16_t ranking_ss_h;					// 286 - 287
		
		uint32_t ranking_s;						// 288 - 291
		uint16_t ranking_s_w;					// 292 - 293
		uint16_t ranking_s_h;					// 294 - 295

		uint32_t ranking_a;						// 296 - 299
		uint16_t ranking_a_w;					// 300 - 301
		uint16_t ranking_a_h;					// 302 - 303

		uint32_t ranking_b;						// 304 - 307
		uint16_t ranking_b_w;					// 308 - 309
		uint16_t ranking_b_h;					// 310 - 311
		
		uint32_t ranking_c;						// 312 - 315
		uint16_t ranking_c_w;					// 316 - 317
		uint16_t ranking_c_h;					// 318 - 319
		
		uint32_t ranking_d;						// 320 - 323
		uint16_t ranking_d_w;					// 324 - 325
		uint16_t ranking_d_h;					// 326 - 327
		
		uint32_t ranking_title;					// 328 - 331
		uint16_t ranking_title_w;				// 332 - 333
		uint16_t ranking_title_h;				// 334 - 335
		
		uint32_t ranking_panel;					// 336 - 339
		uint16_t ranking_panel_w;				// 340 - 341
		uint16_t ranking_panel_h;				// 342 - 343
		
		uint32_t selection_mod;					// 344 - 347
		uint32_t selection_mod_over;			// 348 - 351
		uint32_t selection_random;				// 352 - 355
		uint32_t selection_random_over;			// 356 - 359
		uint16_t selection_w;					// 360 - 361
		uint16_t selection_h;					// 362 - 363
		
		uint32_t mod_auto;						// 364 - 367
		uint32_t mod_nofail;					// 368 - 371
		uint32_t mod_easy;						// 372 - 375
		uint32_t mod_hardrock;					// 376 - 379
		uint32_t mod_hidden;					// 380 - 383
		uint32_t mod_suddendeath;				// 384 - 387
		uint16_t mod_w;							// 388 - 389
		uint16_t mod_h;							// 390 - 391

		
		// Font resources
		uint32_t font_12;						// 392 - 395
		uint32_t font_16;						// 396 - 399
		uint32_t font_24;						// 400 - 403
		uint32_t font_score;					// 404 - 407

		char _padding[104];						// 408 - 511
	} osu_meta;

	typedef struct __attribute__((__packed__)) osu_font_t {
		char magic[4];							// 0   - 3
		uint32_t font[96];						// 4   - 387
		uint8_t widths[96];						// 388 - 483
		uint8_t height;							// 485
		char _padding[27];						// 486 - 511
	} osu_font;
	
	typedef struct __attribute__((__packed__)) storyboard_t {
		uint32_t magic;							//4
		uint32_t object_number;					//4
		elem_hit_circle object_list[65535];		//fill a total of 4096 blocks
		char _padding[24];						//32-8 = 24
	} osu_storyboard;

	typedef struct __attribute__((__packed__)) osu_song_t {
		char magic[4];							// 0   - 3
		uint32_t cover_begin, cover_len;		// 4   - 11
		uint32_t audio_begin, audio_len;		// 12  - 19
		uint32_t beatmap_begin, beatmap_len;	// 20  - 27
		int16_t difficulty;						// 28  - 29
		int16_t hp_drain;						// 30  - 31
		int16_t circle_size;					// 32  - 33
		int16_t approach_rate;					// 34  - 35
		int16_t slider_mult;					// 36  - 37
		int16_t slider_tick;					// 38  - 39
		uint8_t combo_colors[8][3];				// 40  - 63
		uint32_t preview_blk_offset;			// 64  - 67
		uint32_t beatmap_id;					// 68  - 71
		uint32_t beatmap_setid;					// 72  - 75
		uint16_t audio_leadin;					// 76  - 77
		uint16_t stack_leniency;				// 78  - 79

		//for analyze side only
		osu_storyboard *storyboard;					// 80 - 87
		uint8_t combo_colors_count;				// 88
		uint8_t combo_colors_start;				// 89
		uint8_t combo_slider_color[3];			// 90 - 92


		char _padding[227];						// 93  - 319

		char title[64];							// 320 - 383
		char artist[64];						// 384 - 447
		char creator[32];						// 448 - 479
		char version[32];						// 480 - 511
	} osu_song;


	osu_meta osu_read_meta(mdev *dev);
	osu_song osu_read_song(mdev *dev, int idx);
	void osu_write_meta(mdev *dev, osu_meta *meta);
	void osu_write_song(mdev *dev, int idx, osu_song *song);

	void osu_init_device(mdev *dev);

#ifdef __cplusplus
}
#endif

#endif /* osufs_h */
