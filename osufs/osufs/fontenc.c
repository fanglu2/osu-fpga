//
//  fontenc.c
//  osufs
//
//  Created by Fang Lu on 12/10/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "fontenc.h"

#include <stdio.h>
#include <string.h>

#include "imgenc.h"
#include "osufs.h"
#include "blkio.h"

uint32_t font_write(const char *filename, int ptsize, mdev *dev, uint32_t addr) {
	char cbuf[1024], stresc[8];
	int ret;
	
	osu_font fnt;
	
	uint16_t w = -1, h = -1;
	
	system("rm -f /tmp/fnt.png");
	
	for (int i=0; i<96; i++) {
		// Convert TTF to PNG
		char c = i+' ';
		
		printf("Rasterizing '%c'...\n", c);
		
		switch (c) {
			case ' ':
				stresc[0] = '?';
				stresc[1] = '\0';
				break;
			case '"':
				strcpy(stresc, "\\\"");
				break;
			case '`':
				strcpy(stresc, "\\`");
				break;
			case '\\':
				strcpy(stresc, "\\\\\\\\");
				break;
			default:
				stresc[0] = c;
				stresc[1] = '\0';
		}
		
		sprintf(cbuf, "convert -background none -fill black -font %s "
				"-pointsize %d label:\"%s\" /tmp/fnt.png",
				filename, ptsize, stresc);
		ret = system(cbuf);
		if (ret != 0) {
			fprintf(stderr, "imagemagick font-rendering failed with exit code: %d\n", ret);
			return addr;
		}
		
		// Load PNG
		w = -1;
		h = -1;
		fnt.font[i] = addr;
		addr = img_write_rgb16("/tmp/fnt.png", dev, addr, &w, &h);
		fnt.widths[i] = w;
	}

	fnt.height = h;
	strncpy(fnt.magic, "oFNT", 4);
	
	// Write font to disk
	
	blkio_write(dev, addr++, &fnt);
	
	return addr;
}

uint32_t font_score_write(const char *dirname, mdev *dev, uint32_t addr) {

#define FONT_IMPORT_GLYPH(chr, filename)\
strcpy(fname, filename);\
w = -1;\
fnt.font[chr-' '] = addr;\
addr = img_write_dither64(cbuf, dev, addr, &(w), &(h));\
fnt.widths[chr-' '] = w;
	
	osu_font fnt;
	memset(&fnt, 0, sizeof(fnt));
	char cbuf[1024], *fname = cbuf + strlen(dirname);
	strncpy(cbuf, dirname, 1024);
	*fname = '/';
	fname++;
	
	fnt.height = 36;
	uint16_t w, h = fnt.height;

	FONT_IMPORT_GLYPH('0', "score-0.png")
	FONT_IMPORT_GLYPH('1', "score-1.png")
	FONT_IMPORT_GLYPH('2', "score-2.png")
	FONT_IMPORT_GLYPH('3', "score-3.png")
	FONT_IMPORT_GLYPH('4', "score-4.png")
	FONT_IMPORT_GLYPH('5', "score-5.png")
	FONT_IMPORT_GLYPH('6', "score-6.png")
	FONT_IMPORT_GLYPH('7', "score-7.png")
	FONT_IMPORT_GLYPH('8', "score-8.png")
	FONT_IMPORT_GLYPH('9', "score-9.png")
	FONT_IMPORT_GLYPH(',', "score-comma.png")
	FONT_IMPORT_GLYPH('.', "score-dot.png")
	FONT_IMPORT_GLYPH('%', "score-percent.png")
	FONT_IMPORT_GLYPH('x', "score-x.png")

	
	blkio_write(dev, addr++, &fnt);
	strncpy(fnt.magic, "oFNT", 4);
	
	return addr;
}
