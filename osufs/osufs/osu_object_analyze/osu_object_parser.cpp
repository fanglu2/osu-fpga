#include "osu_object_parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

/*int osu_storyboard_generate(void* buff,osu_song* song){
	osu_hit_object* hit_object_list = malloc(65536);
	uint32_t object_count=0,temp;
	while (){
		temp = osu_parse_hit_object(line,hit_object_list+object_count,song){
		if (temp==-1) {
			printf("parsing failed!\n");
			return 1;
		}
		object_count += temp;
	}

	(*(uint32_t*)buff) = object_count;
	buff += 4;
	(*(uint32_t*)buff) = 0b01110010011001010110000101101100;
	buff += 4; ///////////
	memcpy(buff,hit_object_list,(object_count*32));
	free(hit_object_list);
	return 0;
}*/


int osu_parse_hit_object(char* line, osu_song* song){
	elem_hit_circle* elemn = (elem_hit_circle*) malloc(sizeof(elem_hit_circle));

	vector<string> v;
	string_split(line,",",v);
	
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Waddress-of-packed-member"
	sscanf(v[0].c_str(),"%hd",&(elemn->x));
	sscanf(v[1].c_str(),"%hd",&(elemn->y));
#pragma clang diagnostic pop
	
	//type
	char t_type;
	sscanf(v[3].c_str(),"%c",&t_type);
	//color handle
	if (t_type & 0x04){
		//combo analysis
		song->combo_colors_start++;
		song->combo_colors_start += (t_type>>4);
		song->combo_colors_start %= (song->combo_colors_count);
	}
	int combo_count = song->combo_colors_start;
	if (t_type & 0x01)	t_type = 'h';
	else if (t_type & 0x02)	t_type = 'l';
	else if (t_type & 0x08)	t_type = 's';
	elemn -> type = t_type;

	elemn -> gradient_color1 = ((song->combo_colors[combo_count][0]&0b11111000)<<11)
							|((song->combo_colors[combo_count][1]&0b11111100)<<5)
							|((song->combo_colors[combo_count][2]&0b11111000));
	
	//hit sound 
	//elemn->hit_sound = stoi(v[4]); //
	sscanf(v[4].c_str(),"%c",&(elemn->hit_sound)); // 

	//handle timing stuff
	int temp_c = 0;
	switch (t_type){
		case 'h':
			temp_c=osu_parse_hit_circle(v,elemn,song);
			break;
		case 'l':
//			temp_c=osu_parse_slider(v,elemn,song); // TODO
			break;
		case 's':
			temp_c=osu_parse_spinner(v,(elem_spinner *)elemn,song);
			break;
	}
	//update count and data block
	if (!song->storyboard){
		song->storyboard = (osu_storyboard*) malloc(sizeof(osu_storyboard));
		/**((uint32_t*)(song->storyboard)) = 0b01110010011001010110000101101100;
		*((uint32_t*)(song->storyboard)+1) = temp_c;
		memcpy(song->storyboard+8,elemn,32*temp_c);*/
		song->storyboard->magic = 0b01110010011001010110000101101100;
		song->storyboard->object_number = temp_c;
	}else{
		/*song->storyboard = (uint8_t *) realloc(song->storyboard,4+4+
			32*(temp_c+(*((uint32_t*)(song->storyboard)+1))));
		memcpy(song->storyboard+8+32*(*((uint32_t*)(song->storyboard)+1)),elemn,32*temp_c);
		*((uint32_t*)(song->storyboard)+1) += temp_c;*/
		memcpy(song->storyboard->object_list,
			   elemn,temp_c*sizeof(elem_hit_circle));
		song->storyboard->object_number += temp_c;
	}
	free(elemn);
	return 0;
}

void string_split(char* line, const char* delim, vector<string> & v){
	char dup[1024];
	strncpy(dup, line, 1024);
    char * token = strtok(dup, delim);
    while(token != NULL){
        v.push_back(string(token));
        // the call is treated as a subsequent calls to strtok:
        // the function continues from where it left in previous invocation
        token = strtok(NULL, delim);
    }
}

int osu_parse_hit_circle(vector<string>& v, elem_hit_circle* elemn,osu_song* song){
// - 140 * approach rate + 1800
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Waddress-of-packed-member"
	sscanf(v[2].c_str(),"%d",&(elemn -> check_timing));
#pragma clang diagnostic pop
	elemn -> start_timing = elemn -> check_timing - (((song->approach_rate/(256.0))*(-140.0))+1800.0);
	elemn -> disappear_timing = elemn -> check_timing + (200-((song->difficulty)/(256.0))*10);
	//TBD
	//elemn -> end_timing
	elemn -> hit_radius = 100 - (song ->difficulty/(256.0)) * 4.0;
	elemn -> approach_radius = 3*(elemn->hit_radius);
	elemn -> approach_rate = (elemn->approach_radius-elemn->hit_radius)/(elemn->check_timing-elemn->start_timing);

	return 1;
}

int osu_parse_spinner(vector<string>& v, elem_spinner* elemn,osu_song* song){
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Waddress-of-packed-member"
	//elemn -> start_timing = v[2];
	sscanf(v[2].c_str(),"%d",&(elemn -> start_timing));
	//elemn -> disappear_timing = v[5];
	sscanf(v[5].c_str(),"%d",&(elemn -> disappear_timing));
	elemn -> cycles = (elemn -> disappear_timing/1000.0*(1.5+(song ->difficulty/(256.0))/10.0));
	elemn -> disappear_timing += elemn -> start_timing;

	return 1;
#pragma clang diagnostic pop
}

/*int osu_parse_slider(vector<string>& v, elemn_spinner* elemn,osu_song* song){
	if (v[5][0] == 'L'){

		return 1;
	}else if (v[5][0] == 'P'){

	}else{
		return 0;
	}
}*/
