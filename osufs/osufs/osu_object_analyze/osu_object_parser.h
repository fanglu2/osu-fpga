#ifndef OSU_OBJECT_PARSER_H_
#define OSU_OBJECT_PARSER_H_

#include "osu_object_includes.h"

#include <vector>
#include <string>

#include "osufs.h"

//int osu_storyboard_generate(void* buff,osu_song* song);

int osu_parse_hit_object(char* line, osu_song* song);

void string_split(char* dup, const char* delim, std::vector<std::string> & v);

int osu_parse_hit_circle(std::vector<std::string>& v, elem_hit_circle* elemn,osu_song* song);

int osu_parse_spinner(std::vector<std::string>& v, elem_spinner* elemn,osu_song* song);
#endif
