//
//  libpng-wrapper.h
//  mp6
//
//  Created by Fang Lu on 11/16/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef libpng_wrapper_h
#define libpng_wrapper_h

#include <inttypes.h>

#include "png.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct PNGImage_t {
	int width, height;
	png_byte color_type;
	png_byte bit_depth;
	
	png_structp png_ptr;
	png_infop info_ptr;
	int number_of_passes;
	png_bytep * row_pointers;
} PNGImage;

PNGImage pngimg_read(const char* file_name);
int pngimg_write(const char* file_name, png_bytep pixdata,
					 size_t width, size_t height, int alpha);
void pngimg_release(PNGImage *img);

#ifdef __cplusplus
}
#endif /* __cplusplus */

	
#endif /* libpng_wrapper_h */
