//
//  blkio.c
//  osufs
//
//  Created by Fang Lu on 11/28/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include "blkio.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <assert.h>

#ifdef _WIN32

#error "Win32 NOT supported"

#elif __APPLE__

#include <sys/disk.h>

uint64_t blkio_size(uint32_t dev) {
	uint64_t sector_count = 0;
	// Query the number of sectors on the disk
	if (ioctl(dev, DKIOCGETBLOCKCOUNT, &sector_count) == -1) {
		fprintf(stderr, "Failed to get block count\n");
		return 0;
	}
	fprintf(stderr, "IOCTL: block count is %lu\n", sector_count);
	
	uint32_t sector_size = 0;
	// Query the size of each sector
	if (ioctl(dev, DKIOCGETBLOCKSIZE, &sector_size) == -1) {
		fprintf(stderr, "Failed to get block size\n");
		return 0;
	}
	fprintf(stderr, "IOCTL: block size is %lu\n", sector_size);
	
	return sector_count * sector_size;
}

#elif __linux__

#include <linux/fs.h>

uint64_t blkio_size(uint32_t dev) {
	uint64_t block_size = 0;
	if (ioctl(dev, BLKGETSIZE, &block_size) == -1) {
		fprintf(stderr, "Failed to get device size\n");
		return 0; // Failed
	}
	fprintf(stderr, "IOCTL: block size is %lu\n", block_size);
	return block_size*512;
}

#elif __unix__

#error "Platform not supported"

#elif defined(_POSIX_VERSION)

#error "Platform not supported"

#else

#error "Unknown compiler"

#endif


mdev *blkio_open(const char *filename) {
	uint32_t fd = open(filename, O_RDWR);
	if (fd == -1) {
		fprintf(stderr, "Failed to open file %s\n", filename);
		fprintf(stderr, "%s (%d)\n", strerror(errno), errno);
		return NULL;
	}
	uint64_t fsize = blkio_size(fd);
	printf("File opened (fd=%u). Device size is: %lu\n", fd, fsize);
	if (!fsize) {
		return NULL;
	}
//	void *base = mmap(NULL, fsize, PROT_READ,
//					  MAP_SHARED|MAP_FILE, fd, 0);
//	if (base == MAP_FAILED) {
//		fprintf(stderr, "MMap failed.\n");
//		fprintf(stderr, "%s (%d)\n", strerror(errno), errno);
//		base = NULL;
//	}
	mdev *ret = malloc(sizeof(mdev));
//	ret->base = base;
	ret->len = fsize;
	ret->fd = fd;
	return ret;
}

void blkio_close(mdev *dev) {
//	munmap(dev->base, dev->len);
	close(dev->fd);
	free(dev);
}

void blkio_read(mdev *dev, uint32_t addr, void *buf) {
	assert((addr*512) < dev->len && "Requested address beyond device size");
	ssize_t ret = pread(dev->fd, buf, 512, addr*512);
	if (ret == -1) {
		perror("Failed to read block device");
		abort();
	} else if (ret != 512) {
		fprintf(stderr, "Could not fetch 512 bytes in one read. "
				"Bytes read: %zd", ret);
		abort();
	}
}

void blkio_write(mdev *dev, uint32_t addr, void *buf) {
	assert((addr*512) < dev->len && "Writing beyond device size");
	ssize_t ret = pwrite(dev->fd, buf, 512, addr*512);
	if (ret == -1) {
		perror("Failed to read block device");
		abort();
	} else if (ret != 512) {
		fprintf(stderr, "Could not write 512 bytes in one write. "
				"Bytes written: %zd", ret);
		abort();
	}
}
