//
//  libpng-wrapper.c
//  mp6
//
//  Created by Fang Lu on 11/16/17.
//  Adapted from libpng example code at http://zarb.org/~gc/html/libpng.html
//

#include "libpng-wrapper.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PNGImage pngimg_read(const char* file_name) {
	PNGImage ret;
	memset(&ret, 0, sizeof(ret));
	uint8_t header[8];    // 8 is the maximum size that can be checked
	
	/* open file and test for it being a png */
	FILE *fp = fopen(file_name, "rb");
	if (!fp)
		return ret;
	fread(header, 1, 8, fp);
	if (png_sig_cmp(header, 0, 8))
		return ret;
	
	
	/* initialize stuff */
	ret.png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	
	if (!ret.png_ptr)
		return ret;
	
	ret.info_ptr = png_create_info_struct(ret.png_ptr);
	if (!ret.info_ptr)
		return ret;
	
	if (setjmp(png_jmpbuf(ret.png_ptr)))
		return ret;
	
	png_init_io(ret.png_ptr, fp);
	png_set_sig_bytes(ret.png_ptr, 8);
	
	png_read_info(ret.png_ptr, ret.info_ptr);
	
	ret.width = png_get_image_width(ret.png_ptr, ret.info_ptr);
	ret.height = png_get_image_height(ret.png_ptr, ret.info_ptr);
	ret.color_type = png_get_color_type(ret.png_ptr, ret.info_ptr);
	ret.bit_depth = png_get_bit_depth(ret.png_ptr, ret.info_ptr);
	
	ret.number_of_passes = png_set_interlace_handling(ret.png_ptr);
	png_read_update_info(ret.png_ptr, ret.info_ptr);
	
	
	/* read file */
	if (setjmp(png_jmpbuf(ret.png_ptr)))
		return ret;
	
	size_t rowbytes = png_get_rowbytes(ret.png_ptr,ret.info_ptr);
	ret.row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * ret.height);
	png_bytep pixdata = (png_bytep) malloc(ret.height * rowbytes);
	if (!pixdata)
		return ret;
	for (int i=0; i<ret.height; i++)
		ret.row_pointers[i] = pixdata + i * rowbytes;
		
	png_read_image(ret.png_ptr, ret.row_pointers);
	
	fclose(fp);
	
	return ret;
}

int pngimg_write(const char* file_name, png_bytep pixdata,
				   size_t width, size_t height, int alpha) {
	/* create file */
	FILE *fp = fopen(file_name, "wb");
	if (!fp)
		return 0;
	
	PNGImage ps;
	memset(&ps, 0, sizeof(ps));
	
	/* initialize stuff */
	ps.png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	
	if (!ps.png_ptr)
		return 0;
	
	ps.info_ptr = png_create_info_struct(ps.png_ptr);
	if (!ps.info_ptr)
		return 0;
	
	if (setjmp(png_jmpbuf(ps.png_ptr)))
		return 0;
	
	png_init_io(ps.png_ptr, fp);
	
	
	/* write header */
	if (setjmp(png_jmpbuf(ps.png_ptr)))
		return 0;
	
	png_set_IHDR(ps.png_ptr, ps.info_ptr, width, height, 8,
				 (alpha ? PNG_COLOR_TYPE_RGBA :PNG_COLOR_TYPE_RGB),
				 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
				 PNG_FILTER_TYPE_BASE);

	png_write_info(ps.png_ptr, ps.info_ptr);
	
	/* write bytes */
	if (setjmp(png_jmpbuf(ps.png_ptr)))
		return 0;
	
	size_t rowbytes = (alpha ? 4 : 3) * width;
	ps.row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
	for (int i=0; i<height; i++)
		ps.row_pointers[i] = pixdata + i * rowbytes;

	png_write_image(ps.png_ptr, ps.row_pointers);
	
	free(ps.row_pointers);
	
	/* end write */
	if (setjmp(png_jmpbuf(ps.png_ptr)))
		return 0;
	
	png_write_end(ps.png_ptr, NULL);

	png_destroy_write_struct(&ps.png_ptr, &ps.info_ptr);

	fclose(fp);
	return 1;
}

void pngimg_release(PNGImage *img) {
	if (img->png_ptr && img->info_ptr) {
		png_destroy_read_struct(&img->png_ptr, &img->info_ptr, NULL);
	}
	if (!img->row_pointers)
		return;
	if (img->row_pointers[0])
		free(img->row_pointers[0]);
	free(img->row_pointers);
	memset(img, 0, sizeof(*img));
}
