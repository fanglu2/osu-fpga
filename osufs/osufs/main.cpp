//
//  main
//  osufs
//
//  Created by Fang Lu on 11/28/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "imgenc.h"
#include "blkio.h"
#include "osufs.h"
#include "osu.h"
#include "osu_object_includes.h"

#define IO_BUFLEN 1024

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s device file\n", argv[0]);
		return 1;
	}
	
	// Self-checks
	assert(sizeof(osu_meta) == 512 && "Invalid osu_meta struct size");
	assert(sizeof(osu_song) == 512 && "Invalid osu_song struct size");
	assert(sizeof(osu_font) == 512 && "Invalid osu_font struct size");
	assert(sizeof(elem_hit_circle) == 32 && "Invalid elem_hit_circle size");
	assert(sizeof(elem_spinner) == 32 && "Invalid elem_spinner size");
	assert(sizeof(osu_storyboard) == 2097152 && "Invalid osu_storyboard size");

	mdev *dev = blkio_open(argv[1]);
	if (!dev) {
		fprintf(stderr, "Failed to open device file\n");
		return 1;
	}
	
	printf("Device capacity is: %lu MiB\n", (dev->len >> 20));
	if ((dev->len >> 30) > 2) {
		printf("WARNING: this device is bigger than 2 GB. "
			   "SDCards greater than 2 GB will likely fail due to addressing. "
			   "Or please check if you have specified a wrong drive\n");
	} else if ((dev->len >> 20) < 1) {
		printf("WARNING: this device is smaller than 1 MB."
			   "It might not be a block device. "
			   "OsuFs must be written to a raw disk for the FPGA to read.\n");
	}
	
osufs_init:
	
	osu_meta meta = osu_read_meta(dev);
	if (strncmp(meta.magic, "osu!", 4) != 0) {
		printf("WARNING: Device is not currently formatted as osufs\n");
		printf("You have to manually run `init' before any operation.\n");
		printf("Please make sure %s is the device to write!\n", argv[1]);
	} else {
		printf("Device contains %d beatmaps\n", meta.songs);
		printf("Total sectors used: %d (%d MiB)\n", meta.available_block,
			   ((meta.available_block*512) >> 20));
	}
	
	char buf[IO_BUFLEN], *phead, *ptail, *pcmd;
	while (1) {
		buf[0] = '\0';
		printf("OsuFs> ");
		memset(buf, 0, IO_BUFLEN);
		if (!fgets(buf, IO_BUFLEN, stdin)) {
			// EOF
			printf("exit\n");
			break;
		}
		buf[IO_BUFLEN-1]='\0';
		if (buf[IO_BUFLEN-1]!='\0') {
			fprintf(stderr, "Command is too long.\n");
			goto parse_end;
		}
		buf[strlen(buf)-1] = '\0'; // Trim \n
								   // Extract cmd
		for (phead = buf; isspace(*phead); phead++) {
			if (!*phead) {
				// Empty line here
				break;
			}
		}
		if (!*phead || *phead == '#') {
			// Comment or empty line
			continue;
		}
		for (ptail = phead; *ptail && !isspace(*ptail); ptail++) {
			if (!ptail) {
				fprintf(stderr, "Unexpected EOF\n");
				goto parse_end;
			}
		}
		*ptail = '\0';
		
		pcmd = phead;
		for (phead = ptail+1; isspace(*phead); phead++) {
			if (!*phead) {
				// No arguments
				phead = NULL;
				break;
			}
		}
		
		if (strcasecmp(pcmd, "init") == 0) {
			printf("WARNING: init will destroy ALL data on the device, "
				   "including partition table and filesystem table\n");
			printf("Selected device is %s\n", argv[1]);
			printf("Proceed? [Y/N] ");
			char confirm = 'n';
			scanf("%c", &confirm);
			if (confirm == 'Y') {
				osu_init_device(dev);
				goto osufs_init;
			} else {
				printf("Operation cancelled.\n");
			}
		} else if (strcasecmp(pcmd, "import") == 0) {
			if (!phead) {
				printf("Usage: import directory\n");
				goto parse_end;
			}
			osu_dir dir = osu_open_dir(phead);
			const char *filename = osu_read_dir(dir);
			while (filename) {
				printf("Importing %s...\n", filename);
				osu_song s = osu_read_osu(filename, dev, &meta, &dir);
				if (strncmp(s.magic, "BEAT", 4) == 0) {
					osu_write_song(dev, meta.songs, &s);
					meta.songs++;
				} else {
					fprintf(stderr, "Not importing %s due to parsing error\n",
							filename);
				}
				filename = osu_read_dir(dir);
			}
		} else if (strcasecmp(pcmd, "skin") == 0) {
			if (!phead) {
				printf("Usage: skin directory\n");
				goto parse_end;
			}
			printf("Installing directory as default skin...\n");
			osu_install_skin(phead, dev, &meta);
		} else if (strcasecmp(pcmd, "font") == 0) {
			if (!phead) {
				printf("Usage: font /path/to/font.ttf\n");
				goto parse_end;
			}
			printf("Installing font...\n");
			osu_install_font(phead, dev, &meta);
		} else if (strcasecmp(pcmd, "flushmeta") == 0) {
			printf("Flushing meta...\n");
			osu_write_meta(dev, &meta);
		} else if (strcasecmp(pcmd, "exit") == 0) {
			break;
		} else {
			fprintf(stderr, "Unrecognized command `%s'\n", pcmd);
		}
		
	parse_end:
		void(0);
	}
	printf("Flushing meta...\n");
	osu_write_meta(dev, &meta);
	blkio_close(dev);
	
	return 0;
}

