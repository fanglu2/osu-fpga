//
//  blkio.h
//  osufs
//
//  Created by Fang Lu on 11/28/17.
//  Copyright © 2017 Fang Lu. All rights reserved.
//

#ifndef blkio_h
#define blkio_h

#ifdef __cplusplus
extern "C" {
#endif
	
#include <inttypes.h>
	
	typedef struct mdev_t {
		void *base;
		uint64_t len;
		uint32_t fd;
	} mdev;

	mdev *blkio_open(const char *filename);
	void blkio_close(mdev *dev);

	void blkio_read(mdev *dev, uint32_t addr, void *buf);
	void blkio_write(mdev *dev, uint32_t addr, void *buf);

#ifdef __cplusplus
}
#endif

#endif /* blkio_h */
