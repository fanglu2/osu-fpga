module final_sd_interface (
	input logic CLK,    // Clock
	input logic RESET, // RESET
	input logic [7:0] sd_addr,
	input logic sd_cs, sd_read, sd_write,
	input logic [31:0]  sd_writedata,
	output logic [31:0] sd_readdata,
	//export
	output  logic SD_CLK,
	inout wire SD_CMD,
	inout wire [3:0] SD_DAT,
	output wire[6:0] LEDG,
	output wire[15:0] LEDR
);


logic [1:0] interface_status, interface_status_next;

logic SD_CLK_next;
//buffers
logic [31:0] dataBuffer [128] , dataBuffer_next[128];

//addressed command register
logic [2:0] interface_command_mes;

//sd command send register
//logic sd_cmd_reg_load, sd_cmd_reg_shift;
logic [7:0] sd_cmd_message [6], sd_cmd_message_next[6];
//sd command receive register
//logic sd_cmd_rec_shift;
//wire [7:0] sd_cmd_rec_mes;

logic [10:0] counter,counter_next;
logic c_reset;//c_continue;
//logic sd_clk_tog;
logic sd_cmd_in, sd_dat_in, sd_cmd_in_next;
//logic [3:0] sd_dat_buffer;
///logic sd_cmd_reg_output;
logic [47:0]reg_48,reg_48_next;
logic [7:0] reg_8,reg_8_next;

enum logic [12:0]{
	Init 					=13'b0000000000001,
	Idle					=13'b0000000000010,
	Send_cmd				=13'b0000000000100,
	Receive_cmd				=13'b0000000001000,
	Receive_cmd_probe_0		=13'b0000000010000,
	Receive_cmd_probe_1		=13'b0000000100000,
	Receive_cmd_probe_2		=13'b0000001000000,
	Receive_cmd_probe_3		=13'b0000010000000,
	Read_data_0				=13'b0000100000000,
	Read_data_1				=13'b0001000000000,
	Read_data				=13'b0010000000000,
	Dummy_clk				=13'b0100000000000,
	Dummy_clk2				=13'b1000000000000
}state,state_next;

//initial state = Init;
// tristate buffer for inout
assign SD_CMD = sd_cmd_in ? 1'bZ: reg_48[47];
assign SD_DAT = sd_dat_in ? 4'bZ: 4'b0000 ;

assign LEDG = {reg_8};
assign LEDR = {3'b000,state};

always_ff @(posedge CLK) begin
	if(RESET) begin
		SD_CLK <= 1;
		for (int i=0;i<128;++i) begin
			dataBuffer[i] <= 32'b0;
		end
	end else begin
		SD_CLK <= SD_CLK_next;
		
		for (int i=0;i<128;++i) begin
			dataBuffer[i] <= dataBuffer_next[i];
		end
	end
end


always_ff @(posedge CLK) begin
	if(RESET) begin
		state <= Init;
		reg_48 <= 0;
		reg_8 <= 0;
		//sd_cmd_message <= 0;
		for (int i=0;i<6;++i) begin
			sd_cmd_message[i] <= 8'b0;
		end
		interface_status <= 0;
		sd_cmd_in <= 1;
	end else begin
		state <= state_next;
		reg_48 <= reg_48_next;
		reg_8 <= reg_8_next;
		sd_cmd_message <= sd_cmd_message_next;
		for (int i=0;i<6;++i) begin
			sd_cmd_message[i] <= sd_cmd_message_next[i];
		end
		interface_status <= interface_status_next;
		sd_cmd_in <= sd_cmd_in_next;
	end
end

always_ff @(posedge CLK) begin
	if(c_reset | RESET) begin
		counter <= 0;
	end else begin
		counter <= counter_next;
	end 
end

always_comb begin
	for (int i=0;i<6;++i) begin
			sd_cmd_message_next[i] = sd_cmd_message[i];
	end
	sd_readdata = 32'hCCCC;
	c_reset = 1'b0;
	//sd_clk_tog = 0;
	for (int i=0;i<128;++i) begin
			dataBuffer_next[i] = dataBuffer[i];
		end
	counter_next = counter;
	state_next = state;

	sd_cmd_in_next = sd_cmd_in;

	reg_48_next = reg_48;
	reg_8_next = reg_8;

	interface_status_next = interface_status;
	interface_command_mes = 3'b0;

	//sd_cmd_in = 1; //set cmd line to output
	sd_dat_in = 1; //set data line to input
	SD_CLK_next = 1;
	//sd_clk_tog = 0;

	

/*	address 0 - 127 : 512 B data buffer
	
	address 10000001 interface command signal
	
	address 10001000 - 10001101
	6 addresses, each 8 bit sd cmd message

	address 10000010 sd cmd response
	address 10000100 interface status signal
*/
	if (sd_cs && sd_read) begin
		if (sd_addr[7]) begin
			//reading functional address
			if (sd_addr[3]) begin
				sd_readdata = {24'b0,sd_cmd_message[sd_addr[2:0]]};
			end else if (sd_addr[2]) begin
			//reading sd card response
				sd_readdata = {30'b0,interface_status};
				
			end else if (sd_addr[1]) begin
			//reading interface status
				sd_readdata = {24'b0,reg_8};
			end
		end else begin
			//reading data buffer
			sd_readdata = dataBuffer[sd_addr[6:0]];
		end
	end

	if (sd_cs && sd_write) begin
		if (sd_addr[7]) begin
			if (sd_addr[3]) begin
			//write sd card command message
				sd_cmd_message_next[sd_addr[2:0]] = sd_writedata[7:0];
				//sd_cmd_reg_load = 1;
			end else if (sd_addr[0])begin
			//write interface command signal
				interface_command_mes = sd_writedata [2:0];
			end
		end
	end

	//state case
	case (state)
		Init : begin
			sd_cmd_in_next = 1; 
			sd_dat_in = 1; //set data line to input
			SD_CLK_next = 1;
			//sd_clk_tog = 0;
			//SD_CMD = 1;
			//SD_DAT = 0;
			if (interface_command_mes == 3'b111) begin
				state_next = Idle;
				interface_status_next = 2'b01;
			end else 
				state_next = Init;
		end

		Idle : begin
			interface_status_next = 2'b01;
			case (interface_command_mes)
				3'b001: begin
					//send message routine
					c_reset = 1;
					SD_CLK_next = 0;
					//sd_clk_tog = 1;
					//sd_cmd_reg_load = 1;
					//sd_cmd_reg_input = sd_cmd_message [0];
					reg_48_next = {sd_cmd_message[0],sd_cmd_message[1],sd_cmd_message[2],
								   sd_cmd_message[3],sd_cmd_message[4],sd_cmd_message[5]};
					state_next = Send_cmd;
					sd_cmd_in_next = 0; 
					interface_status_next = 2'b00;
				end
				3'b010: begin
					//receive cmd state
					c_reset = 1;
					//sd_clk_tog = 1;
					//sd_cmd_rec_shift = 1;		
					SD_CLK_next = 0;
					state_next = Receive_cmd;
					interface_status_next = 2'b00;
				end
				3'b011: begin
					//read data state
					c_reset = 1;
					//sd_clk_tog = 1;
					SD_CLK_next = 0;
					state_next = Read_data_0;
					interface_status_next = 2'b00;
				end
				3'b100: begin
					//toggle 8 SD_CLK 
					c_reset = 1;
					//sd_clk_tog = 1;
					SD_CLK_next = 0;
					state_next = Dummy_clk;
					interface_status_next = 2'b00;
				end
				3'b101: begin
					c_reset = 1;
					//sd_clk_tog = 1;
					SD_CLK_next = 0;
					state_next = Dummy_clk2;
					interface_status_next = 2'b00;
				end
				3'b110: begin
					//probe read
					c_reset = 1;
					//sd_clk_tog = 1;
					SD_CLK_next = 0;
					state_next = Receive_cmd_probe_0;
					interface_status_next = 2'b00;
				end
				default : state_next = Idle;
			endcase
		end

		Send_cmd: begin
			//sd_clk_tog = 1;
			//sd_cmd_in = 0; //set cmd line to output
			counter_next = counter + 1;
			if (counter[0]) begin
				reg_48_next = {reg_48[46:0],1'b0};
				SD_CLK_next = 0;
 			end else
 				SD_CLK_next = 1;

			if (counter == 11'b00001011111) begin
				c_reset = 1;
				state_next = Idle;
				interface_status_next = 2'b01;
				sd_cmd_in_next = 1; 
				SD_CLK_next = 1;
			end else begin
				state_next = Send_cmd;
				//sd_clk_tog = 1;
			end
		end

		Receive_cmd: begin
			//sd_cmd_in = 1; // set cmd line to be input
			counter_next = counter + 1;
			if (counter[0]) begin
				reg_8_next = {reg_8[6:0],SD_CMD};
				SD_CLK_next = 0;
			end else
				SD_CLK_next = 1;

			if (counter == 11'b00000001111) begin
				c_reset = 1;
				state_next = Idle;
				interface_status_next = 2'b01;
				SD_CLK_next = 1;
			end else begin
				//sd_cmd_rec_shift = 1;
				state_next = Receive_cmd;
				//sd_clk_tog = 1;
			end
		end

		Receive_cmd_probe_0: begin
			counter_next = counter + 1;
			//sd_cmd_in = 1; // set cmd line to be input
			//sd_clk_tog = 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;
			//sd_cmd_rec_shift = 1;
			state_next = Receive_cmd_probe_1;
		end
		Receive_cmd_probe_1: begin
			counter_next = counter + 1;
			//sd_cmd_in = 1; // set cmd line to be input
			//sd_clk_tog = 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;

			reg_8_next = {reg_8[6:0],SD_CMD};
			if (counter >= 11'b00100111111) begin
				c_reset = 1;
				state_next = Init;
				interface_status_next = 2'b11;
				SD_CLK_next = 1;
			end else if (SD_CMD) begin
				state_next = Receive_cmd_probe_0;
			end else begin
				state_next = Receive_cmd_probe_2;
			end
		end
		Receive_cmd_probe_2: begin
			counter_next = counter + 1;
			//sd_cmd_in = 1; // set cmd line to be input
			//sd_clk_tog = 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;
			//sd_cmd_rec_shift = 1;
			state_next = Receive_cmd_probe_3;
		end
		Receive_cmd_probe_3: begin
			counter_next = counter + 1;
			//sd_cmd_in = 1; // set cmd line to be input
			//sd_clk_tog = 1;

			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;

			reg_8_next = {reg_8[6:0],SD_CMD};
			if ((!SD_CMD) || (counter >= 11'b00100111111)) begin
				
				if (SD_CMD) begin
					c_reset = 1;
					state_next = Init;
					interface_status_next = 2'b11;
					SD_CLK_next = 1;
				end else begin
					state_next = Receive_cmd;
					counter_next = 11'b00000000100;
				end
			end else begin
				state_next = Receive_cmd_probe_0;
			end
		end
		Read_data_0: begin
			//sd_clk_tog = 1;
			SD_CLK_next = 1;

			state_next = Read_data_1;
		end

		Read_data_1:begin
			//sd_clk_tog = 1;
			SD_CLK_next = 0;

			if (SD_DAT) begin
				state_next = Read_data_0;
			end else begin
				state_next = Read_data;
			end
		end

		Read_data:begin
			counter_next = counter + 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;
			if (counter[0]) begin
					dataBuffer_next[counter[10:4]][{(counter[3:2]),~counter[1],2'b11}] = SD_DAT[3];
					dataBuffer_next[counter[10:4]][{(counter[3:2]),~counter[1],2'b10}] = SD_DAT[2];
					dataBuffer_next[counter[10:4]][{(counter[3:2]),~counter[1],2'b01}] = SD_DAT[1];
					dataBuffer_next[counter[10:4]][{(counter[3:2]),~counter[1],2'b00}] = SD_DAT[0];
			end
			if (counter == 11'b11111111111) begin
				c_reset = 1;
				state_next = Idle;
				interface_status_next = 2'b01;
				SD_CLK_next = 1;
			end else begin
				state_next = Read_data;
				//sd_clk_tog = 1;
				
			end
		end

		Dummy_clk: begin
			counter_next = counter + 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;
			if (counter == 11'b00000001111) begin
				c_reset = 1;
				state_next = Idle;
				interface_status_next = 2'b01;
				SD_CLK_next = 1;
			end else begin
				state_next = Dummy_clk;
				//sd_clk_tog = 1;
			end
		end
		//default : /* default */;
		Dummy_clk2: begin
			counter_next = counter + 1;
			if (counter[0])
				SD_CLK_next = 0;
			else
				SD_CLK_next = 1;
			if (counter == 11'b00000110001) begin
				c_reset = 1;
				state_next = Idle;
				interface_status_next = 2'b01;
				SD_CLK_next = 1;
			end else begin
				state_next = Dummy_clk2;
				//sd_clk_tog = 1;
			end
		end
	endcase
	// if (//sd_clk_tog) 
	// 	SD_CLK_next = ~SD_CLK;
	// else 
	// 	SD_CLK_next = SD_CLK;
end
/*
reg_48 sd_cmd_register(.Clk(CLK),.SD_CLK(SD_CLK),.Reset(RESET),.Shift_In(1'b0),.Load(sd_cmd_reg_load),
	.Shift_En(sd_cmd_reg_shift),
	.D({sd_cmd_message[0],sd_cmd_message[1],sd_cmd_message[2],
		sd_cmd_message[3],sd_cmd_message[4],sd_cmd_message[5]}),
	.Shift_Out(sd_cmd_reg_output),.Data_Out());

reg_8 sd_cmd_rec_register(.Clk(CLK), .SD_CLK(SD_CLK),.Reset(RESET), .Shift_In(SD_CMD),.Load(1'b0),
	.Shift_En(sd_cmd_rec_shift),.D(),.Shift_Out(),.Data_Out(sd_cmd_rec_mes)
	);*/
endmodule
