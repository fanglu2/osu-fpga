module test;

// Timer setup
timeunit 10ns;
timeprecision 1ns;

logic Clk, Reset;
// Instantiating the adder controller
osu_fpga_toplevel main(.CLOCK_50(Clk), .KEY({3'b111, Reset}));

// Toggle the clock
// #1 means wait for a delay of 1 timeunit
always begin : CLOCK_GENERATION
	#1 Clk = ~Clk;
end

initial begin: CLOCK_INITIALIZATION
	Clk = 0;
end

// Testing begins here
// The initial block is not synthesizable
// Everything happens sequentially inside an initial block as in a software
// program
initial begin: TEST_MAIN

	// Reset
	Reset = 0;
#2	Reset = 1;
end


endmodule // test
