## **ECE 385**

### Fall 2017 Final Project

<br><br><br><br><br><br>

# **osu!fpga**

<br><br><br><br><br><br><br><br>

Fang Lu, Xutao Jumbo Jiang

Section AB8 (Wed 9 am)

TA: Byung Hoon Min

### Introduction

This project tries to construct the rhythm game *osu!* on the DE2-115 FPGA development board. Specifically, mimicking the design of modern computing systems, we built a set of interacting generic-purpose peripheral interfaces, device drivers, hardware accelerators and software frameworks to allow the implementation of game software with HID inputs and multimedia outputs at a relatively high level of abstraction efficiently. Osu is one example of such game software, yet the framework supports a wider variety, such as Maimai and SDVX, as we once considered.

### Design Overview

The system is designed to create layers of abstraction from the lowest physical hardware communication to the highest device-unaware software. The block diagram below shows the relationship between the major functional units in this system.

![Architecture Block Diagram](arch.svg)

The highest layer, the game software, is denoted by the center black square "Main Core". Running on a Nios II processor core, this unit only connects to the abstracted IO units through either shared memory with sub-processors or mapped memory to hardware modules. This core is mainly responsible for implementing game logic and send all painting commands.

The USB sub-processor interfaces with the on-board EZ-OTG chip through HPI connection and handles all the USB enumeration procedures. It is separated from the main core so as any issue with USB, such as the hot-plug routine, will not interrupt the graphics.

The multimedia core interfaces with the SD controller and audio controller. On one hand it provides abstraction over the SD control commands, and on the other hand it handles streamed audio from sdcard, which is a blocking process of continuous IO that cannot be executed on the main core.

In order to simplify the game logic, we also created a helper tool *OsuFs*, as denoted by the black processor square "OsuFs", to write songs, resources and beatmaps to the sdcard. It runs on a computer instead of the FPGA to pre-process some computation-intensive tasks that are infeasible on Nios or unnecessarily complicated on hardware, such as audio resampling and image dithering, and save the results to the sdcard in a simple format.

The graphics library, as denoted by the units above the main processor, takes painting commands from the main process and sends it to one of its painter modules. The painter modules fulfills the command by requesting changes to the frame buffer. The frame buffer structures the painted pixels into pixel buffer, background buffer, redraw control and color palette for efficient storage usage and lookup. Finally, the frame buffer reads itself in sequence and sends the pixels to VGA.

In short, for the lifecycle of a gameplay is roughly as follows: a standard osu beatmap is parsed and converted on a computer and saved to the sdcard. The multimedia core reads from the sdcard, caches the resources into memory and passes the converted beatmap to the main processor for playing. During gameplay, the graphics library draws the frame buffer as commanded by the main core, the USB core continuously sends mouse coordinates to the main core, and the multimedia core streams the audio by reading from sdcard into the audio controller.

The toplevel circuit block diagram is also shown below for reference, though it might not provide sufficient details about the system.

![Toplevel block diagram](toplevel.svg)

![Toplevel block diagram with Qsys expanded](toplevel-qsys.svg)

The detailed operation of each functional unit will be described in the next section.

### Descriptions of Functional Units

#### Input Driver: PS/2 Keyboard

> * **Module:** `input/ps2kb.sv`
> * **Input/Output:** the commented module prototype is reproduced below

```v
module ps2kb (
	// Clock
	input logic CLK, RESET,

	// Avalon-MM Slave Signals
	input  logic AVL_READ, // Avalon-MM Read
	input  logic AVL_WRITE, // Avalon-MM Write
	input  logic AVL_CS, // Avalon-MM Chip Select
	input  logic [3:0] AVL_BYTE_EN, // Avalon-MM Byte Enable
	input  logic [1:0] AVL_ADDR, // Avalon-MM Address
	input  logic [31:0] AVL_WRITEDATA, // Avalon-MM Write Data
	output logic [31:0] AVL_READDATA, // Avalon-MM Read Data

	// PS/2 Conduit
	inout wire PS2_CLK, PS2_DATA
);
```

This module interfaces with the keyboard connected to the on-board PS/2 socket. It contains an Avalon slave occupying 4 addresses for returning the results.

This module returns the keystrokes from the keyboard as "keyboard events" to the accessing master. Address 0 and 1 contains the "keydown" events in the form of the keycode being pressed, and address 2 and 3 contains the "keyup" events in the form of the keycode being released. All four addresses can be read or written and behaves similarly to mailbox. Each time the module receives a keystroke from the keyboard, it will write the event into the first available register (which contains 0). The accessing master should set any event register to 0 after reading to signal "acknowledgment" and free the register for this module. If the hardware cannot find an available register to write to on an event, the event will be discarded and cannot be retrieved. The module will not modify any non-zero registers, unless it is being reset, which frees all registers back to 0.

To implement the PS/2 protocol, this module contains a state machine to serially read the `PS2_DAT` on each positive edge trigger of `PS2_CLK` as pulled by the other device. At the end of the serial read, the machine either saves the value as a new event, changes the current event type due to a control code was received, or resets itself due to parity error. The state machine diagram is shown below.

![PS/2 Keyboard State Machine](statemachine_ps2kb.svg)

#### Input Driver: SD Card Reader

This hardware module works as the interface for software and SD card. It is directly connected to the IO pins of the SD card socket: SD_CLK, SD_CMD, SD_DATA[3:0].

```v
module final_sd_interface (
	input logic CLK,    // Clock
	input logic RESET, // RESET

	//Avalon
	input logic [7:0] sd_addr,
	input logic sd_cs, sd_read, sd_write,
	input logic [31:0]  sd_writedata,
	output logic [31:0] sd_readdata,

	//export
	output  logic SD_CLK,
	inout wire SD_CMD,
	inout wire [3:0] SD_DAT
);
```

`SD_CLK` is the output signal, which the SD card module pulls to control the IO behavior of the SD card.

`SD_CMD` is an input/output wire that is used to communicate protocols and commands. The SD card reader send a command through `SD_CMD` line to the SD card, and the SD card response on the same line back to the SD card reader.

`SD_DATA` is a 4-bit serial port used to transfer read data. The port is bidirectional so that SD card could be read and written. But in our case, we only need to read data from SD card, so the lines are constantly set to input.

The hardware implements a state machine to accepts commands from software via Avalon bus, and perform operations to the SD card. It stores the command registers, response registers, and data buffer, to temporarily hold the bits.

The specific command sent is controlled by the software. The software put the command in command register, and issue a send command command to the reader. Then the software issues a receive response command. The SD card reader is going to probe the `SD_CMD` line (maybe high-Z at beginning) until SD card response with correct header, and then put the response into the response register waiting for the software to fetch response. If it is a data transfer (read in our case) process, the SD reader would probe SD_DATA lines (could be high-Z) until `SD_DATA` line comes with the heading 0 bit. Then the SD reader would accept a stream of 512 bytes of data, and put them into the data buffer waiting to be fetched by the software.

The SD card protocol and library comes from DE2-115 board demonstration. 512 bytes of data is a block for the SD card. For the library given, only single block read command would be issued.

State machine diagram for SD Controller:

![SD Controller State Machine](statemachine-sdcard.svg)

#### Output Driver: Audio DAC Interface

This hardware module is actually an interface for the Avalon bus and an audio driver we found on the Internet.

```v
module audio(
	input logic CLK,RESET,
	//Avalon
	input logic [8:0]AVL_ADDR,
	input logic AVL_CS,
	input logic AVL_RD,AVL_WR,
	input logic [31:0] AVL_WDATA,
	output logic [31:0] AVL_RDATA,

	//export signals from audio driver
	output logic AUD_DACDAT, I2C_SCLK,
	input logic  AUD_ADCDAT,

	inout wire AUD_BCLK,
				AUD_ADCLRCK,
				AUD_DACLRCK,
				I2C_SDAT
);
```

The audio hardware accepts audio data from the software and store it into audio buffer, and automatically feed audio data into the audio driver. The interface hardware that we wrote has two buffer of length 512 bytes, well, as you could guess, to store a block of audio data from Avalon bus. We used double buffering in case that SD card read is not that fast enough.

Here is the file for the audio buffer, which is actually a M9K memory declaration and simple register operation

```v
module byte_enabled_simple_dual_port_ram
	#(parameter int
		ADDR_WIDTH = 6,
		WIDTH = 8
)
(
	input [ADDR_WIDTH-1:0] waddr,	//write addr
	input [ADDR_WIDTH-1:0] raddr,	//read addr
	input [WIDTH-1:0] wdata, 		//write data
	input we, clk,					//write enable
	output reg [WIDTH - 1:0] q 		//read output
);
```

The audio driver we got consists of two parts, one for handling DAC and ADC streams for the audio hardware on the board, the other send command to change settings of the audio hardware on the board via I2C bus.

Here is the audio driver that we connect to:

```
module Audio_Controller(
	// Inputs
	CLOCK_50,
	reset,

	clear_audio_in_memory,
	read_audio_in,

	clear_audio_out_memory,
	left_channel_audio_out,
	right_channel_audio_out,
	write_audio_out,

	AUD_ADCDAT,

	// Bidirectionals
	AUD_BCLK,
	AUD_ADCLRCK,
	AUD_DACLRCK,

	// Outputs
	left_channel_audio_in,
	right_channel_audio_in,
	audio_in_available,

	audio_out_allowed,

	AUD_XCK,
	AUD_DACDAT
);
input				CLOCK_50;
input				reset;

input				clear_audio_in_memory;
input				read_audio_in;

input				clear_audio_out_memory;
input		[AUDIO_DATA_WIDTH:1]	left_channel_audio_out;
input		[AUDIO_DATA_WIDTH:1]	right_channel_audio_out;
input				write_audio_out;

input				AUD_ADCDAT;

// Bidirectionals
inout				AUD_BCLK;
inout				AUD_ADCLRCK;
inout				AUD_DACLRCK;

// Outputs
output	reg			audio_in_available;
output		[AUDIO_DATA_WIDTH:1]	left_channel_audio_in;
output		[AUDIO_DATA_WIDTH:1]	right_channel_audio_in;

output	reg			audio_out_allowed;

output				AUD_XCK;
output				AUD_DACDAT;
```

Signals with AUD prefix are exported directly to the audio hardware. The rest of signals are for audio data I/O. We didn't use any of the ADC ports since we are just outputting audio data. `write_audio_out` is the audio data output port. `audio_out_allowed` indicates whether the audio driver could read in a sample of binary data yet. We need this kind of buffering and checking-if-available mechanism because the audio we played are at a rate that is much slower than processing clock. And the outputting sample rate we used is 44100 Hz/s.

The audio driver could actually read left channel and right channel each at a sample length of 32 bits, but wave audio file actually is 16 bits for each sound channel. We add padding zeros at the end and sign extend at the beginning so that the sound wouldn't be too small or too loud.

I2C part:

```v
module avconf (
	//	Host Side
	CLOCK_50,
	reset,
	AUD_VOL,

	//	I2C Side
	I2C_SCLK,
	I2C_SDAT
);
//	Host Side
input		CLOCK_50;
input		reset;
//	I2C Side
output		I2C_SCLK;
inout		I2C_SDAT;
//  audio_volumn control
input 		[6:0] AUD_VOL;
```

This module keeps all the configurations for the audio hardware and push them via I2C port. We create a port on the volume setting register so that we could change the volume of the audio from the software.

State machine diagram for the audio controller:

![Audio Controller State Machine](statemachine_audio.svg)

#### Output Driver: the Graphics Library

The graphic library is designed for high-performance graphics at `640x480@59fps`. It is based on frame buffer for more flexible drawing and varied graphics. Since the VGA pixel clock for this resolution is 25 MHz while our main clock frequency is only 50 MHz, it becomes a challenge to update the pixels no slower than their being read. On the SRAM, the only storage element on board that is large enough and fast enough, each pixel can only be accessed about 1.5 times per frame theoretically if the SRAM is being accessed every clock cycle. This module creates one strategy to reduce the necessary amount of pixels redrawn in each frame.

##### Frame Buffer

> * **Module**: `gl/gl_frame_buffer.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_frame_buffer (
	// Clock
	input logic CLK, RESET,
	// VGA Read
	input logic VGA_REQ,
	input logic [9:0] VGA_X, VGA_Y,
	output logic [23:0] VGA_RGB,

	// GL Read
	input logic GL_REQ,
	input logic [9:0] GL_X, GL_Y,
	input logic [19:0] GL_ADDR,
	output logic [15:0] GL_DATA,
	output logic GL_READY,

	// Paint Write
	input logic PAINT_REQ,
	input logic [9:0] PAINT_X, PAINT_Y,
	input logic [15:0] PAINT_RGB16,
	output logic PAINT_READY,

	// Avalon Write
	input logic AVL_REQ,
	input logic [19:0] AVL_ADDR,
	input logic [15:0] AVL_DATA,
	output logic AVL_READY,

	// Frame-switch control
	// Active: being drawn, Inactive: being displayed
 	input logic BUF_ACTIVE,

	// Redraw control Conduit
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_IN,
	output logic RC_WE,
	input logic RC_DATA_OUT,

	// Palette Conduit
	output logic [7:0] PL_ADDR,
	input logic [23:0] PL_DATA_OUT,

	// SRAM Conduit
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N
);
```

The frame buffer processes the updating and retrieval of pixels. It flags each pixel as either "foreground" or "background". A foreground pixel is drawn by one of the painter module at each frame, while a background pixel is pre-loaded into the background buffer. Any dynamic graphics are foreground pixels which can be drawn at high performance on each frame, while any background and static images are background pixels which can only be drawn at low perform during loading periods. There exists 2 pixel buffers for foreground pixels since they have to be udpated at each frame, but only 1 buffer for background pixels as they remain unchanged throughout rendering. In this way, during each frame, the total redraw area contains only foreground pixels, and it takes no time to set or reset any background pixel.

In memory, the information about each pixel is stored in different locations as described in the table below. First, whether a pixel is foreground or background is stored as 1-bit boolean flag on a piece of on-chip memory. Both pixel buffer needs separate redraw flags. This will take 1 Mib of M9K out of 3.888 Mib. The foreground pixels are stored as 16-bit RGB, while the background are stored as 8-bit color index in another location on SRAM. The palette for these index colors are stored in another piece of on-chip memory in full 24-bit RGB. The remaining space on SRAM is left for storing image resources that will be used by `gl_painter_image` to draw images and fonts.

| Device	| Address			| Content			| Format	|
|-----------|-------------------|-------------------|-----------|
| SRAM		| 0x00000 - 0x9FFFF	| Pixel buffer		| rgb565	|
| SRAM		| 0xA0000 - 0xC7FFF	| Backgound buffer	| pal8		|
| SRAM		| 0xC8000 - 0xFFFFF	| Image resources	| rgb565	|
| M9K (1)	| 0x00000 - 0xFFFFF	| Redraw Flags		| bool		|
| M9K (2)	| 0x00 - 0xFF		| Color Palette		| rgb24		|

The mapping from coordinates to memory offset is calculated in the following way:

```v
logic [9:0] X, Y;
logic BUF_ACTIVE;
logic [19:0] FG_ADDR, BG_ADDR;
assign FG_ADDR = {X, Y[8:0], BUF_ACTIVE};
assign BG_ADDR = {2'b0, X[9:1], Y[8:0]} + 20'hA0000;
```

The frame buffer handles 4 sources of access requests: read request from VGA for pixel rendering, read request from a GL painter for reading from image resources or blending on existing pixels, write request from a GL painter, and write request from the Avalon interconnect for writing to background pixels or image resources by software. The connections for painter write and Avalon write are quite straightforward, but the read requests require more careful handling.

For the VGA read, the full request will have four steps: querying the redraw flags, generate and register the right SRAM query address, wait for the SRAM to complete operation, and query the palette if necessary. However, the VGA will create a request every 2 `CLK` cycles and cannot wait for the full query for each pixel. As the three memory queries go to different locations, this module creates a pipeline to solve this issue. The three memory devices can then be queried at the same time, and the response can be served at full 50 MHz with 4 cycles of latency.

The GL read is very similar to the VGA read in terms of datapath. However, since the `gl_sram` controller may not respond to the read request when a VGA read request occupies the SRAM, the pipeline will not work. However, since GL read does not require the same response rate as VGA does, the GL read operation will simply wait for every operation sequentially and return after all of them have been completed.

> * **Module**: `gl/gl_vga.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_vga (
	// Clock
	input CLOCK_50, RESET,

	// VGA Conduit
	output logic VGA_HS, VGA_VS, VGA_BLANK_N, VGA_SYNC_N, VGA_CLK,
	output logic [7:0] VGA_R, VGA_G, VGA_B,

	// Frame buffer
	output logic FB_REQ,
	output logic [9:0] FB_X, FB_Y,
	input logic [23:0] FB_RET,

	// Informational
	input logic RENDER_BUFFER,
	output logic VGA_INTERFRAME
);
```

This module is the VGA controller. It reads pixels from the frame buffer pipeline in advance and sends the pixel to the on-board VGA DAC. It also generates the corresponding clock, horizontal sync, vertical sync and blanking signals. When the VGA enters vertical sync, this module also notifies `gl_mgr` to perform frame finalization and buffer switching logic.

> * **Module**: `gl/gl_sram.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_sram (
	// Clock
	input logic CLK, RESET,

	// Read 1
	input logic read1_req,
	input logic [19:0] read1_addr,
	output logic [15:0] read1_data,
	output logic read1_ready,

	// Read 2
	input logic read2_req,
	input logic [19:0] read2_addr,
	output logic [15:0] read2_data,
	output logic read2_ready,

	// Write 1
	input logic write1_req,
	input logic [19:0] write1_addr,
	input logic [15:0] write1_data,
	output logic write1_ready,

	// Write 2
	input logic write2_req,
	input logic [19:0] write2_addr,
	input logic [15:0] write2_data,
	output logic write2_ready,

	// SRAM Conduit
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N
);
```

This module wraps the SRAM and handles the arbitration of 2 read and 2 write signals. The SRAM is clocked at the `CLK` frequency, i.e. 50 MHz since unstable access starts to arise when clocked at 100 MHz. For each cycle only one operation is performed on the SRAM. The four incoming requests, `read1_req`, `write1_req`, `read2_req`, `write2_req` are processed in the priority of that order. I.e., if `read1_req` is constantly reading for consecutive cycles, `write1_req` will not be processed even though it has been waiting for a long time, not like a queue. This behavior is to ensure that `read1_req` is guaranteed to fulfill in the next clock edge since it is connected to the VGA requeset by `gl_frame_buffer`.

> * **Module**: `gl/gl_redraw_cache.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_redraw_cache
	#(parameter int
		DATA_WIDTH = 1,
		ADDRESS_WIDTH = 20
	 ) (
		input [ADDRESS_WIDTH-1:0] addr1,
		input [ADDRESS_WIDTH-1:0] addr2,
		input [DATA_WIDTH-1:0] data_in1,
		input [DATA_WIDTH-1:0] data_in2,
		input we1, we2, clk,
		output reg [DATA_WIDTH-1:0] data_out1,
		output reg [DATA_WIDTH-1:0] data_out2
);
```

This module is derived from the Quartus Prime SystemVerilog Template *True Dual-Port RAM with single clock and different data width on the two ports*. It creates the memory for the redraw flags mentioned in `gl_frame_buffer` by synthesizing into a M9K memory megafunction.

> * **Module**: `gl/gl_palette.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_palette
	#(parameter int
		DATA_WIDTH = 24,
		ADDRESS_WIDTH = 8
	 ) (
		input [ADDRESS_WIDTH-1:0] addr1,
		input [ADDRESS_WIDTH-1:0] addr2,
		input [DATA_WIDTH-1:0] data_in1,
		input [DATA_WIDTH-1:0] data_in2,
		input we1, we2, clk,
		output reg [DATA_WIDTH-1:0] data_out1,
		output reg [DATA_WIDTH-1:0] data_out2,
		input logic [7:0] background_dim
);
```

This module is derived from the Quartus Prime SystemVerilog Template *True Dual-Port RAM with single clock and different data width on the two ports*. It creates the memory for the color palette mentioned in `gl_frame_buffer` by synthesizing into a M9K memory megafunction.

In addition, this module also takes the parameter `background_dim` and transparently dims the entire palette by the value provided. A `background_dim` value of 128 results in no dimming at all (exact inputs returned) while a value of 0 results in all black. Values larger than 128 can cause undefined behavior.

The background dimming works by multiplying the RGB components individually with the supplied factor. This implies that the colors store are in strict rgb24 format and any other data formats will likely be corrupted by the dimming behavior (unless the factor is 128 will keeps the values intact).

##### Painter Modules

The painter modules draws specific shapes and graphics to the foreground. They all read parameters and signals from `gl_mgr` and draws into `gl_frame_buffer`. They share similar behaviors and module prototypes.

```v
module gl_painter_generic (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	// input logic [9:0] X, Y,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory read conduit
	output logic fb_GL_REQ,
	output logic [19:0] fb_GL_ADDR,
	input logic [15:0] fb_GL_DATA,
	input logic fb_GL_READY,

	// Memory write conduit
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache conduit
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

The module will start drawing as soon as `EN` goes high, or reset itself otherwise. The `PAINT_BUFFER` is passed in case the painter needs to calculate SRAM addresses for reading pixels. Once the drawing is completed, the painter will bring `DONE` to high when it is still enabled.

All the painters should optimally use the same amount of cycles as there are pixels to read and write. However, practically some cycles has to be used for arithmetic approximation since the direct implementation of operations like division, square root and trigonometry functions can be complicated and inefficient. The description of each module will mention such behavior when reporting "wasted cycles".

> * **Module**: `gl/painters/gl_painter_rect.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_painter_rect (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[9:0] X0, Y0, X1, Y1,
	input logic[15:0] C0, C1, CSTROKE,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

This module draws a filled rectangle with upper-left corner `(X0, Y0)` and lower-right corner `(X1, Y1)`. Violating the ordering of the coordinates will cause undefined behavior, typically the painter filling outside regions. The gradient parameter are not implemented. Both boundaries are inclusive, and the outermost 1 pixel at the boundary will be painted with the stroke color `CSTROKE` instead of the fill color `C0`. To disable stroking, simply set the stroke color as the same as fill color.

This module does not waste any cycles. The state diagram for this painter is shown below.

![Rectangle Painter State Machine](statemachine_painter_rect.svg)

> * **Module**: `gl/painters/gl_painter_circle.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_painter_circle (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[15:0] X0, Y0, R,
	input logic[15:0] C0, C1, CSTROKE,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

This module draws a filled circle centered at `(X0, Y0)` with radius `R`. The gradient parameter are not implemented. The radius is inclusive, and the outermost 1 pixel at the boundary will be painted with the stroke color `CSTROKE` instead of the fill color `C0`. To disable stroking, simply set the stroke color as the same as fill color.

This module will waste a total of $\frac{2 - \sqrt{2}}{2} R$ cycles doing Pythagorean calculation. The state diagram for this painter is shown below.

![Circle Painter State Machine](statemachine_painter_circle.svg)

> * **Module**: `gl/painters/gl_painter_ring.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_painter_ring (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[15:0] X0, Y0, R, W,
	input logic[15:0] CSTROKE,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

This module draws an approximated ring centered at `(X0, Y0)` with outer radius `R` and inner radius `R-W`. It is implemented in a way very similar to that of `gl_painter_circle`. This module is designed to draw very thin rings, and only the outer radius is calculated exactly. The inner radius is approximated by the outer radius and `W` and the error of such approximation will become more significant when `W` increases.

This module will waste a total of $\frac{2 - \sqrt{2}}{2} R$ cycles doing Pythagorean calculation. The state diagram for this painter is shown below.

![Ring Painter State Machine](statemachine_painter_ring.svg)

> * **Module**: `gl/painters/gl_painter_polygon.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_painter_polygon (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[15:0] XT, YT, XB, YB, XL, YL, XR, YR,
	input logic[15:0] C0, C1,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

This module draws a convex quadrilateral with the given top, bottom, left and right vertices. Violating the ordering of the coordinates will cause undefined behavior. This module was designed draw larger, more complex polygons with several calls to `gl_painter_polygon`, or to approximate shapes with non-circularly curved surfaces by differentiating the curve into multiple polygons.

Although this modules uses `gl_line_scanner` for slope approximation, it approaches the edge from the painted side. So this painter does not waste any cycles. The state diagram for this painter is shown below.

![Polygon Painter State Machine](statemachine_painter_polygon.svg)

> * **Module**: `gl/painters/gl_painter_image.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_painter_image (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic [19:0] IMG_BASE,
	input logic [9:0] W, H,
	input logic [9:0] X[6], Y[6],

	// Transparency
	input logic [15:0] TRANSPARENCY,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory read connection
	output logic fb_GL_REQ,
	output logic [19:0] fb_GL_ADDR,
	input logic [15:0] fb_GL_DATA,
	input logic fb_GL_READY,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
```

This module copies a rectangular image to at most 6 locations. The source image must be located at `BASE` on the SRAM, encoded in 16-bit RGB (rgb565), and its pixels arranged in row-major order. A partial of the source image may be drawn given that the subimage has the full width of the source image, the `BASE` address points to the first pixel of a row, and the height is calculated so as not to go beyond the bounds of the source image. The color specified by `TRANSPARENCY` will be treated as the transparent color, and all the pixels in the source image equal to the specified color will be treated as transparent pixel and skipped for all six possible destinations. The destination location specifies the upper-left corner of the copied image. Not all six destinations are required. A destination location of -1 (10'h3FF) in both X and Y denotes an unused location. All used destinations must use the first available space, as the painter will automatically treat all subsequent destinations as unused when it encounters the first one.

Although transparent pixels are not painted, they still needs to be fetched. For each pixel, only one fetch is performed for all six possible locations to reduce reading waits. The state diagram for this painter is shown below.

![Image Painter State Machine](statemachine_painter_image.svg)

> * **Module**: `gl/utils/gl_line_scanner.sv`
> * **Input**: `CLOCK`, `signed [15:0] X1, Y1, X2, Y2, X, Y`
> * **Output**: `SIDE`

This module calculates if the point `(X,Y)` lies to left of the line through `(X1, Y1)` and `(X2, Y2)` or not. The result `SIDE` will be available on the next clock edge. This module uses the following formula for linear function to perform the calculation. Since the calculation involves a lot of combinational arithmetics, the result is registered to shorten the combinational path.

$$\frac{x-x_1}{y-y_1} = \frac{x_2-x_1}{y_2-y_1}$$

$$\Delta x=(x-x_1)  (y_2-y1) - (y-y_1)(x_2-x_1)$$

This module is used by `gl_painter_polygon` to approximate the edge of the polygon.

##### Organizers and Interfaces

> * **Module**: `gl/gl_mgr.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_mgr (
	// Clock
	input logic CLOCK, RESET,

	// Status Control
	output logic GL_FRAME_FINISHED, GL_TIMEOUT,
	output logic[9:0] GL_DRAWTIME,

	// GL Commands and arguments
	input logic[3:0] GL_CMD,
	input logic[31:0] GL_ARG1, GL_ARG2, GL_ARG3, GL_ARG4,
	input logic[31:0] GL_ARG5, GL_ARG6, GL_ARG7, GL_ARG8,
	input logic GL_EXEC,
	output logic GL_DONE,

	// Background processing
	input logic [7:0] BG_DIM,
	input logic [15:0] TRANSPARENCY,

	// Avalon connection
	input logic AVL_REQ,
	input logic [19:0] AVL_ADDR,
	input logic [15:0] AVL_DATA,
	output logic AVL_READY,

	input logic AVL_PLT_RD, AVL_PLT_WR,
	input logic[7:0] AVL_PLT_INDEX,
	output logic[23:0] AVL_PLT_RD_COLOR,
	input logic[23:0] AVL_PLT_WR_COLOR,

	// SRAM connection
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N,

	// VGA connection
	output logic VGA_HS, VGA_VS, VGA_BLANK_N, VGA_SYNC_N, VGA_CLK,
	output logic [7:0] VGA_R, VGA_G, VGA_B
);
```

This module is essentially the toplevel of the graphics library. It reads commands from the Avalon interface and routes them to the corresponding painters. It instantiates `gl_frame_buffer`, `gl_sram`, `gl_redraw_control`, `gl_palette` and all the painters and wires them together. It controls the switching of the 2 pixel buffers. It also reports the current status back to the Avalon interface.

> * **Module**: `gl/gl_avalon_intf.sv`
> * **Input/Output**: the commented module prototype is reproduced below

```v
module gl_avalon_intf (
	// Avalon Clock Input
	input logic CLOCK, RESET,

	// GL Controller Slave
	input  logic AVL_GL_READ,
	input  logic AVL_GL_WRITE,
	input  logic AVL_GL_CS,
	input  logic [3:0] AVL_GL_BYTE_EN,
	input  logic [3:0] AVL_GL_ADDR,
	input  logic [31:0] AVL_GL_WRITEDATA,
	output logic [31:0] AVL_GL_READDATA,

	// SRAM Slave
	input  logic AVL_SRAM_READ,
	input  logic AVL_SRAM_WRITE,
	input  logic AVL_SRAM_CS,
	input  logic [3:0] AVL_SRAM_BYTE_EN,
	input  logic [19:0] AVL_SRAM_ADDR,
	input  logic [31:0] AVL_SRAM_WRITEDATA,
	output logic [31:0] AVL_SRAM_READDATA,
	output logic AVL_SRAM_WAITREQ,

	// Palette Slave
	input  logic AVL_PLT_READ,
	input  logic AVL_PLT_WRITE,
	input  logic AVL_PLT_CS,
	input  logic [3:0] AVL_PLT_BYTE_EN,
	input  logic [7:0] AVL_PLT_ADDR,
	input  logic [31:0] AVL_PLT_WRITEDATA,
	output logic [31:0] AVL_PLT_READDATA,

	// SRAM Conduit
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N,

	// VGA Conduit
	output logic VGA_CLK,
	output logic [7:0] VGA_R, VGA_G, VGA_B,
	output logic VGA_SYNC_N, VGA_BLANK_N, VGA_VS, VGA_HS
);
```

This module exports the control signals of the graphics library to the mapped registers on the Avalon interconnect. This module contains 3 Avalon slaves: the GL control registers slave, the palette slave and the SRAM slave.

The GL control registers slave allows the software to send commands to the GL to perform painting as well as checking for GL status. A total of 16 addresses is allocated, each with size 32 bits. Reading and writing these registers are all combinational. The addresses are assigned as follow:

| Addr	| Meaning				| Permissions	|
|-------|-----------------------|---------------|
| 0		| Status				| Readonly		|
| 1		| Total draw time		| Readonly		|
| 2		| Draw command			| Read-write	|
| 3		| Background dimming	| Read-write	|
| 4-7	| Unused				| No access		|
| 8-16	| Painter arguments 1-8	| Read-write	|

Before issuing any new commands, the software should first make sure no command is in progress by waiting for `GL_STATUS_DONE` flag of status going low. The `GL_TIMEOUT` flag should also be checked. To issue a drawing command, first set the args registers. Then set the draw command to the desired drawing command. Drawing will start instantly. The software could then wait for `GL_STATUS_DONE` flag to go low again, but postponing this waiting till the beginning of the next command is recommended so that software could utilize the meantime. To issue the next command, set the command register to the new command. If the command is the same as the previous one, the command must first be set to `GL_CMD_NOOP` (0) so that the painter is reset.

After finishing all drawing commands, the software should send `GL_CMD_FIN` (16) to the command to indicate that it is safe to finalize the current frame. The frame will be finalized when VGA is in vertical blanking and the current command is `GL_CMD_FIN`. The `GL_STATUS_FRAME_DONE` will be brought to high when it happens. If the frame is still not finalized after VGA vertical blanking ends, the frame will be forced to finalize and a new frame started. In this case the `GL_STATUS_FRAME_TIMEOUT` will be set to signal this exception.

#### Software Processor: USB Core

The USB core interfaces to the HPI bus to access the EZ-OTG USB controller chip. It encapsulates all the USB enumeration and hot-plug routines necessary to connect one single HID mouse directly. All the HPI controller and USB enumeration logic are mostly taken from lab8, with the modification to parse the packets into mouse movements and buttons.

This core is separated from the main core to reduce overhead and prevent exceptions from USB from interrupting the graphics. The parsed mouse coordinates is then passed to the main core using the interprocess shared memory. The USB core will update the mouse coordinates whenever it gets updates from the mouse, and the main core simply polls the coordinate whenever it needs it.

#### Software Processor: Multimedia Core

This core works as the data transferor that handles the SD card read to give the main core the data that it wants. All SD card protocols and command libraries are used in this core. It also handles audio. It play specific audio request from the main core to stream play the audio specified. It also supports audio mixing when a second channel audio request is also present.

A block of M9K on-chip memory is connected to the Avalon bus to share the data and transfer command between the processor cores that we have. Most communication happens between the MPEG core and the main processing core. The communication protocols are encapsulated in the `interprocess.h` file, which marks what address means what for the process cores, and also the instructions to access the M9K on-chip memory. This header file is shared over all the cores.

There is also a space in the `interprocess.h` for audio buffer that would be send to the audio interface. Writing from registers to M9K is much faster than writing to SDRAM.

The way we communicate is: the main core sends a request by setting a data (usually a pointer, because Avalon max width 32 bits is the same as an address length) to a specifically designated address, and set the addresses afterwards as parameters. The MPEG core is constantly probing the meaningful addresses of functions. When it detects a request, it goes into the routine specified.

The routines we have are:

Audio Play: given the audio data SD address and number of SD blocks, it could start streaming audio play, and it could also handle mixing request at the playing time

General Read: given SD card block address, number of blocks, and destination address, it could read the specified data from SD card to the address on SDRAM

FX Audio Play: given the FX audio data SD address and number of SD blocks, it could start streaming FX audio play

Image Read: given the image SD address, the width of the image, the height of the image, and the destination address on SRAM, it could read a image of the specified width and height from the SD card to the specific SRAM address because image resources are on SRAM (faster than SDRAM)

Background Image Read: given a SD card block address, it could read the palettes of the background image and image matrix (full of indexes to palettes) to the background image address on SRAM. The length of palettes, the size of image, and the SRAM addresses are previously fixed.

#### Software Processor: Main Core

This is the main process core of our project. It is responsible for all the graphic drawing handling, file data analyzing, input handling, and main game logic handling.

File Data Analysis: The main core knows how the data in the SD card is arranged. It gets data by requesting the MPEG core. When it gets a block's data, it would get where other data resources are located on the SD card. For example, when the meta data block is read, aka. block 0, it could know the SD block addresses of a bunch of resources.

Input Handling: The main core fetches mouse and keyboard data at the start of each frame, and response to those inputs. In the song selection page, the main core could response to up/down arrow keys to select between songs (in the meanwhile request the MPEG core to load the background and play the preview audio), enter key to start playing a song. And the mouse is what the user uses to play the game, whether the mouse is moving round or clicking

Game Logic Handling: The main core handles all the gaming logic. When loading a song resources, it will load the list of objects that would be drawn as the storyboard and time-line. When the core keeps track of the current time relative to the start time of playing, the logic looks up the time-line to determine whether next object should be drawn, and looks back into the objects that are currently in the frame to see if one object should disappear. Besides this, it also produce responses to user's mouse input during playing. Whether the user click on the object and how accurate the click is according to the song determine what feedback image to show and which FX sound to play.

It also keeps track of the score during playing.

Graphic Drawing: Game logic determines what to draw on the current frame. The main core goes through all the objects that should be drawn on the frame and determine how to draw the object. Sometimes the object depends on the current time of the frame. It then send draw requests to the GL hardwares to draw the objects into frame buffer in sequence. Also it perform some tricky drawings, like, the slider, it actually consists of a string of rings one over another.

#### SDCard Storage Helper: osu filesystem

The osu filesystem (osufs) is a helper tool for storing beatmaps on the sdcard. It is a C++ program that runs on a computer running a UNIX-like system (tested under MacOS and Ubuntu) that does preprocessing such as image dithering and audio resampling.

This program was created because performing such tasks on the FPGA is either inefficient or overcomplicated. To decode a MP3-encoded audio into PCM samples, using [libmad](https://www.underbit.com/products/mad/) on Nios II produced 5 seconds of audio after 10 minutes of decoding. Hardware decoding is far from trivial and goes beyond the proposal of this project. At the same time, none of the FAT libraries available on Nios for sdcard could properly handle large wave files.

This program efficiently solves the above program by doing preprocessing. First, as suggested by its name, this program creates a custom filesystem on the raw SD device to workaround the FAT issue. Taking the advantage of file IO to block devices on POSIX systems, the program is able to treat the SD card not as a directory, but as a block of continuous memory -- which the FPGA can also do reliably. Then the program can perform all the conversion fairly trivially using existing tools like ffmpeg and dump the raw PCM samples or pixels onto the sdcard. Detailed operations perform by this program is documented below.

> **main**
>
> * `int main(int argc, char *argv[])`

This file define the entry point for the program. It parses user inputs from the console and issues  function calls to the corresponding modules.

> **blkio**
>
> * `mdev *blkio_open(const char *filename);`
> * `void blkio_close(mdev *dev);`
> * `void blkio_read(mdev *dev, uint32_t addr, void *buf);`
> * `void blkio_write(mdev *dev, uint32_t addr, void *buf);`


This file encapsulates IO system calls such as `open` and `pwrite` into a sdcard IO interface.

> **osufs**
>
> * `osu_meta osu_read_meta(mdev *dev);`
> * `osu_song osu_read_song(mdev *dev, int idx);`
> * `void osu_write_meta(mdev *dev, osu_meta *meta);`
> * `void osu_write_song(mdev *dev, int idx, osu_song *song);`
> * `void osu_init_device(mdev *dev);`

This file define the basic `struct` for storing metadata and song information. This file is also included in the main core for parsing. This file defined functions for reading and writing these `struct`s from and to the device.

> **wavenc**
>
> * `uint32_t wav_write(const char *filename, mdev *dev, uint32_t addr);`

This file imports an arbitrary audio file onto the specified address of the sdcard. It first does audio conversion and resampling with ffmpeg into 44100 Hz 2 channel signed 16-bit little-endian integer PCM (pcm_s16le) wave file. Then the program reads the binary content of the wave file and copies its RIFF wave data section onto the sdcard.

> **imgenc**
>
> * `uint32_t img_write_dither128(const char *filename, mdev *dev, uint32_t addr);`
> * `uint32_t img_write_dither64(const char *filename, mdev *dev, uint32_t addr, uint16_t *w, uint16_t *h);`
> * `uint32_t img_write_rgb16(const char *filename, mdev *dev, uint32_t addr, uint16_t *w, uint16_t *h);`

This file imports arbitrary image files onto the specified address of the sdcard using different pixel formats.

For the background image of each song, `img_write_dither128` is called. This function calls ffmpeg to generate a 128-color palette from the colors in the image and convert the image file into indexed color using that palette with Floyd-Steinberg dithering algorithm to reduce color banding. The program then opens the product PNG image using libpng to get the palette and pixels encoded in the image and dump them into the sdcard.

For the UI resources such as buttons and icons, `img_write_rgb16` is called. This function calls ffmpeg to first convert the image into standard PNG image with rgba (32-bit RGB) pixel format. The program then opens the product PNG image using libpng to get the pixels encoded in the image. The pixels are converted into rgb565 (16-bit RGB) and transparent pixels are replaced with pure green (0x00ff00) so that `gl_painter_image` could use them.

For large static images used in the UI, such as the ranking letters and scoreboard backgrounds, which could be rendered only once during loading but are not part of the background, `img_write_dither64`. This function calls ffmpeg to generate a 64-color-dithered PNG image similar to `img_write_dither128`. The program then opens the product image with libpng and dumps the palette and pixels into the sdcard.

Functions in this file significantly improves the quality and reduces the size of the images due to the usage of palette and modern dithering algorithms. They can also be directly dumped into the background buffer of the SRAM without exerting any computation load on Nios.

> **fontenc**
>
> * `uint32_t font_write(const char *filename, int ptsize, mdev *dev, uint32_t addr);`
> * `uint32_t font_score_write(const char *dirname, mdev *dev, uint32_t addr);`

This file converts and imports any valid TTF font into the sdcard. The program invokes imagemagick to render all the printable ASCII character into image file with the specified font. Then the product image is imported as a resource image using `img_write_rgb16`.

The function `font_score_write` handles score fonts from the skin instead of TTF UI fonts. Score fonts are already PNG files in the skin directory, so there is no need to call font rendering when importing score fonts.

> **osu**
>
> * `void osu_install_skin(const char *dirname, mdev *dev, osu_meta *meta);`
> * `void osu_install_font(const char *filename, mdev *dev, osu_meta *meta);`
> * `osu_song osu_read_osu(const char *filename, mdev *dev, osu_meta *meta, osu_dir *cont);`
> * `osu_dir osu_open_dir(const char *dirname);`
> * `const char *osu_read_dir(osu_dir dir);`

This file handles the parsing and import of osu! beatmaps and skin. osu! beatmaps are the definition of playable songs in the desktop osu! game. These functions parse the hit objects (circles, sliders, spinners) defined in the beatmap into the sdcard and also imports the linked song audio and background image.

### Design Resources and Statistics

| Attributes 		| OSU!FPGA	|
|-------------------|-----------|
| Total LE 			| 25573		|
| LUT				| 20959		|
| DSP				| 32     	|
| Memory (BRAM)		| 2592 Kb   |
| Flip-Flop			| 15253		|
| Frequency			| 52.12 MHz	|
| Core Static Power | 108.89 mW	|
| Core Dynamic Power| 346.49 mW	|
| I/O Power 		| 170.58 mW	|
| Total Power		| 625.97 mW	|

### Conclusion

It was until halfway through the project did we realize that what we are building is actually a framework for audio-capable games instead of merely a rhythm game. The system was designed and constructed taking into consideration the needs we have and we may not have. In the end, we do have a nicely connected system of reliable hardware drivers, high-performance layered graphics renderer and structured file and memory systems. Coming up with, building and getting such a system to work is what we are most proud of during the development of this project. Although the final software on the main core that handles game logic is not fully tested and cannot perfectly utilize every feature of this underlying system, we believe that given a few more time to correct and fine tune the high level software, our framework could deliver a smooth and impressive gameplay experience in terms of input, graphics, audio and extensibility.

#### Problems & Bugs & Complaints

We have encountered so many problems that we had never thought about.

Finding a usable audio driver took us some time. Given resource on the course wiki didn't work. University Programs' IP didn't work. DE2-demo programs were too hard to edit.

SD interface also cost a lot of time. The University Programs' SD card hardware is too slow since it's using 1-bit mode for transferring SD card data. DE2-demo program's SD interface is also slow because it performs its logic all on the software. It was only after we wrote hardware logic, and changed the FAT16 file-system to our own file-system that we reached a relatively high data reading speed.

The SRAM is supposed to finish a operation within 20ns, which means we could over-clock it to 100 MHz, but the reality was that it just couldn't.

When playing with memory in the software, we found that in order to correctly load a 32 bit data, we had to align the data addresses.

NIOS II E IS WAY TOO SLOW !!!!

We were thinking about decoding mp3 files and png files on the boeard using software, but it seemed to take forever to do that when we tried that.

The processors are so slow that we can't afford to do function calling since they push and pop the stack a lot.

NIOS II E even performs multiplication by doing for loops in software!

It is annoying that uploading software programs into hardware frequently meets problems. Sometimes it was connection problem, sometimes the processor didn't response and failed to load elf files, and sometimes the JTAG-UART is somehow dead.
