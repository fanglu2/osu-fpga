ECE 385 Final Project Proposal
==============================

***Section AB8 (Fall 2017)***

Fang Lu, Xutao Jumbo Jiang

Overview
--------

This project reproduces the major gameplay experience of the desktop rhythm game [osu!](https://osu.ppy.sh) on the DE2-115 FPGA development board. The user plays the game using a pointing device such as a USB mouse and a keyboard device such as a PS/2 keyboard. The graphics will be displayed using a VGA-capable monitor. It supports all the fundamental gameplay elements, including hit-circles, sliders, spinners and judgement system.

The system is implemented with several modules: the game module that processes all the game logic such as hit objects appearing and judgement with software running on a NIOS II processor core, the input modules that interfaces with on-board peripherals such as USB and PS/2 and converts to standard pointing and keystroke signals for the game module, the graph module that takes instructions from the game module and distributes to its submodules performing specialized hardware rendering, a sound module that , and optionally a file IO module that allows loading skins and bitmaps from external storage devices such as SD card.

Block Diagram
-------------

![Design Diagram](proposal_diagram.svg)

Features
--------

### Baseline Features

* osu! basic gameplay
	* Hit circles, sliders and spinners
	* Timing judgment and score calculation
* Input device: pointing and keystroke
	* USB Mouse
	* PS/2 Keyboard
* Audio output

### Extra Features

**Game Functionalities**

* Song selection
* Score report
* Song preview
* Scoreboard
* Pause
* Mods
* Settings

**Input devices**

* Wacom tablet
* USB hub

**Graphics**

* Background images
* Effect sounds
* Dimming and lighting
* Animated objects
	* Fade in/out
	* Extending sliders
	* Zoom animation
	* Rotation animation
* Vector Path rendering
	* Bezier curve
	* Variable-width vector font
* Effects
	* Particles
	* Pointer trails

**Importing Levels (aka. beatmaps)**

* SDCard reading
* Dedicated filesystem for sdcard
* Beatmap parsing (This step may be moved to a computer as a "preprocessing" conversion step due to unfavorable FPGA processing power)
	* Image decoding
	* Image optimization: palette generation and dithering
	* Audio decoding
	* Audio resampling
* Skinning

Difficulty
----------

We estimate that the minimal baseline product will be of 5-6 difficulty. The baseline specification already contain a set of complex features regarding software complexity, hardware logic and different peripheral drivers. Additionally, the high timing requirement for rhythm games would enforce us to keep different modules accurately synchronized while achieving the full frame rate, or the game will become unplayable. In terms of features to implement, *osu!* contains significantly more complex graphics than traditional arcade games, with images and vector objects all over the place.

The most difficult part as we currently estimate falls into the hardware graphics part. Instead of filling regions with solid colors or repeating sprite patterns, our graphics have to consist of image-based texture that cover the entire view. Since the game must run at 60 FPS, we only have about 50ns per pixel, which must cover a sequence of tasks including reading texture from another memory, perform combinational painting logic, storing to the frame buffer, and reserving some time for the VGA controller to read that memory out. Even after over-clocking the SRAM and corresponding modules to 100 MHz, we still have to pack all the painting into roughly less than 3 redraws per pixel. This would require us to devise meticulous DRAM loading strategies and conditional redraw strategies to save as much waits and paints as possible.

Our optional features contains increased complexity in most modules, so we expect that a full product worth 7-8 difficulty. These include software and hardware implementation of a great variety of fancy graphics like particles, effects, transitions and animations, highly extensible application by loading files from external sources, near full compatibility with any published *osu!* beatmap files and highly-complicated input device drivers such as USB hubs and wacom tablets with custom protocol.

Timeline
--------

* 11/8 - 11/14
	* PS/2 driver setup
	* Audio driver setup
* 11/15 - 11/28 (Thanksgiving)
	* Graphics modules and submodules
	* Bootstrapper and main game program
	* Modules timing synchronization
	* File IO support
* 11/29 - 12/5
	* Baseline testing & debugging
	* Extra graphics
* 12/6-12/13
	* Extra game features
	* Extra drivers
	* Product testing & debugging
