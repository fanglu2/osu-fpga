#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../interprocess/interprocess.h"
#include "osu_main_process/osu_process.h"
#include "osu_main_process/title_process.h"


int main(){
	printf("Hello world\n");

	// Reset MPEG core
	interprocess_reset;


	//initialization
	osu_session* global = malloc(sizeof(osu_session));

	init_process(global);

	while (1) {
		int ret = title_process(global);
		loading_process(global, ret);
		play_process(global);
		score_process(global);
	}

}
