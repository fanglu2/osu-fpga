#ifndef GL_H_
#define GL_H_

#include "regs.h"
#include "util.h"
#include "painters.h"
#include "font.h"

#define GL_CMD_NOOP			0x0
#define GL_CMD_RECT			0x1
#define GL_CMD_CIRCLE		0x2
#define GL_CMD_POLYGON		0x3
#define GL_CMD_RING			0x4
#define GL_CMD_IMAGE		0x6
#define GL_CMD_FIN			0xf

void gl_wait();
void gl_finalize_frame();
void gl_finalize_wait();
void gl_background_dim(uint8_t level);

#endif
