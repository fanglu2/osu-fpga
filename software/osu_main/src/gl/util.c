#include "util.h"

inline uint32_t gl_make_point(uint16_t x, uint16_t y) {
	return ((x << 10) | y);
}

inline uint32_t gl_make_point_8bit(uint16_t x, uint16_t y) {
	return (((x>>1) << 9) | y);
}

inline uint32_t gl_make_rgb(uint8_t r, uint8_t g, uint8_t b) {
	r >>= 3;
	g >>= 2;
	b >>= 3;
	return (r << 11) | (g << 5) | b;
}

inline uint32_t gl_make_color(uint32_t c) {
	return ((c>>19)<<11) | (((c&0x00ff00)>>10)<<5) | ((c&0x0000ff)>>3);
}
