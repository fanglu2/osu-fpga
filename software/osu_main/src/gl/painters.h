#ifndef GL_PAINTERS_H_
#define GL_PAINTERS_H_

#include "inttypes.h"

void gl_rect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1,
			 uint16_t color0, uint16_t color1, uint16_t stroke,
			 uint8_t gradient_direction);

void gl_circle(uint16_t x0, uint16_t y0, uint16_t r,
			 uint16_t color0, uint16_t color1, uint16_t stroke,
			 uint8_t gradient_direction);

void gl_ring(uint16_t x0, uint16_t y0, uint16_t r, uint16_t w, uint16_t stroke);

void gl_polygon(uint16_t x_top, uint16_t y_top,
				uint16_t x_bot, uint16_t y_bot,
				uint16_t x_left, uint16_t y_left,
				uint16_t x_right, uint16_t y_right,
				uint16_t color0, uint16_t color1,
				uint8_t gradient_direction);

void gl_image(uint32_t base, uint16_t w, uint16_t h,
			  uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,
			  uint16_t x3, uint16_t y3, uint16_t x4, uint16_t y4,
			  uint16_t x5, uint16_t y5, uint16_t x6, uint16_t y6);

#endif
