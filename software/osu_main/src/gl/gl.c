#include "gl.h"

#include "io.h"
#include "regs.h"
#include "../interprocess/interprocess.h"
inline void gl_wait() {
	while (!(GL_IORD(GL_STATUS, 0)&GL_STATUS_DONE)) {
		if (GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_TIMEOUT) {
			printf("Warning: timeout during wait\n");
			break;
		}
	}
	// *gl_command = GL_CMD_NOOP;
	GL_IOWR(GL_COMMAND, 0, GL_CMD_NOOP);
}

inline void gl_finalize_frame() {
	// *gl_command = GL_CMD_FIN;
	gl_wait();
	gl_draw_mouse();
	gl_wait();
	GL_IOWR(GL_COMMAND, 0, GL_CMD_FIN);
}

inline void gl_finalize_wait() {
	while (!(GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_DONE)) {
		if (GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_TIMEOUT) {
			printf("Warning: timeout during finalize\n");
			return;
		}
	}
}

inline void gl_background_dim(uint8_t level) {
	GL_IOWR(GL_BG_DIM, 0, level);
}

void gl_draw_mouse(){
	uint16_t x = mouse_xy>>16;
	uint16_t y = mouse_xy & 0x0000FFFF;
	uint8_t click = mouse_btn & 0x01;

	gl_ring(
			x,
			y,
			15+click*10,
			//20,
			2,
			gl_make_color(0xffffff)
			);
	gl_rect(
			x-1,y-8,
			x+1,y+8,
			gl_make_color(0xae95f0),
			gl_make_color(0xae95f0),
			gl_make_color(0xae95f0),
			0
		);
	gl_rect(
			x-8,y-1,
			x+8,y+1,
			gl_make_color(0xae95f0),
			gl_make_color(0xae95f0),
			gl_make_color(0xae95f0),
			0
		);
}
