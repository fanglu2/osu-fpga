#include "painters.h"

#include "io.h"
#include "gl.h"
#include "regs.h"

void gl_rect(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1,
			 uint16_t color0, uint16_t color1, uint16_t stroke,
			 uint8_t gradient_direction) {
	gl_wait();

	GL_IOWR(GL_ARGS, 0, gl_make_point(x0, y0));
	GL_IOWR(GL_ARGS, 1, gl_make_point(x1, y1));
	GL_IOWR(GL_ARGS, 2, (color1<<16) | color0);
	GL_IOWR(GL_ARGS, 3, gradient_direction);
	GL_IOWR(GL_ARGS, 4, stroke);

	GL_IOWR(GL_COMMAND, 0, GL_CMD_RECT);
}

void gl_circle(uint16_t x0, uint16_t y0, uint16_t r,
			 uint16_t color0, uint16_t color1, uint16_t stroke,
			 uint8_t gradient_direction) {
	gl_wait();

	GL_IOWR(GL_ARGS, 0, gl_make_point(x0, y0));
	GL_IOWR(GL_ARGS, 1, r);
	GL_IOWR(GL_ARGS, 2, (color1<<16) | color0);
	GL_IOWR(GL_ARGS, 3, gradient_direction);
	GL_IOWR(GL_ARGS, 4, stroke);

	GL_IOWR(GL_COMMAND, 0, GL_CMD_CIRCLE);
}

void gl_ring(uint16_t x0, uint16_t y0, uint16_t r, uint16_t w, uint16_t stroke) {
	gl_wait();

	GL_IOWR(GL_ARGS, 0, gl_make_point(x0, y0));
	GL_IOWR(GL_ARGS, 1, gl_make_point(w, r));
	GL_IOWR(GL_ARGS, 2, stroke);

	GL_IOWR(GL_COMMAND, 0, GL_CMD_RING);
}

void gl_polygon(uint16_t x_top, uint16_t y_top,
				uint16_t x_bot, uint16_t y_bot,
				uint16_t x_left, uint16_t y_left,
				uint16_t x_right, uint16_t y_right,
				uint16_t color0, uint16_t color1,
				uint8_t gradient_direction) {
	gl_wait();

	GL_IOWR(GL_ARGS, 0, gl_make_point(x_top, y_top));
	GL_IOWR(GL_ARGS, 1, gl_make_point(x_bot, y_bot));
	GL_IOWR(GL_ARGS, 2, gl_make_point(x_left, y_left));
	GL_IOWR(GL_ARGS, 3, gl_make_point(x_right, y_right));
	GL_IOWR(GL_ARGS, 4, (color1<<16) | color0);
	GL_IOWR(GL_ARGS, 5, gradient_direction);

	GL_IOWR(GL_COMMAND, 0, GL_CMD_POLYGON);
}

void gl_image(uint32_t base, uint16_t w, uint16_t h,
			  uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,
			  uint16_t x3, uint16_t y3, uint16_t x4, uint16_t y4,
			  uint16_t x5, uint16_t y5, uint16_t x6, uint16_t y6) {
	gl_wait();

	GL_IOWR(GL_ARGS, 0, base);
	GL_IOWR(GL_ARGS, 1, gl_make_point(w, h));
	GL_IOWR(GL_ARGS, 2, gl_make_point(x1, y1));
	GL_IOWR(GL_ARGS, 3, gl_make_point(x2, y2));
	GL_IOWR(GL_ARGS, 4, gl_make_point(x3, y3));
	GL_IOWR(GL_ARGS, 5, gl_make_point(x4, y4));
	GL_IOWR(GL_ARGS, 6, gl_make_point(x5, y5));
	GL_IOWR(GL_ARGS, 7, gl_make_point(x6, y6));

	GL_IOWR(GL_COMMAND, 0, GL_CMD_IMAGE);
}
