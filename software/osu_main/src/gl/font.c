#include "font.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "io.h"
#include "gl.h"
#include "regs.h"

void gl_text(gl_text_ctx *context, const char *str, int x, int y) {
	if (y < 0 || y > 480)
		return;
	int idx;
	for (const char *c = str; *c; c++) {
		idx = *c - ' ';
		if (idx < 0) {
			// Render char 32 (unknown symbol)
			context->x_[0][context->count_[0]] = x;
			context->y_[0][context->count_[0]] = y;
			context->count_[0]++;
			x += context->font->widths[0];
		} else if (idx == 0) {
			// Skip
			x += context->font->widths[0];
		} else {
			// Render char
			context->x_[idx][context->count_[idx]] = x;
			context->y_[idx][context->count_[idx]] = y;
			context->count_[idx]++;
			x += context->font->widths[idx];
		}
		if (x > 640) {
			return;
		}
	}
}

void gl_text_flush(gl_text_ctx *context) {
	gl_wait();
	for (int c = 0; c < 96; c++) {
		int argptr = 2, cptr = context->count_[c];
		if (cptr) {
			GL_IOWR(GL_ARGS, 0, context->font->font[c]);
			GL_IOWR(GL_ARGS, 1, gl_make_point(context->font->widths[c],
											  context->font->height));
		} else continue;
		while (cptr --> 0) {
			GL_IOWR(GL_ARGS, argptr++,
					gl_make_point(context->x_[c][cptr], context->y_[c][cptr]));
			if (argptr == 8) {
				// Flush
				GL_IOWR(GL_COMMAND, 0, GL_CMD_IMAGE);
				gl_wait();
				GL_IOWR(GL_COMMAND, 0, GL_CMD_NOOP);
				argptr = 2;
			}
		}
		if (argptr != 2) {
			// Remaining undrawn characters. Flush
			GL_IOWR(GL_ARGS, argptr, -1);
			GL_IOWR(GL_COMMAND, 0, GL_CMD_IMAGE);
			gl_wait();
			GL_IOWR(GL_COMMAND, 0, GL_CMD_NOOP);
		}
	}
	// Reset
	memset(context->count_, 0, sizeof(context->count_));
}

void gl_text_clear(gl_text_ctx *context) {
	memset(context->count_, 0, sizeof(context->count_));
}
