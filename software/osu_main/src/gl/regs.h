#ifndef GL_REGS_H_
#define GL_REGS_H_

#include "system.h"
#include "io.h"

#define GL_BASE			GL_SLAVE_GL_BASE
#define GL_IOWR(base, offset, data) IOWR_32DIRECT(GL_BASE+(((base)+(offset))<<2), 0, data)
#define GL_IORD(base, offset) IORD_32DIRECT(GL_BASE+(((base)+(offset))<<2), 0)

#define GL_STATUS		0x0
#define GL_DRAWTIME		0x1
#define GL_COMMAND		0x2
#define GL_BG_DIM		0x3
#define GL_ARGS			0x8

#define GL_STATUS_DONE			0x1
#define GL_STATUS_FRAME_DONE	0x2
#define GL_STATUS_FRAME_TIMEOUT	0x4

#define GL_PALETTE		GL_SLAVE_PALETTE_BASE
#define GL_PLWR(index, data) IOWR_32DIRECT(GL_PALETTE+((index)<<2), 0, data)
#define GL_PLRD(index) IORD_32DIRECT(GL_PALETTE+((index)<<2), 0)

#define GL_SRAM			GL_SLAVE_SRAM_BASE
#define GL_SRAMWR(addr, data) IOWR_32DIRECT(GL_SRAM+((addr)<<2), 0, data)

#define GL_SRAM_BGBASE	0xa0000
#define GL_SRAM_RESBASE	0xc8000

#endif /* SRC_GL_REGS_H_ */
