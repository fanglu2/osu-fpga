#ifndef GL_UTIL_H_
#define GL_UTIL_H_

#include <inttypes.h>

uint32_t gl_make_point(uint16_t x, uint16_t y);
uint32_t gl_make_point_8bit(uint16_t x, uint16_t y);
uint32_t gl_make_rgb(uint8_t r, uint8_t g, uint8_t b);
uint32_t gl_make_color(uint32_t c);

#endif
