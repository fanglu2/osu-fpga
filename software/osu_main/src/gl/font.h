#ifndef FONT_H_
#define FONT_H_

#include "../osufs.h"
#include <inttypes.h>

typedef struct gl_text_ctx_t {
	osu_font *font;
	uint8_t count_[96];
	uint16_t x_[96][128], y_[96][128];
} gl_text_ctx;

void gl_text(gl_text_ctx *context, const char *str, int x, int y);
void gl_text_flush(gl_text_ctx *context);
void gl_text_clear(gl_text_ctx *context);

#endif
