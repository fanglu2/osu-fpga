#include "resources.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../interprocess/interprocess.h"
#include "gl/gl.h"


// TODO: CRITICAL: MPEG's Writing to SRAM is NEVER buffered!

#define RES_LOAD_BLOCK(entry) \
len = meta-> entry ## _len;\
mptr = malloc(512*len);\
mpeg_read(meta-> entry ## _begin, len, (uint32_t)mptr);\
meta-> entry ## _begin = (uint32_t) mptr;

#define RES_LOAD_IMAGE(entry) \
mpeg_img_read(meta->entry, meta-> entry ## _w, meta-> entry ## _h, sram_ptr);\
sram_ptr += meta-> entry ## _w * meta-> entry ## _h;\
meta->entry = sram_ptr;

void resource_load(osu_meta *meta) {

	if (strncmp(meta->magic, "osu!", 4) != 0) {
		printf("CRITICAL: invalid meta magic!\n");
		return;
	}

	printf("Loading resources...\n");

	void *mptr;
	uint32_t len, w, h;

	uint32_t sram_ptr = GL_SRAM_RESBASE;

	RES_LOAD_BLOCK(spinnerspin)
	RES_LOAD_BLOCK(spinnerbonus)
	RES_LOAD_BLOCK(menuclick)
	RES_LOAD_BLOCK(menuback)
	RES_LOAD_BLOCK(menuhit)
	RES_LOAD_BLOCK(combobreak)

	RES_LOAD_BLOCK(drum_hitclap)
	RES_LOAD_BLOCK(drum_hitfinish)
	RES_LOAD_BLOCK(drum_hitnormal)
	RES_LOAD_BLOCK(drum_hitwhistle)
	RES_LOAD_BLOCK(drum_sliderslide)
	RES_LOAD_BLOCK(drum_slidertick)
	RES_LOAD_BLOCK(drum_sliderwhistle)

	RES_LOAD_BLOCK(normal_hitclap)
	RES_LOAD_BLOCK(normal_hitfinish)
	RES_LOAD_BLOCK(normal_hitnormal)
	RES_LOAD_BLOCK(normal_hitwhistle)
	RES_LOAD_BLOCK(normal_sliderslide)
	RES_LOAD_BLOCK(normal_slidertick)
	RES_LOAD_BLOCK(normal_sliderwhistle)

	RES_LOAD_BLOCK(soft_hitclap)
	RES_LOAD_BLOCK(soft_hitfinish)
	RES_LOAD_BLOCK(soft_hitnormal)
	RES_LOAD_BLOCK(soft_hitwhistle)
	RES_LOAD_BLOCK(soft_sliderslide)
	RES_LOAD_BLOCK(soft_slidertick)
	RES_LOAD_BLOCK(soft_sliderwhistle)

	RES_LOAD_IMAGE(hit_0)
	RES_LOAD_IMAGE(hit_100)
	RES_LOAD_IMAGE(hit_300)
	RES_LOAD_IMAGE(hit_100k)
	RES_LOAD_IMAGE(hit_300k)
	RES_LOAD_IMAGE(hit_300g)

	RES_LOAD_IMAGE(pause_back)
	RES_LOAD_IMAGE(pause_continue)
	RES_LOAD_IMAGE(pause_retry)

	w = meta->selection_w;
	h = meta->selection_h;
	len = w * h;
	mpeg_img_read(meta->selection_mod, w, h, sram_ptr);
	meta->selection_mod = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->selection_mod_over, w, h, sram_ptr);
	meta->selection_mod_over = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->selection_random, w, h, sram_ptr);
	meta->selection_random = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->selection_random_over, w, h, sram_ptr);
	meta->selection_random_over = sram_ptr;
	sram_ptr += len;


	w = meta->mod_w;
	h = meta->mod_h;
	len = w * h;
	mpeg_img_read(meta->mod_auto, w, h, sram_ptr);
	meta->mod_auto = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->mod_nofail, w, h, sram_ptr);
	meta->mod_nofail = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->mod_easy, w, h, sram_ptr);
	meta->mod_easy = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->mod_hardrock, w, h, sram_ptr);
	meta->mod_hardrock = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->mod_hidden, w, h, sram_ptr);
	meta->mod_hidden = sram_ptr;
	sram_ptr += len;
	mpeg_img_read(meta->mod_suddendeath, w, h, sram_ptr);
	meta->mod_suddendeath = sram_ptr;
	sram_ptr += len;

	meta->font_12 = resource_load_font((meta->font_12), &sram_ptr);
	meta->font_16 = resource_load_font((meta->font_16), &sram_ptr);
	meta->font_24 = resource_load_font((meta->font_24), &sram_ptr);
	meta->font_score = resource_load_font((meta->font_score), &sram_ptr);

	if (sram_ptr > 0xfffff) {
		printf("SEVERE: SRAM overflow\n");
	}
	printf("Done loading. SRAM usage is %lx", sram_ptr);
}

uint32_t resource_load_font(uint32_t fontp, uint32_t *sram_ptr) {
	osu_font *font = malloc(sizeof(osu_font));

	mpeg_read(fontp, 1, (uint32_t) font);

	if (strncmp(font->magic, "oFNT", 4) != 0) {
		printf("CRITICAL: invalid font magic!\n");
		return 0;
	}

	for (int i=0; i<96; i++) {
		if (font->font[i]) {
			mpeg_img_read(font->font[i], font->widths[i], font->height, *sram_ptr);
			font->font[i] = *sram_ptr;
			*sram_ptr += font->widths[i] * font->height;
		}
	}
	return (uint32_t) font;
}
