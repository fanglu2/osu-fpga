#ifndef OSU_PROCESS_H

#define OSU_PROCESS_H

#include "../../../interprocess/interprocess.h"
#include "../osufs.h"

#define FEEDBACK_QUEUE_SIZE 32
#define FEEDBACK_QUEUE_SIZE_MOD 0b011111
//counted by 4 bytes a time
#define ELEMENT_LENGTH 32
//#define PLAY_TIMELINE_START
//declare the global

typedef struct __attribute__((__packed__)) osu_feedback_t {
	uint32_t addr;
	//char sound[3];
	uint16_t x, y;
	uint32_t end_time;
	uint16_t width,height;
	//uint32_t duration;
	//uint32_t color;
	//uint32_t data_offset;
	//uint32_t data_len;
	//uint16_t extra1, extra2;
} osu_feedbackobj;

typedef struct osu_t {
	osu_song *songs;
	osu_song playing_song;
	osu_beatmap* play_timeline; //= PLAY_TIMELINE_START,
	osu_meta* buff;
	void** draw_hard_list;

	uint32_t play_timeline_start;

	uint32_t current_time;

	uint16_t total_song_count;

	uint16_t song_number;

	uint16_t visible_begin;
	uint16_t visible_end;

	osu_feedbackobj draw_feedback_queue[32];
	uint8_t draw_feedback_queue_start;
	uint8_t draw_feedback_queue_end;

	uint8_t mouse_prev;

	uint32_t score;

}osu_session;




int init_process(osu_session* global);
int loading_process(osu_session* global, int s);
int play_process(osu_session* global);
void osu_draw_queue_update(osu_session* global);
void scene_end(osu_session* global);

#endif
