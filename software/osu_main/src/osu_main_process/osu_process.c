#include "osu_process.h"
#include "osu_draw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../gl/gl.h"
#include "../../../interprocess/interprocess.h"
#include "../keyboard.h"
#include "../resources.h"

int init_process(osu_session* global) {

	//global->draw_dynamic_queue = malloc(sizeof(void*)*DRAW_QUEUE_SIZE);
	global->buff = malloc(512);
	memset(global->buff,0xCC,512);
	mpeg_read(-1,1,(uint32_t)global->buff);
	global->total_song_count = (global->buff) -> songs;
	global->playing_song.beatmap = NULL;
	global->songs = malloc((global->buff)->songs * sizeof(osu_song));
	mpeg_read(1, (global->buff)->songs, global->songs);

	resource_load(global->buff);
	printf("version:%s",global->playing_song.version);
	return 0;
}

int loading_process(osu_session* global, int songid) {
	//scene start  TBD

	//send loading command to sd-card core
	mpeg_read(songid,1, (uint32_t)&(global->playing_song));
	global->playing_song.play_time = global->playing_song.audio_len*512/(44100*4)*1000;

	mpeg_set_bg(global->playing_song.cover_begin);
	while(IORD_32DIRECT(ISD_BASE, MPEG_BG_ADDR));
	//beat map reading
	global->playing_song.beatmap = realloc(global->playing_song.beatmap, global->playing_song.beatmap_len*512);
	mpeg_read(
			global->playing_song.beatmap_begin,
			global->playing_song.beatmap_len,
			(uint32_t)(global->playing_song.beatmap));
	//beat map parsing
	if (strncmp(global->playing_song.beatmap, "HITS", 4) != 0) {
		printf("Invalid beatmap magic\n");
		return 1;
	}
	global->playing_song.bm_meta = global->playing_song.beatmap;
	global->playing_song.bm_objects = global->playing_song.beatmap+32;
	global->playing_song.bm_payload = global->playing_song.beatmap + 32*(global->playing_song.bm_meta->hitobjs+1);
	global->playing_song.judge_time = 50-((global->playing_song.difficulty)/(256.0))*2.5;
	global->playing_song.approach_time = ((global->playing_song.approach_rate/(256.0))*(-140.0))+1800.0;
	global->playing_song.circle_size = 50 - (global->playing_song.circle_size/(256.0)) * 2.0;

	return 0;
}

/*void title_process(){
	scene_start();
	//timing
	clock_t start_time= clock();
	//send loading command to sd-card core
	clock_t current_time;
	//loop start
	while(1){

		//get current time
		current_time = clock()-start_time;
		//check mouse press

		//specify the title elements


		//draw the frame
		osu_draw_frame();
	}

	//end handle
	scene_end();
}*/
int play_process(osu_session* global) {
	//scene_start(global);
	//timing
	set_mouse_sensitivity(128);
	gl_background_dim(72);
	//send loading command to sd-card core

	global->play_timeline_start = 0;
	global->visible_begin = 0;
	global->visible_end = 0;
	global->draw_feedback_queue_start = 0;
	global->draw_feedback_queue_end = 0;
	global->mouse_prev = 0;
	global->score = 0;
	//loop start
	mpeg_cancel_audio;
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_LEN, 0);
	mpeg_play_audio(global->playing_song.audio_begin,global->playing_song.audio_len,0);
	clock_t start_time= clock();
	//while(1);
	while(1){
		//get current time

		global->current_time = clock()-start_time;
		if (global->current_time > (global->playing_song.play_time)) break;
		//printf("working on time %ld",global->current_time);
		//update the drawing elements first
		osu_draw_queue_update(global);
		//then get keyboard/mouse i`nput and handle
		//HInput();
		//draw the frame
		osu_draw_frame(global);

		if (KB_KEYDOWN1) {
			if (KB_KEYDOWN1 == KEY_ESCAPE) {
				KB_KEYDOWN1_ACK;
				mpeg_cancel_audio;
				return 0;
			}
			KB_KEYDOWN1_ACK;
		}

	}
	printf("Done.\n");
	//end handle
	scene_end(global);
	return 0;
}

void osu_draw_queue_update(osu_session* global) {
	osu_song *song = &(global->playing_song);

	uint8_t click = mouse_btn & 0x01;
	uint16_t x = mouse_xy>>16;
	uint16_t y = mouse_xy & 0x0000FFFF;

	int kbclk = 0;
	if (KB_KEYDOWN1) {
		switch(KB_KEYDOWN1) {
		case KEY_Z:
		case KEY_X:
			kbclk = 1;
			break;
		}
		KB_KEYDOWN1_ACK;
	}
	if (KB_KEYDOWN2) {
		KB_KEYDOWN2_ACK;
	}
	if (KB_KEYUP1) {
		KB_KEYUP1_ACK;
	}
	if (KB_KEYUP2) {
		KB_KEYUP2_ACK;
	}

	if ((kbclk || (click && (!global->mouse_prev))) && (global->visible_begin < global->visible_end)){
		uint16_t x0 = song->bm_objects[global->visible_begin].x;
		uint16_t y0 = song->bm_objects[global->visible_begin].y;
		uint16_t csize = global->playing_song.circle_size;
		uint32_t check_time = song->bm_objects[global->visible_begin].time;
		uint32_t jud_time = song->judge_time;
		//check if in the hit circle (must draw something and pop one stuff from timeline)
		if ((x0-x)*(x0-x)+(y0-y)*(y0-y) < (csize*csize) &&
				song->bm_objects[global->visible_begin].type != 'b' &&
				song->bm_objects[global->visible_begin].type != 'c'){
			osu_feedbackobj temp_backobj;
			temp_backobj.end_time = global->current_time + 300;
			++global->visible_begin;
			//check if on the time
			if (global->current_time < (check_time - 3*jud_time)){
				//x
				temp_backobj.addr = global->buff->hit_0;
				temp_backobj.x = x0 - ((global->buff->hit_0_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_0_h)>>1);
				temp_backobj.width = global->buff->hit_0_w;
				temp_backobj.height = global->buff->hit_0_h;
			} else if (global->current_time < (check_time - 2*jud_time)){
				//50
				mpeg_play_fx(global->buff->normal_hitnormal_begin,global->buff->normal_hitnormal_len);
				temp_backobj.addr = global->buff->hit_50;
				temp_backobj.x = x0 - ((global->buff->hit_50_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_50_h)>>1);
				temp_backobj.width = global->buff->hit_50_w;
				temp_backobj.height = global->buff->hit_50_h;
				global->score += 50;
			} else if (global->current_time < (check_time - jud_time)){
				//100
				mpeg_play_fx(global->buff->normal_hitnormal_begin,global->buff->normal_hitnormal_len);
				temp_backobj.addr = global->buff->hit_100;
				temp_backobj.x = x0 - ((global->buff->hit_100_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_100_h)>>1);
				temp_backobj.width = global->buff->hit_100_w;
				temp_backobj.height = global->buff->hit_100_h;
				global->score += 100;
			} else if (global->current_time < (check_time + jud_time)){
				//300
				mpeg_play_fx(global->buff->normal_hitnormal_begin,global->buff->normal_hitnormal_len);
				temp_backobj.addr = global->buff->hit_300;
				temp_backobj.x = x0 - ((global->buff->hit_300_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_300_h)>>1);
				temp_backobj.width = global->buff->hit_300_w;
				temp_backobj.height = global->buff->hit_300_h;
				global->score += 300;
			} else if (global->current_time < (check_time + 2*jud_time)){
				//100
				mpeg_play_fx(global->buff->normal_hitnormal_begin,global->buff->normal_hitnormal_len);
				temp_backobj.addr = global->buff->hit_100;
				temp_backobj.x = x0 - ((global->buff->hit_100_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_100_h)>>1);
				temp_backobj.width = global->buff->hit_100_w;
				temp_backobj.height = global->buff->hit_100_h;
				global->score += 100;
			} else if (global->current_time < (check_time + 3*jud_time)){
				//50
				mpeg_play_fx(global->buff->normal_hitnormal_begin,global->buff->normal_hitnormal_len);
				temp_backobj.addr = global->buff->hit_50;
				temp_backobj.x = x0 - ((global->buff->hit_50_w)>>1);
				temp_backobj.y = y0 - ((global->buff->hit_50_h)>>1);
				temp_backobj.width = global->buff->hit_50_w;
				temp_backobj.height = global->buff->hit_50_h;
				global->score += 50;
			}
			global->draw_feedback_queue[global->draw_feedback_queue_end]
			=temp_backobj;
			++global->draw_feedback_queue_end;
			global->draw_feedback_queue_end &= FEEDBACK_QUEUE_SIZE_MOD;
		}
	}
	global->mouse_prev = click;
	//pop overtime feedback objects
	while (global->draw_feedback_queue_start != global->draw_feedback_queue_end){
		if (global->current_time > global->draw_feedback_queue[global->draw_feedback_queue_start].end_time){
			++global->draw_feedback_queue_start;
			global->draw_feedback_queue_start &= FEEDBACK_QUEUE_SIZE_MOD;
		}else break;
	}

	osu_hitobj element_temp_p;

	//clear existing elements
	//uint8_t queue_temp_start = global->draw_dynamic_queue_start;
	while (global->visible_begin < song->bm_meta->hitobjs){
		//element_temp_p = &((global->draw_dynamic_queue)[queue_temp_start]);
		if ((song->bm_objects[global->visible_begin]).type != 'c'){
			if (global->current_time >= (song->bm_objects[global->visible_begin]).time + ((song->judge_time)*3)){
				//pop draw element from front
				osu_feedbackobj temp_backobj;
				temp_backobj.end_time = global->current_time + 300;

				mpeg_play_fx(global->buff->failsnd_begin,global->buff->failsnd_len);
				temp_backobj.addr = global->buff->hit_0;
				temp_backobj.x = song->bm_objects[global->visible_begin].x - ((global->buff->hit_0_w)>>1);
				temp_backobj.y = song->bm_objects[global->visible_begin].y - ((global->buff->hit_0_h)>>1);
				temp_backobj.width = global->buff->hit_0_w;
				temp_backobj.height = global->buff->hit_0_h;
				global->draw_feedback_queue[global->draw_feedback_queue_end]
							=temp_backobj;
				++global->draw_feedback_queue_end;
				global->draw_feedback_queue_end &= FEEDBACK_QUEUE_SIZE_MOD;
				++(global->visible_begin);
			} else break;
		}else{
			if (global->current_time >= (song->bm_objects[global->visible_begin]).duration){
				++(global->visible_begin);
			} else break;
		}
	}
	//add new element
	if (global->play_timeline_start >= song->bm_meta->hitobjs) {
		return;
	}

	if (global->visible_end < global->visible_begin) {
		printf("Fucked.\n");
	}

	while(global->play_timeline_start < song->bm_meta->hitobjs){
		if (global->current_time >= (song->bm_objects[global->visible_end]).time - song->approach_time){
			//add draw element at queue end
			++(global->visible_end);
			++(global->play_timeline_start);
//		} else break;
		} else break;
	}
}

void scene_end(osu_session* global) {
	//clear the global draw queue
	int i;
	//dim the background
	global->visible_end=0;
	global->visible_begin=0;
	for (i=128;i>=0;i--){
		gl_background_dim(i);
		osu_draw_frame(global);
	}
}
/*
void scene_start(){
	//show the background

}*/

/*void HInput(){
	get_mouse_input();
	if (mouse_left_click){
		//check if it hits anything
	}
}*/

