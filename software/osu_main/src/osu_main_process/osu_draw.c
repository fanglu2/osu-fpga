#include "osu_draw.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../gl/gl.h"

void osu_draw_frame(osu_session* global) {
	//wait until vertical sync
	osu_song *song = &(global->playing_song);
	gl_finalize_wait();
	if (GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_TIMEOUT)
		return;
	if (global->visible_end==global->visible_begin){
		gl_finalize_frame();
		return;
	}

	osu_font *fnt = (osu_font *) global->buff->font_24;
	gl_text_ctx ctx;
	ctx.font = fnt;
	gl_text_clear(&ctx);

	uint32_t temp_queue_end = global->visible_end;
	//temp_queue_end &= DRAW_QUEUE_SIZE_MOD;
	char object_label;
	//void* temp_p;
	while(temp_queue_end>(global->visible_begin)){
		temp_queue_end --;
		object_label = (song->bm_objects[temp_queue_end]).type;
		switch (object_label){
			case 'h': // hit circle
			case 's':
				osu_draw_hit_circle(global, &(song->bm_objects[temp_queue_end]),global->current_time);
				break;
			case 'a':
				osu_draw_slider_head(global, &(song->bm_objects[temp_queue_end]),global->current_time);
				break;
			case 'b':
				osu_draw_slider_mid(global, &(song->bm_objects[temp_queue_end]),global->current_time);
				break;
			case 'c':
				osu_draw_slider_end(global, &(song->bm_objects[temp_queue_end]),global->current_time);
				break;
				/*case 'l':  // slider
				osu_draw_slider(draw_dynamic_queue[temp_queue_end]);
				break;
			*/
		}
		if (object_label == 'h' || object_label == 's' || object_label == 'a') {
			// Draw combo number
			char combostr[2];
			combostr[0] = '0' + song->bm_objects[temp_queue_end].extra2;
			combostr[1] = '\0';
			gl_text(&ctx, combostr, song->bm_objects[temp_queue_end].x - 36, song->bm_objects[temp_queue_end].y - 36);
		}
	/*	--temp_queue_end;
		temp_queue_end &= DRAW_QUEUE_SIZE_MOD;*/
	}

	gl_text_flush(&ctx);

	//feedback
	temp_queue_end = global->draw_feedback_queue_end;
	while(temp_queue_end != global->draw_feedback_queue_start){
		--temp_queue_end;
		temp_queue_end &= FEEDBACK_QUEUE_SIZE_MOD;
		gl_image(global->draw_feedback_queue[temp_queue_end].addr,
				global->draw_feedback_queue[temp_queue_end].width,
				global->draw_feedback_queue[temp_queue_end].height,
				global->draw_feedback_queue[temp_queue_end].x,
				global->draw_feedback_queue[temp_queue_end].y,
				-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
				);
	}
	//hard draw part


	//gl_wait();
	gl_finalize_frame();
}

void osu_draw_hit_circle(osu_session* global, osu_hitobj* circle,clock_t current_time) {
	uint16_t csize = global->playing_song.circle_size;
	if (current_time < circle->time){
		gl_ring(
			circle->x,
			circle->y,
			csize+((global->playing_song.approach_rate*(circle->time-current_time))>>13),
			4,
			gl_make_color(circle->color)
			);
		gl_circle(
			circle->x,
			circle->y,
			csize,
			gl_make_color(circle->color),
			gl_make_color(circle->color),
			gl_make_color(0xffffff),
			0
			);
		gl_ring(
			circle->x,
			circle->y,
			csize+2,
			//20,
			3,
			gl_make_color(0xffffff)
			);
			//circle->approach_rate ++;
			//if (circle->approach_radius<5) circle->approach_radius=5;
	} else if (current_time < circle->time + (global->playing_song.judge_time*3)){
		gl_circle(
			circle->x,
			circle->y,
			csize,
			gl_make_color(circle->color),
			gl_make_color(circle->color),
			gl_make_color(0xffffff),
			0
			);
		gl_ring(
			circle->x,
			circle->y,
			csize+2,
			//20,
			3,
			gl_make_color(0xffffff)
			);
	}
}

void osu_draw_spinner(elem_spinner* circle) {
	gl_circle(
				circle->x,
				circle->y,
				circle->radius,
				circle->gradient_color1,
				circle->gradient_color1,
				gl_make_color(0xffffff),
				0
				);
}

void osu_draw_slider_head(osu_session* global, osu_hitobj* circle, clock_t current_time){
	uint16_t csize = global->playing_song.circle_size;
	if (current_time < circle->time){
		gl_ring(
			circle->x,
			circle->y,
			csize+((global->playing_song.approach_rate*(circle->time-current_time))>>13),
			4,
			gl_make_color(circle->color)
			);
		gl_circle(
			circle->x,
			circle->y,
			csize,
			gl_make_color(circle->color),
			gl_make_color(circle->color),
			gl_make_color(0xffffff),
			0
			);
		gl_ring(
			circle->x,
			circle->y,
			csize+2,
			//20,
			3,
			gl_make_color(0xffffff)
			);
			//circle->approach_rate ++;
			//if (circle->approach_radius<5) circle->approach_radius=5;
	} else if (current_time < circle->duration){
		gl_circle(
			circle->x,
			circle->y,
			csize,
			gl_make_color(circle->color),
			gl_make_color(circle->color),
			gl_make_color(0xffffff),
			0
			);
		gl_ring(
			circle->x,
			circle->y,
			csize+2,
			//20,
			3,
			gl_make_color(0xffffff)
			);
	}
}
void osu_draw_slider_mid(osu_session* global, osu_hitobj* circle, clock_t current_time){
	uint16_t csize = global->playing_song.circle_size;
	gl_ring(
			circle->x,
			circle->y,
			csize,
			//20,
			3,
			gl_make_color(circle->color)
			);
	if (current_time - circle->time < 10) {
		gl_ring(
				circle->x,
				circle->y,
				csize<<1,
				//20,
				3,
				gl_make_color(0xf9e654)
				);
	}
}
void osu_draw_slider_end(osu_session* global, osu_hitobj* circle, clock_t current_time){
	uint16_t csize = global->playing_song.circle_size;
	gl_circle(
			circle->x,
			circle->y,
			csize,
			gl_make_color(circle->color),
			gl_make_color(circle->color),
			gl_make_color(0xffffff),
			0
			);
		gl_ring(
			circle->x,
			circle->y,
			csize+2,
			//20,
			3,
			gl_make_color(0xffffff)
			);
}
