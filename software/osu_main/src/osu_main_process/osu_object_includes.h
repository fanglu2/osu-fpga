#ifndef OSU_OBJECT_INCLUDES_H_

#define OSU_OBJECT_INCLUDES_H_

#include <inttypes.h>

typedef struct __attribute__((__packed__)) hit_circle_t{
	//32 Byte
	uint16_t x,y;
	uint32_t start_timing;
	uint32_t check_timing;
	uint32_t disappear_timing;
	//uint32_t end_timing,
	uint32_t reserved;
	uint16_t hit_radius;
	uint16_t approach_radius;
	uint16_t approach_rate;
	uint16_t gradient_color1;
	uint16_t gradient_color2;
	uint8_t hit_sound;
	char type;
} elem_hit_circle;

typedef struct __attribute__((__packed__)) hit_spinner_t{
	//32 Byte
	uint16_t x,y;
	uint32_t start_timing;
	uint32_t reserved;
	uint32_t disappear_timing;
	uint16_t radius;
	uint16_t gradient_color1;
	uint16_t gradient_color2;
	uint16_t stroke_color;
	uint8_t cycles;
	uint8_t hit_sound;
	char padding[5];
	char type;

} elem_spinner;

/*typedef struct __attribute__((__packed__)) rect_slider_t{
	char type;
	uint32_t start_timing,
	uint32_t check_timing,
	uint32_t disappear_timing,
	//uint32_t end_timing,
	uint16_t hit_radius,
	uint16_t approach_radius,
	uint16_t x1,y1,x2,y2,x3,y3,x4,y4,
	uint8_t gradient_direction,
	uint32_t reserved
}elem_rect_slider;*/

#endif
