#ifndef TITLE_PROCESS_H_
#define TITLE_PROCESS_H_

#include "osu_process.h"

int title_process(osu_session *s);
int score_process(osu_session *s);
#endif
