#include "title_process.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../gl/gl.h"
#include "../keyboard.h"

int title_process(osu_session *s) {
	gl_background_dim(0);
	int dim_current = 0, dim_target = 128;

	// Song selection
	int song_count = s->buff->songs;
	int selected_song = clock() % song_count;

	printf("Total song count: %d\n", song_count);

	gl_text_ctx fnt24, fnt16, fnt12;
	fnt24.font = (osu_font *) s->buff->font_24;
	fnt16.font = (osu_font *) s->buff->font_16;
	fnt12.font = (osu_font *) s->buff->font_12;
	gl_text_clear(&fnt24);
	gl_text_clear(&fnt16);
	gl_text_clear(&fnt12);

	// Exit animation
	int exit_progress = 0, exit_act = 0;

	// Colors
	uint16_t c1, c2, c3, c4, c5;
	uint16_t ltgrey = gl_make_color(0xeeeeee);
	uint16_t ltblue = gl_make_color(0x66ccff);
	uint16_t white = gl_make_color(0xffffff);
	uint16_t ltyellow = gl_make_color(0xffff66);
	c3 = ltgrey;

	// Song selection
	int bmset = 0; // Force reload

	while (1) {
		gl_finalize_wait();
		if (GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_TIMEOUT) {
			continue;
		}
		// Start Painting

		// Dim
		if (exit_act) {
			dim_target = 128;
			exit_progress+=2;
			if (exit_progress == 100) {
				return selected_song;
			}
		}
		if (IORD_32DIRECT(ISD_BASE, MPEG_BG_ADDR) && !exit_progress) {
			dim_current = dim_target = 0;
			gl_background_dim(0);
		} else {
			dim_target = 96;
		}
		if (dim_current != dim_target) {
			if (dim_current < dim_target) {
				dim_current++;
			} else {
				dim_current--;
			}
			gl_background_dim(dim_current);
			if (dim_current == 10) {
				mpeg_cancel_audio;
				mpeg_play_audio(s->playing_song.audio_begin + s->playing_song.preview_blk_offset*4,
								s->playing_song.audio_len -  s->playing_song.preview_blk_offset*4,0); // TODO
			}
		}

		// Header
		gl_rect(0, 0, 639, 88-exit_progress,
			gl_make_color(0xffffff), gl_make_color(0xffffff),
			gl_make_color(0xffffff), 0);

		char cbuf[1024];
		sprintf(cbuf, "%s - %s [%s]",
				s->playing_song.artist,
				s->playing_song.title,
				s->playing_song.version);
		gl_text(&fnt24, cbuf, 0,0 - exit_progress);
		sprintf(cbuf, "Mapped by %s", s->playing_song.creator);
		gl_text(&fnt16, cbuf, 0, 40 - exit_progress);

		gl_text_flush(&fnt24);
		gl_text_flush(&fnt16);


		// Entry 1
		if (selected_song > 1) {
			gl_rect(360+exit_progress*2.4, 110, 639, 160, c1, c1, white, 0);
			gl_text(&fnt16, s->songs[selected_song-2].title, 370+exit_progress*2.4, 120);
			gl_text(&fnt12, s->songs[selected_song-2].version, 370+exit_progress*2.4, 145);
			gl_text_flush(&fnt16);
			gl_text_flush(&fnt12);
		}

		// Entry 2
		if (selected_song > 0) {
			gl_rect(320+exit_progress*2.4, 170, 639, 220, c2, c2, white, 0);
			gl_text(&fnt16, s->songs[selected_song-1].title, 330+exit_progress*2.4, 180);
			gl_text(&fnt12, s->songs[selected_song-1].version, 330+exit_progress*2.4, 205);
			gl_text_flush(&fnt16);
			gl_text_flush(&fnt12);
		}

		// Entry 3 (Active entry)
		gl_rect(270+exit_progress*2.4, 230, 639, 280, c3, c3, white, 0);
		gl_text(&fnt16, s->songs[selected_song].title, 290+exit_progress*2.4, 240);
		gl_text(&fnt12, s->songs[selected_song].version, 290+exit_progress*2.4, 265);
		gl_text_flush(&fnt16);
		gl_text_flush(&fnt12);

		// Entry 4
		if (selected_song < song_count-1) {
			gl_rect(320+exit_progress*2.4, 290, 639, 340, c4, c4, white, 0);
			gl_text(&fnt16, s->songs[selected_song+1].title, 330+exit_progress*2.4, 300);
			gl_text(&fnt12, s->songs[selected_song+1].version, 330+exit_progress*2.4, 325);
			gl_text_flush(&fnt16);
			gl_text_flush(&fnt12);
		}

		// Entry 5
		if (selected_song < song_count-2) {
			gl_rect(360+exit_progress*2.4, 350, 639, 400, c5, c5, white, 0);
			gl_text(&fnt16, s->songs[selected_song+2].title, 370+exit_progress*2.4, 360);
			gl_text(&fnt12, s->songs[selected_song+2].version, 370+exit_progress*2.4, 385);
			gl_text_flush(&fnt16);
			gl_text_flush(&fnt12);
		}

		int mouse_x = mouse_xy >> 8;
		int mouse_y = mouse_xy & 0x00ff;

		gl_rect(0, 424+exit_progress, 639, 479, 0,0,0, 0);

		gl_rect(0, 89-exit_progress, 639, 91-exit_progress,
			gl_make_color(0x0000ff),
			gl_make_color(0x0000ff),
			gl_make_color(0x0000ff), 0);

		gl_rect(0, 422+exit_progress, 639, 423+exit_progress,
			gl_make_color(0x0000ff),
			gl_make_color(0x0000ff),
			gl_make_color(0x0000ff), 0);

		// Selection buttons
		if (mouse_x > 120 && mouse_x < 168 && mouse_y > 242 && !exit_progress)
			gl_image(s->buff->selection_mod_over,
					 s->buff->selection_w, s->buff->selection_h,
					 120, 424, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
		else
			gl_image(s->buff->selection_mod,
					 s->buff->selection_w, s->buff->selection_h - exit_progress,
					 120, 424+exit_progress, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);

		if (mouse_x > 170 && mouse_x < 218 && mouse_y > 242 && !exit_progress)
			gl_image(s->buff->selection_random_over,
					 s->buff->selection_w, s->buff->selection_h,
					 170, 424, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
		else
			gl_image(s->buff->selection_random,
					 s->buff->selection_w, s->buff->selection_h - exit_progress,
					 170, 424+exit_progress, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);

		// End painting
		gl_finalize_frame();

		// Handle keyboard
		int update_colors = 0;
		switch(KB_KEYDOWN1) {
			case 0:
				break;
			case KEY_RETURN:
				s->playing_song = s->songs[selected_song];
				exit_act = 1;
				mpeg_cancel_audio;
				KB_KEYDOWN1_ACK;

				return selected_song+1;
				break;
			case KEY_UP:
				if (selected_song != 0) {
					selected_song--;
					update_colors = 1;
					printf("Selected %d\n", selected_song);
				}
				KB_KEYDOWN1_ACK;
				break;
			case KEY_DOWN:
				if (selected_song != song_count-1) {
					selected_song++;
					update_colors = 1;
					printf("Selected %d\n", selected_song);
				}
				KB_KEYDOWN1_ACK;
				break;
			case KEY_F1:
				// Mods
				KB_KEYDOWN1_ACK;
				break;
			case KEY_F2:
				// Random
				selected_song = clock() % song_count;
				printf("Selected %d\n", selected_song);
				update_colors = 1;
				KB_KEYDOWN1_ACK;
				break;
			default:
				printf("Key: %x\n", KB_KEYDOWN1);
				KB_KEYDOWN1_ACK;
		}
		if (KB_KEYDOWN2) {
			KB_KEYDOWN2_ACK;
		}
		if (KB_KEYUP1) {
			KB_KEYUP1_ACK;
		}
		if (KB_KEYUP2) {
			KB_KEYUP2_ACK;
		}

		if (s->songs[selected_song].beatmap_setid != bmset) {
			// Reload
			s->playing_song = s->songs[selected_song];
			mpeg_cancel_audio;
			mpeg_set_bg(s->playing_song.cover_begin);
			bmset = s->playing_song.beatmap_setid;
			printf("Changing to beatmap set %d\n", bmset);
		}
		if (update_colors) {
			if (s->songs[selected_song-2].beatmap_setid == bmset) {
				c1 = ltblue;
			} else {
				c1 = ltyellow;
			}
			if (s->songs[selected_song-1].beatmap_setid == bmset) {
				c2 = ltblue;
			} else {
				c2 = ltyellow;
			}
			if (s->songs[selected_song+1].beatmap_setid == bmset) {
				c4 = ltblue;
			} else {
				c4 = ltyellow;
			}
			if (s->songs[selected_song+2].beatmap_setid == bmset) {
				c5 = ltblue;
			} else {
				c5 = ltyellow;
			}
		}

	}
}

int score_process(osu_session *s) {
	gl_background_dim(96);

	gl_text_ctx fnt24, fnt16, fnt12;
	fnt24.font = (osu_font *) s->buff->font_24;
	fnt16.font = (osu_font *) s->buff->font_16;
	fnt12.font = (osu_font *) s->buff->font_12;
	gl_text_clear(&fnt24);
	gl_text_clear(&fnt16);
	gl_text_clear(&fnt12);

	// Exit animation
	int exit_progress = 0, exit_act = 0;

	// Colors
	uint16_t c1, c2, c3, c4, c5;
	uint16_t ltgrey = gl_make_color(0xeeeeee);
	uint16_t ltblue = gl_make_color(0x66ccff);
	uint16_t white = gl_make_color(0xffffff);
	uint16_t ltyellow = gl_make_color(0xffff66);
	c3 = ltgrey;

	// Song selection
	int bmset = 0; // Force reload

	while (1) {
		gl_finalize_wait();
		if (GL_IORD(GL_STATUS, 0)&GL_STATUS_FRAME_TIMEOUT) {
			continue;
		}
		// Start Painting

		// Dim

		// Header
		gl_rect(10, 80, 400, 230,
			gl_make_color(0xeeeeee), gl_make_color(0xeeeeee),
			gl_make_color(0xeeeeee), 0);

		char cbuf[1024];
		sprintf(cbuf, "Score %d\n", s->score);
		gl_text(&fnt24, cbuf, 50, 80);

		gl_text_flush(&fnt24);

		// End painting
		gl_finalize_frame();

		// Handle keyboard
		int update_colors = 0;
		switch(KB_KEYDOWN1) {
			case 0:
				break;
			case KEY_ESCAPE:
				mpeg_cancel_audio;
				KB_KEYDOWN1_ACK;

				return 0;
				break;
			default:
				printf("Key: %x\n", KB_KEYDOWN1);
				KB_KEYDOWN1_ACK;
		}
		if (KB_KEYDOWN2) {
			KB_KEYDOWN2_ACK;
		}
		if (KB_KEYUP1) {
			KB_KEYUP1_ACK;
		}
		if (KB_KEYUP2) {
			KB_KEYUP2_ACK;
		}

	}
}

