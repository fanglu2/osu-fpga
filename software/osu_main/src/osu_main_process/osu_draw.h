#ifndef OSU_DRAW_H_

#define OSU_DRAW_H_

#include <inttypes.h>
#include "osu_process.h"
#include "osu_object_includes.h"


void osu_draw_frame(osu_session* global);
void osu_draw_hit_circle(osu_session *global, osu_hitobj *circle,clock_t current_time);
void osu_draw_spinner(elem_spinner* circle);
#endif 
