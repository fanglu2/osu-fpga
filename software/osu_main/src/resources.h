#ifndef RESOURCES_H_
#define RESOURCES_H_

#include "osufs.h"

void resource_load(osu_meta *meta);
uint32_t resource_load_font(uint32_t fontp, uint32_t *sram_ptr);

#endif
