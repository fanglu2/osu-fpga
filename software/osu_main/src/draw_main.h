#include<>

int main(){
	//initialization
	draw_queue = malloc(sizeof(void*)*DRAW_QUEUE_SIZE);
	osu_init();

	//Title Page
	title_process();
	
	//Selection Page
	song_select_process();

	//song play, scoring also handled
	song_play_process();

	//
	song_score_process();
}