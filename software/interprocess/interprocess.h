#ifndef INTERPROCESS_H_
#define INTERPROCESS_H_

#include <stdio.h>
#include <inttypes.h>

#include "system.h"
#include "io.h"

#define ISD_BASE INTERPROC_SHARED_DATA_BASE

// Mouse: R
#define MOUSE_XY			0		// - 3
#define MOUSE_W				4		// - 5
#define MOUSE_SENSITIVITY	6		// - 7
#define MOUSE_WSENSITIVITY	8		// - 9
#define MOUSE_BTN			10		//

// For main
#define mouse_xy IORD_32DIRECT(ISD_BASE, MOUSE_XY)
#define mouse_wheel IORD_16DIRECT(ISD_BASE, MOUSE_W)
#define mouse_btn IORD_8DIRECT(ISD_BASE, MOUSE_BTN)
#define set_mouse_sensitivity(val) IOWR_16DIRECT(ISD_BASE, MOUSE_SENSITIVITY, val)
#define set_wheel_sensitivity(val) IOWR_16DIRECT(ISD_BASE, MOUSE_WSENSITIVITY, val)

// For usb
#define set_mouse_xy(val) IOWR_32DIRECT(ISD_BASE, MOUSE_XY, val)
#define set_mouse_wheel(val) IOWR_16DIRECT(ISD_BASE, MOUSE_W, val)
#define set_mouse_btn(val) IOWR_8DIRECT(ISD_BASE, MOUSE_BTN, val)
#define mouse_sensitivity IORD_16DIRECT(ISD_BASE, MOUSE_SENSITIVITY)
#define mosue_wsensitivity IORD_16DIRECT(ISD_BASE, MOUSE_WSENSITIVITY)


// Background: W
// Trig: ADDR
// Ready: ADDR
#define MPEG_BG_ADDR		12	// - 15

// For main
#define mpeg_set_bg(addr)\
	while (IORD_32DIRECT(ISD_BASE, MPEG_BG_ADDR));\
	IOWR_32DIRECT(ISD_BASE, MPEG_BG_ADDR, addr);


// For mpeg
#define mpeg_bg IORD_32DIRECT(ISD_BASE, MPEG_BG_ADDR)
#define mpeg_bg_reset(val) IOWR_32DIRECT(ISD_BASE, MPEG_BG_ADDR, val);

// Audio: W
// Trig: ADDR
// Ready: ADDR
#define MPEG_AUDIO_ADDR		16	// - 19
#define MPEG_AUDIO_LEN		20	// - 23
#define MPEG_AUDIO_START	24	// - 27

// For main
#define mpeg_play_audio(addr, len, synctime)\
	while(IORD_32DIRECT(ISD_BASE, MPEG_AUDIO_ADDR));\
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_LEN, len);\
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_START, synctime);\
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_ADDR, addr);\

#define mpeg_cancel_audio\
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_ADDR, 0);\
	IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_LEN, 0);\
	IOWR_32DIRECT(ISD_BASE, MPEG_MIX_LEN, 0);\
	IOWR_32DIRECT(ISD_BASE, MPEG_MIX_ADDR, 0);

// For mpeg
#define mpeg_audio_addr IORD_32DIRECT(ISD_BASE, MPEG_AUDIO_ADDR)
#define mpeg_audio_len IORD_32DIRECT(ISD_BASE, MPEG_AUDIO_LEN)
#define mpeg_audio_start IORD_32DIRECT(ISD_BASE, MPEG_AUDIO_START)
#define mpeg_audio_reset(val) IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_ADDR, val)
#define mpeg_audio_len_change(val) IOWR_32DIRECT(ISD_BASE, MPEG_AUDIO_LEN, val)

// Audio Mixdown: W
// Trig: ADDR
// Ready: ADDR
#define MPEG_MIX_ADDR		28	// - 31
#define MPEG_MIX_LEN		32	// - 35

// For main
#define mpeg_play_fx(addr, len)\
	IOWR_32DIRECT(ISD_BASE, MPEG_MIX_LEN, len);\
	IOWR_32DIRECT(ISD_BASE, MPEG_MIX_ADDR, addr);


// For mpeg
#define mpeg_fx_addr IORD_32DIRECT(ISD_BASE, MPEG_MIX_ADDR)
#define mpeg_fx_len IORD_32DIRECT(ISD_BASE, MPEG_MIX_LEN)
#define mpeg_fx_reset(val) IOWR_32DIRECT(ISD_BASE, MPEG_MIX_ADDR, val)
#define mpeg_fx_len_change(val) IOWR_32DIRECT(ISD_BASE, MPEG_MIX_LEN, val)

// Generic Read: RW
// Trig: ADDR
// Ready: ADDR
#define MPEG_READ_ADDR		36	// - 39
#define MPEG_READ_LEN		40	// - 43
#define MPEG_READ_PTR		44	// - 47

// For main
#define mpeg_read(addr, len, buf)\
	while(IORD_32DIRECT(ISD_BASE, MPEG_READ_ADDR));\
	IOWR_32DIRECT(ISD_BASE, MPEG_READ_LEN, len);\
	IOWR_32DIRECT(ISD_BASE, MPEG_READ_PTR, buf);\
	IOWR_32DIRECT(ISD_BASE, MPEG_READ_ADDR, addr);\
	while(IORD_32DIRECT(ISD_BASE, MPEG_READ_ADDR));\
	for (size_t _i = 0; _i < len*128; _i++) {\
		((uint32_t*)buf)[_i] = IORD_32DIRECT(((uint32_t*)buf)+_i,0);\
	}

// For mpeg
#define mpeg_read_addr IORD_32DIRECT(ISD_BASE, MPEG_READ_ADDR)
#define mpeg_read_len IORD_32DIRECT(ISD_BASE, MPEG_READ_LEN)
#define mpeg_read_ptr (void *)(IORD_32DIRECT(ISD_BASE, MPEG_READ_PTR))
#define mpeg_read_reset(val) IOWR_32DIRECT(ISD_BASE, MPEG_READ_ADDR, val)

// Image Resources: W
// Trig: ADDR
// Ready: ADDR
#define MPEG_IMG_ADDR		52	// - 55
#define MPEG_IMG_WIDTH		56	// - 57
#define MPEG_IMG_HEIGHT		58	// - 59
#define MPEG_IMG_DEST		60	// - 63

// For main
#define mpeg_img_read(addr, w, h, dest)\
	while(IORD_32DIRECT(ISD_BASE, MPEG_IMG_ADDR));\
	IOWR_16DIRECT(ISD_BASE, MPEG_IMG_WIDTH, w);\
	IOWR_16DIRECT(ISD_BASE, MPEG_IMG_HEIGHT, h);\
	IOWR_32DIRECT(ISD_BASE, MPEG_IMG_DEST, dest);\
	IOWR_32DIRECT(ISD_BASE, MPEG_IMG_ADDR, addr);


// For mpeg
#define mpeg_img_addr IORD_32DIRECT(ISD_BASE, MPEG_IMG_ADDR)
#define mpeg_img_width IORD_16DIRECT(ISD_BASE, MPEG_IMG_WIDTH)
#define mpeg_img_height IORD_16DIRECT(ISD_BASE, MPEG_IMG_HEIGHT)
#define mpeg_img_dest IORD_32DIRECT(ISD_BASE, MPEG_IMG_DEST)
#define mpeg_img_reset(val) IOWR_32DIRECT(ISD_BASE, MPEG_IMG_ADDR, val)


#define AUDIO_BUFFER1	3072		// - 3
#define AUDIO_BUFFER2	3584	// - 5

// For read audio buffer
#define rd_audio_buffer1(val1) IORD_32DIRECT(ISD_BASE, AUDIO_BUFFER1+(val1<<2))
#define rd_audio_buffer2(val1) IORD_32DIRECT(ISD_BASE, AUDIO_BUFFER2+(val1<<2))
// For write audio buffer
#define wr_audio_buffer1(val1,val2) IOWR_32DIRECT(ISD_BASE, AUDIO_BUFFER1+(val1<<2),val2)
#define wr_audio_buffer2(val1,val2) IOWR_32DIRECT(ISD_BASE, AUDIO_BUFFER2+(val1<<2),val2)

#define rd_audio_buffer1_8(val1) IORD_8DIRECT(ISD_BASE, AUDIO_BUFFER1+val1)
#define rd_audio_buffer1_16(val1) IORD_16DIRECT(ISD_BASE, AUDIO_BUFFER1+(val1<<1))

#define interprocess_reset \
	for (int _i=0; _i<4096; _i++)\
		IOWR_32DIRECT(ISD_BASE, _i, 0);

#endif
