// --------------------------------------------------------------------
// Copyright (c) 2010 by Terasic Technologies Inc. 
// --------------------------------------------------------------------
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development 
//   Kits made by Terasic.  Other use of this code, including the selling 
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use 
//   or functionality of this code.
//
// --------------------------------------------------------------------
//           
//                     Terasic Technologies Inc
//                     356 Fu-Shin E. Rd Sec. 1. JhuBei City,
//                     HsinChu County, Taiwan
//                     302
//
//                     web: http://www.terasic.com/
//                     email: support@terasic.com
//
// --------------------------------------------------------------------

#include "../terasic_lib/terasic_includes.h"
#include "sd_hal.h"
#include "crc16.h"
#include <inttypes.h>
#include <unistd.h>
#include "system.h"
#include "io.h"

#include "../../interprocess/interprocess.h"

#define SD_BASE ((uint32_t*)(SDCARD_BASE))
//#define SD_BASE ((uint32_t*)0x10001000)
/*volatile uint32_t* InterfaceCmd = SD_BASE + 0x81;
volatile uint32_t* SDCmd = SD_BASE + 0x88;
volatile uint32_t* SDResponse = SD_BASE + 0x82;
volatile uint32_t* InterfaceStatus = SD_BASE + 0x84;
volatile uint32_t* SDData = SD_BASE;*/

#define InterfaceCmd (SD_BASE + 0x81)
#define InterfaceStatus (SD_BASE + 0x84)
#define SDCmd (SD_BASE + 0x88)
#define SDResponse (SD_BASE + 0x82)
#define SDData (SD_BASE)


bool SDHAL_IsSupport4Bits(void){
    return TRUE;
}

void SDHAL_Init(void){

    IOWR_32DIRECT(InterfaceCmd,0,7);
    //*InterfaceCmd = 7;
}

void SDHAL_SendCmd(alt_u8 szCommand[6], int nCmdLen){

    int i;

    for (i=0;i<6;++i){
        IOWR_32DIRECT(SDCmd+i,0,szCommand[i]);
        //*(SDCmd+i) = szCommand[i];

    }
    //*InterfaceCmd = 1;
    IOWR_32DIRECT(InterfaceCmd,0,1);
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
}




bool SDHAL_GetResponse(alt_u8 szResponse[], int nLen){

    int i;
    //*InterfaceCmd = 6;
    IOWR_32DIRECT(InterfaceCmd,0,6);
    //while (!(*InterfaceStatus));
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
    //if (*InterfaceStatus == 3){ 
    if (IORD_32DIRECT(InterfaceStatus,0)==3) {
        printf("Response Timeout\n");
        return 0;
    }
    szResponse[0] = IORD_32DIRECT(SDResponse,0);
    //szResponse[0] = *SDResponse;
    for (i=1;i<nLen;++i){
        //*InterfaceCmd = 2;
        IOWR_32DIRECT(InterfaceCmd,0,2);
        //while (!(*InterfaceStatus));
        while (!(IORD_32DIRECT(InterfaceStatus,0)));
        //szResponse[i] = *SDResponse;
        szResponse[i] = IORD_32DIRECT(SDResponse,0);
    }

    //*InterfaceCmd = 4;
    IOWR_32DIRECT(InterfaceCmd,0,4);
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
    return 1;
}


bool SDHAL_ReadData(alt_u8 szBuf[], int nBufLen){

    int i;
    
    //*InterfaceCmd = 3;
    IOWR_32DIRECT(InterfaceCmd,0,3);

    //while (!(*InterfaceStatus));
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
    if (szBuf == AUDIO_BUFFER1){
        for (i=0;i<128;++i){
            wr_audio_buffer1(i,IORD_32DIRECT(SDData+i,0));
        }
    }else if (szBuf == AUDIO_BUFFER2){
        for (i=0;i<128;++i){
            wr_audio_buffer2(i,IORD_32DIRECT(SDData+i,0));
        }
    }else{
        for (i=0;i<128;++i){
            ((uint32_t*)szBuf)[i] = IORD_32DIRECT(SDData+i,0);
        }
    }
    //*InterfaceCmd = 5;
    IOWR_32DIRECT(InterfaceCmd,0,5);
    //*InterfaceCmd = 4;
    //Jump 16 + 1 + 8 = 25 cycles
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
    return 1;
}

bool SDHAL_WriteData(alt_u8 szDataWrite[], int nDataLen){
   
    return 0;
    
}


void SDHAL_DummyClock(int nClockCnt){
    //*InterfaceCmd = 4;
    IOWR_32DIRECT(InterfaceCmd,0,4);
    while (!(IORD_32DIRECT(InterfaceStatus,0)));
}


