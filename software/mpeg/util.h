#ifndef UTIL_H_
#define UTIL_H_

#define ALIGN_16(ptr) (void *)(((uint64_t)(ptr))&(~0x3)+2)
#define ALIGN_32(ptr) (void *)(((uint64_t)(ptr))&(~0xf)+4)
#define ALIGN_64(ptr) (void *)(((uint64_t)(ptr))&(~0xff)+8)

#endif