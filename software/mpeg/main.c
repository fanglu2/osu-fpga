#ifndef SDCARD_H_
#define SDCARD_H_

#include "terasic_lib/terasic_includes.h"
//#include "osufs/osufs.h"
#include "util.h"
#include "terasic_sdcard/sd_lib.h"
#include "../interprocess/interprocess.h"
#include "system.h"
#include "../osu_main/src/gl/gl.h"
#include "time.h"

/*
#define audioData ((uint32_t*)AUDIO_0_BASE)
#define audioReady ((uint32_t*)(AUDIO_0_BASE+0x100))
#define audioVol ((uint32_t*)(AUDIO_0_BASE+0x102))
#define VolReset ((uint32_t*)(AUDIO_0_BASE+0x103))
*/

#define AUDIO_BASE ((uint32_t*)AUDIO_0_BASE)
volatile uint32_t *audioData = AUDIO_BASE;
volatile uint32_t *audioVol = AUDIO_BASE+0x102;
volatile uint32_t *VolReset = AUDIO_BASE+0x103;
volatile uint32_t *audioReady = AUDIO_BASE+0x100;

int main() {
	printf("Hello MPEG\n");

	interprocess_reset;

	//from 00000 to 11111
	clock_t start;
	*audioVol = 0x77;
	*VolReset = 1;
	uint32_t i;
	uint8_t j,c1,c2;
	uint16_t img_buf;
	uint32_t temp_dest;
    SDLIB_Init();
    uint32_t bg_buf[128];
    uint8_t *buf = bg_buf;
    /*
    char o_meta_buf[516];
	uint32_t *audio_buf1 = ALIGN_32(&o_meta_buf);
	char o_song_buf[516];
	uint32_t *audio_buf2 = ALIGN_32(&o_song_buf);*/
	int bptr = 512;
	uint32_t bg_ptr; 

	//giant probe sequence
//	SDLIB_ReadBlock512(o_song->cover_begin,buf);
	PROBE:
	while (1){
    	if (mpeg_bg){
    		printf("Setting background to %x...\n", mpeg_bg);
    		bg_ptr = mpeg_bg;
    		SDLIB_ReadBlock512(bg_ptr,buf);
    		if (strncasecmp(buf, "PLT", 3)!=0) {
				printf("Invalid palette magic: %c%c%c\n", buf[0], buf[1], buf[2]);
			}
			for (int x=0; x<128; x++) {
				uint32_t c = (buf[x*3+3]<<16) | (buf[x*3+4]<<8) | buf[x*3+5];
				GL_PLWR(x+128, c);
				GL_PLWR(x, 0);
			}
			bptr = 512;
			for (int y=0; y<480; y++) {
				for (int x=0; x<640; x+=2) {
					if (bptr >= 512) {
						// Fetch next
						//osu_read_song(++sdptr, buf);
						SDLIB_ReadBlock512(++bg_ptr,(char*)AUDIO_BUFFER1);
						bptr = 0;
					}
					// c1 = buf[bptr++];
					// c2 = buf[bptr++];
					c1 = rd_audio_buffer1_8(bptr++);
					c2 = rd_audio_buffer1_8(bptr++);
//					GL_SRAMWR(GL_SRAM_BGBASE+gl_make_point_8bit(x,y), (c2<<8)|c1);
					GL_SRAMWR(GL_SRAM_BGBASE+(((x>>1) << 9) | y), (c2<<8)|c1);
				}
			}
			mpeg_bg_reset(0);
  			continue;
  		}
  		if (mpeg_img_addr){
  			goto IMG;
  		}
  		if (mpeg_read_addr){
  			printf("Trying to read %x into %x...\n", mpeg_read_addr, mpeg_read_ptr);
  			goto READ;
  		}
		if (mpeg_audio_addr){
			goto AUDIO;
		}	
		if (mpeg_fx_addr){
			goto FX_AUDIO;
		}
	}
	AUDIO:
	//SDLIB_ReadBlock512(temp_start,audioData);
	//usleep(mpeg_audio_start);
	//start= clock();
	printf("loading audio from %ld length %ld\n",mpeg_audio_addr,mpeg_audio_len);
	while(mpeg_audio_len>0){
		SDLIB_ReadBlock512(mpeg_audio_addr,(char*)AUDIO_BUFFER1);
		//wr_audio_buffer1(val1,val2)
		if (mpeg_fx_addr && mpeg_fx_len){
//			SDLIB_ReadBlock512(mpeg_fx_addr,(char*)AUDIO_BUFFER2);
			while (!(*audioReady));
			for (j=0;j<128;++j){
				uint32_t mixin_val = IORD_32DIRECT(mpeg_fx_addr, j<<2);
				uint32_t audio_l, audio_r, fx_l, fx_r, mix_l, mix_r;
				audio_l = (rd_audio_buffer1(j) & 0xffff0000) >> 16;
				audio_r = (rd_audio_buffer1(j) & 0x0000ffff);
				fx_l = (mixin_val & 0xffff0000) >> 16;
				fx_r = (mixin_val & 0x0000ffff);
				mix_l = (audio_l + fx_l) >> 1;
				mix_r = (audio_r + fx_r) >> 1;
				IOWR_32DIRECT(audioData+j,0, (audio_l << 16) | (fx_r & 0x0000ffff));
			}
			mpeg_fx_len_change(mpeg_fx_len-512);
			if (mpeg_fx_len < 512) {
				mpeg_fx_len_change(0);
				mpeg_fx_reset(0);
			} else {
				mpeg_fx_reset(mpeg_fx_addr+512);
			}
		}else{
			while (!(*audioReady));
			for (j=0;j<128;++j){
				IOWR_32DIRECT(audioData+j,0, rd_audio_buffer1(j));
			}
		}

		//SDLIB_ReadBlock512(mpeg_audio_addr,audioData);
		mpeg_audio_reset(mpeg_audio_addr+1);
		mpeg_audio_len_change(mpeg_audio_len-1);
	}
	mpeg_audio_reset(0);
	mpeg_fx_len_change(0);
	mpeg_fx_reset(0);
	//printf("time used %ld",clock()-start);
	goto PROBE;

	READ:
	for (i=0;i<mpeg_read_len;++i){
		SDLIB_ReadBlock512(mpeg_read_addr==-1?0:mpeg_read_addr,mpeg_read_ptr+(i<<9));
		mpeg_read_reset(mpeg_read_addr+1);
	}
	mpeg_read_reset(0);
	goto PROBE;

	FX_AUDIO:
	while (mpeg_fx_len>0){
//		SDLIB_ReadBlock512(mpeg_fx_addr,(char*)AUDIO_BUFFER2);
		while (!(*audioReady));
		for (j=0;j<128;++j){
			uint32_t mixin_val = IORD_32DIRECT(mpeg_fx_addr, j<<2);
//			printf("%x\n", mixin_val);
			IOWR_32DIRECT(audioData+j,0,mixin_val);
		}
		mpeg_fx_len_change(mpeg_fx_len-512);
		if (mpeg_fx_len < 512) {
			mpeg_fx_len_change(0);
			mpeg_fx_reset(0);
		} else {
			mpeg_fx_reset(mpeg_fx_addr+512);
		}
	}
	mpeg_fx_reset(0);
	goto PROBE;

	IMG:
	printf("Trying to cache %x (%dx%d) into %x...\n", mpeg_img_addr, mpeg_img_width, mpeg_img_height, mpeg_img_dest);
	temp_dest = mpeg_img_dest;
	bg_ptr = mpeg_img_addr;
	bptr = 256;
	for (int y=0; y<mpeg_img_height; y++) {
		for (int x=0; x<mpeg_img_width; x++) {
			if (bptr >= 256) {
				// Fetch next
				//osu_read_song(++sdptr, buf);
				SDLIB_ReadBlock512(bg_ptr++,(char*)AUDIO_BUFFER1);
				bptr = 0;
			}
			// c1 = buf[bptr++];
			// c2 = buf[bptr++];
			img_buf = rd_audio_buffer1_16(bptr++);
			GL_SRAMWR(temp_dest++,img_buf);
		}
	}
	mpeg_img_reset(0);
	goto PROBE;
}


#endif
