/**
 *	ps2kb
 *
 *	PS2 Keyboard driver (Avalon slave)
 *
 *	16 addresses are allocated for this module. The first 8 bits contain
 *	keydown events and the latter 8 bits contain keyup events.
 *	A non-zero value denotes that the event has been triggered with the keycode
 *	value as indicated.
 *	After retrieving the event, the software should zero the address so that the
 *	hardware can reuse the location. Otherwise the hardware will not be able to
 *	sent new events.
 *
 *	```
 *	volatile uint32_t *kbdr = BASE;
 *	kbdr[0-7]: Keydown events
 *	kbdr[8-15]: Keyup events
 *	```
 *
 */

module ps2kb (
	// Avalon Clock Input
	input logic CLK,

	// Avalon Reset Input
	input logic RESET,

	// Avalon-MM Slave Signals
	input  logic AVL_READ, // Avalon-MM Read
	input  logic AVL_WRITE, // Avalon-MM Write
	input  logic AVL_CS, // Avalon-MM Chip Select
	input  logic [3:0] AVL_BYTE_EN, // Avalon-MM Byte Enable
	input  logic [1:0] AVL_ADDR, // Avalon-MM Address
	input  logic [31:0] AVL_WRITEDATA, // Avalon-MM Write Data
	output logic [31:0] AVL_READDATA, // Avalon-MM Read Data

	// Exported Conduit
	inout wire PS2_CLK, PS2_DATA

	// Debug exports
	// output logic [7:0] debug_kdr1, debug_kdr2, debug_kur1, debug_kur2, debug_kc,
	// output logic [2:0] debug_state
);
// Static assignments

logic clock_we, data_we, clock_out, data_out, clock_in, data_in;
assign PS2_CLK = (clock_we) ? clock_out : 1'bZ;
assign PS2_DATA = (data_we) ? data_out : 1'bZ;

enum logic [2:0] {
	idle, b_data, b_parity, b_end, b_enqueue, b_error, b_fin
} state, state_next;
logic [2:0] counter, counter_next;
logic clock_wait, clock_wait_next, error, error_next;

// Avalon MM functionalities

logic[7:0] keydown[2], keydown_next[2], keyup[2], keyup_next[2];
logic[7:0] keycode, keycode_next, keycode_out;
logic is_release, is_release_next, is_e0code, is_e0code_next;

assign keycode_out = {keycode[7] | is_e0code, keycode[6:0]};

// assign debug_kdr1 = keydown[0];
// assign debug_kdr2 = keydown[1];
// assign debug_kur1 = keyup[0];
// assign debug_kur2 = keyup[1];
// assign debug_state = state;
// assign debug_kc = keycode_out;

always_ff @(posedge CLK) begin
	if(RESET) begin
		keydown[0] <= 8'h00;
		keydown[1] <= 8'h00;
		keyup[0] <= 8'h00;
		keyup[1] <= 8'h00;
		clock_in <= 1'b1;
		data_in <= 1'b1;

		state <= idle;
		keycode <= 8'h0;
		counter <= 3'b0;
		clock_wait <= 1'b0;
		error <= 1'b0;

		is_release <= 1'b0;
		is_e0code <= 1'b0;
	end else begin
		keydown[0] <= keydown_next[0];
		keydown[1] <= keydown_next[1];
		keyup[0] <= keyup_next[0];
		keyup[1] <= keyup_next[1];
		clock_in <= PS2_CLK;
		data_in <= PS2_DATA;

		state <= state_next;
		keycode <= keycode_next;
		counter <= counter_next;
		clock_wait <= clock_wait_next;
		error <= error_next;

		is_release <= is_release_next;
		is_e0code <= is_e0code_next;
	end
end

always_comb begin
	// Avalon IO

	// Defaults
	keydown_next[0] = keydown[0];
	keydown_next[1] = keydown[1];
	keyup_next[0] = keyup[0];
	keyup_next[1] = keyup[1];

	AVL_READDATA = 32'hCCCC;

	if (AVL_CS) begin
		if (AVL_READ) begin
			case (AVL_ADDR)
				2'b00:
					AVL_READDATA = {24'h0, keydown[0]};
				2'b01:
					AVL_READDATA = {24'h0, keydown[1]};
				2'b10:
					AVL_READDATA = {24'h0, keyup[0]};
				2'b11:
					AVL_READDATA = {24'h0, keyup[1]};
			endcase
		end else if (AVL_WRITE && AVL_BYTE_EN[0]) begin
			case (AVL_ADDR)
				2'b00:
					keydown_next[0] = AVL_WRITEDATA[7:0];
				2'b01:
					keydown_next[1] = AVL_WRITEDATA[7:0];
				2'b10:
					keyup_next[0] = AVL_WRITEDATA[7:0];
				2'b11:
					keyup_next[1] = AVL_WRITEDATA[7:0];
			endcase
		end
	end

	// PS2 Logic
	state_next = state;
	counter_next = counter;
	clock_wait_next = clock_wait;
	keycode_next = keycode;
	error_next = error;

	is_release_next = is_release;
	is_e0code_next = is_e0code;

	clock_we = 1'b0;
	data_we = 1'b0;
	unique case (state)
		idle: begin
			if (!clock_in && !clock_wait) begin
				clock_wait_next = 1'b1;
				// if (data_in != 1'b0)
				// 	error_next = 1'b1;
				// else
					error_next = 1'b0;
			end
			if (clock_in && clock_wait) begin
				clock_wait_next = 1'b0;
				counter_next = 3'b0;
				keycode_next = 8'h0;
				state_next = b_data;
			end
		end

		b_data: begin
			if (!clock_in && !clock_wait) begin
				clock_wait_next = 1'b1;
				// keycode_next[counter] = data_in;
				keycode_next = {data_in, keycode[7:1]};
			end
			if (clock_in && clock_wait) begin
				clock_wait_next = 1'b0;
				counter_next = counter + 3'b1;
				if (counter == 3'h7)
					state_next = b_parity;
			end
		end

		b_parity: begin
			if (!clock_in && !clock_wait) begin
				clock_wait_next = 1'b1;
				// if (data_in != keycode[0] ^ keycode[1] ^ keycode[2] ^
				// 	keycode[3] ^ keycode[4] ^ keycode[5] ^ keycode[6] ^
				// 	keycode[7])
				// 	error_next = 1'b1;
			end
			if (clock_in && clock_wait) begin
				clock_wait_next = 1'b0;
				state_next = b_end;
			end
		end

		b_end: begin
			if (!clock_in && !clock_wait) begin
				clock_wait_next = 1'b1;
				// if (data_in != 1'b1)
				// 	error_next = 1'b1;
			end
			if (clock_in && clock_wait) begin
				clock_wait_next = 1'b0;
				counter_next = 3'b0;
				if (error)
					state_next = b_error;
				else
					state_next = b_enqueue;
				if (keycode == 8'hE0) begin
					is_e0code_next = 1;
					state_next = idle;
				end
				if (keycode == 8'hF0) begin
					is_release_next = 1'b1;
					state_next = idle;
				end
			end
		end

		b_enqueue: begin
			if (is_release) begin
				if (keyup[0] == 8'h0) begin
					keyup_next[0] = keycode_out;
					state_next = b_fin;
				end else if (keyup[1] == 8'h0) begin
					keyup_next[1] = keycode_out;
					state_next = b_fin;
				end else
					state_next = b_fin;
			end else begin
				if (keydown[0] == 8'h0) begin
					keydown_next[0] = keycode_out;
					state_next = b_fin;
				end else if (keydown[1] == 8'h0) begin
					keydown_next[1] = keycode_out;
					state_next = b_fin;
				end else
					state_next = b_fin;
			end
		end

		b_error: begin
			// Just ignore any error
			keydown_next[0] = 8'had;
			keydown_next[1] = 8'hde;
			state_next = b_fin;
		end

		b_fin: begin
			is_e0code_next = 1'b0;
			is_release_next = 1'b0;
			state_next = idle;
		end

	endcase

end

endmodule
