module audio(
	input logic CLK,RESET,

	input logic [8:0]AVL_ADDR,
	input logic AVL_CS,
	input logic AVL_RD,AVL_WR,
	input logic [31:0] AVL_WDATA,
	output logic [31:0] AVL_RDATA,
	
	output logic [6:0] LEDG,

	output logic //AUD_MCLK,                   			
				 AUD_DACDAT,           
				        
				 I2C_SCLK,

	input logic  AUD_ADCDAT,
				 

	inout wire AUD_BCLK,
				AUD_ADCLRCK,
				AUD_DACLRCK,
				I2C_SDAT

	//output logic [15:0] LEDR
);

assign LEDG= {3'b0,available,state};

logic should_give,should_give_next;

logic [7:0]counter,counter_next/*synthesis keep*/;

logic audio_out_allowed,write_audio_out,write_audio_out_next;

logic [6:0] data,data_next;

logic vol_set,vol_set_next;

logic [1:0] state,state_next;

//logic c_reset/*synthesis keep*/;

logic buff_select,buff_select_next;

logic available,available_next;

//logic write_complete,write_complete_next;

logic MEM_WE[2];

logic[31:0] MEM_DATAIN;
logic [31:0] MEM_DATAOUT[0:1];
logic[7:0] MEM_ADDR;

logic [31:0] audio_data_in /*synthesis keep*/;

//logic audio_ready,audio_ready_next;

//logic audio_out_allowed;
//assign LEDR = data[0][31:16];

always_ff @(posedge CLK) begin
	if(RESET) begin
		data <= 32'h0;
		//data[1] <= 32'h0;
		//write_audio_out <= 1'b0;
		vol_set <= 1'b0;
		state <=2'b11;
		buff_select <= 1'b0;
		available <= 1'b0;
		//write_complete <= 1'b0;
	end else begin
		data <= data_next;
		//data[1] <= data_next[1];
		//write_audio_out <= write_audio_out_next;
		vol_set <= vol_set_next;
		state <= state_next;
		buff_select <= buff_select_next;
		available <= available_next;
		//write_complete <= write_complete_next;
	end
end

always_ff @(posedge CLK) begin
	if(RESET) begin
		counter <= 0;
	end else begin
		counter <= counter_next;
	end
end

always_comb begin

	//audio_ready_next = audio_ready;
	data_next = data;
	//data_next[1] = data[1];
	//INIT = 1'b0;
	//c_reset = 0;
	counter_next = counter;
	AVL_RDATA = 32'hCCCC;
	write_audio_out=0;
	vol_set_next = 0;
	
	available_next = available;
	buff_select_next = buff_select;
	state_next = state;

	MEM_WE[0] = 1'b0;
	MEM_WE[1] = 1'b0;
	MEM_ADDR = 8'hCC;
	MEM_DATAIN = 32'hCCCC;

	audio_data_in = MEM_DATAOUT[!buff_select];

	if (AVL_WR && AVL_CS) begin
		if (!AVL_ADDR[8]) begin
			MEM_ADDR = AVL_ADDR[6:0];
			MEM_DATAIN = AVL_WDATA;
			MEM_WE[buff_select] = 1'b1;
		end else begin
			if (AVL_ADDR[1:0] == 2'b10) begin
				data_next = AVL_WDATA;
			end else if (AVL_ADDR[1:0] == 2'b11) begin
				vol_set_next = 1;
			end
		end
	end


	if (AVL_RD && AVL_CS) begin
		if (AVL_ADDR[8] && (AVL_ADDR[1:0] == 2'b00))begin
				AVL_RDATA = {31'b0,available};
		end
	end

//	if (audio_out_allowed) begin
//		audio_ready_next = 1'b1;
//	end else
//		audio_ready_next = 1'b0;
	case (state)
		2'b00:begin
			buff_select_next = 0;
			//buffer 0 for read
			//buffer 1 for audio data
				
			if (audio_out_allowed) begin
				counter_next = counter + 1;
				write_audio_out = 1;

				if (counter == 8'b01111111) begin
					if (!available) begin
						state_next = 2'b01;
						counter_next = 8'b0;
						available_next = 1;
						buff_select_next = 1;
						//write_complete_next = 0;
					end else begin
						audio_data_in = 32'h0;
						counter_next = counter;
					end
				end
			end
			if (AVL_ADDR == 9'b001111111 && (AVL_WR)) begin
				available_next = 0;
				//write_complete_next = 1;
			end

		end
		2'b01:begin
			//reading in buffer 1 
			//output audio in buffer 0
			buff_select_next = 1;
			if (audio_out_allowed) begin
				counter_next = counter + 1;
				write_audio_out = 1;
				if (counter == 8'b01111111) begin
					if (!available) begin
						state_next = 2'b00;
						counter_next = 8'b0;
						available_next = 1;
						buff_select_next = 0;
						//write_complete_next = 0;
					end else begin
						audio_data_in = 32'h0;
						counter_next = counter;
					end
				end
			end
			if ((AVL_ADDR == 9'b001111111) && (AVL_CS)) begin
				available_next = 0;
				//write_complete_next = 1;
			end
		end
		2'b11:begin
			//reading buffer 0
			//output nothing
			buff_select_next = 0;
			available_next = 1'b1;
			if ((AVL_ADDR == 9'b001111111)  && (AVL_CS)) begin
				//c_reset = 1;
				state_next = 2'b01;
				//write_complete_next = 0;
				buff_select_next = 1;
				counter_next = 8'b0;
			end
		end

	endcase
end

byte_enabled_simple_dual_port_ram #(
		.ADDR_WIDTH(7),
		.WIDTH(32)
)MEM0
( 
	.waddr(MEM_ADDR),
	.raddr(counter[6:0]),
	.wdata(MEM_DATAIN), 
	.we(MEM_WE[0]),
	.clk(CLK),
	.q(MEM_DATAOUT[0])
);

byte_enabled_simple_dual_port_ram #(
		.ADDR_WIDTH(7),
		.WIDTH(32)
)MEM1
( 
	.waddr(MEM_ADDR),
	.raddr(counter[6:0]),
	.wdata(MEM_DATAIN), 
	.we(MEM_WE[1]),
	.clk(CLK),
	.q(MEM_DATAOUT[1])
);

Audio_Controller Audio_Controller (
	// Inputs
	.CLOCK_50					(CLK),
	.reset						(RESET),

	.clear_audio_in_memory		(),
	.read_audio_in				(1'b0),
	
	.clear_audio_out_memory		(),
	.left_channel_audio_out		({{4{audio_data_in[31]}},audio_data_in[31:16],12'h0}),
	.right_channel_audio_out	({{4{audio_data_in[15]}},audio_data_in[15:0],12'h0}),
	
	//16'(signed'(IR[5:0]))
	.write_audio_out			(write_audio_out),

	.AUD_ADCDAT					(AUD_ADCDAT),

	// Bidirectionals
	.AUD_BCLK					(AUD_BCLK),
	.AUD_ADCLRCK				(AUD_ADCLRCK),
	.AUD_DACLRCK				(AUD_DACLRCK),


	// Outputs
	.audio_in_available			(),
	.left_channel_audio_in		(),
	.right_channel_audio_in		(),

	.audio_out_allowed			(audio_out_allowed),

	.AUD_XCK(),				//(AUD_MCLK),
	.AUD_DACDAT					(AUD_DACDAT)

);

avconf #(.USE_MIC_INPUT(0)) avc (
	.I2C_SCLK					(I2C_SCLK),
	.I2C_SDAT					(I2C_SDAT),
	.CLOCK_50					(CLK),
	.reset						(vol_set|RESET),
	.AUD_VOL 					(data)
);
endmodule
