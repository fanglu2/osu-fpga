// Quartus Prime SystemVerilog Template
//
// True Dual-Port RAM with single clock and different data width on the two ports
//
// The first datawidth and the widths of the addresses are specified
// The second data width is equal to DATA_WIDTH1 * RATIO, where RATIO = (1 << (ADDRESS_WIDTH1 - ADDRESS_WIDTH2)
// RATIO must have value that is supported by the memory blocks in your target
// device.  Otherwise, no RAM will be inferred.
//
// Read-during-write behavior returns old data for all combinations of read and
// write on both ports
//
// This style of RAM cannot be used on certain devices, e.g. Stratix V; in that case use the template for Dual-Port RAM with new data on read-during write on the same port

module gl_palette
	#(parameter int
		DATA_WIDTH = 24,
		ADDRESS_WIDTH = 8
	 ) (
		input [ADDRESS_WIDTH-1:0] addr1,
		input [ADDRESS_WIDTH-1:0] addr2,
		input [DATA_WIDTH-1:0] data_in1,
		input [DATA_WIDTH-1:0] data_in2,
		input we1, we2, clk,
		output reg [DATA_WIDTH-1:0] data_out1,
		output reg [DATA_WIDTH-1:0] data_out2,
		input logic [7:0] background_dim
);

	localparam RAM_DEPTH = 1 << ADDRESS_WIDTH;

	// Use a multi-dimensional packed array to model the different read/ram width
	reg [DATA_WIDTH-1:0] ram[0:RAM_DEPTH-1];

	reg [DATA_WIDTH-1:0] data_reg1;
	reg [DATA_WIDTH-1:0] data_reg2;

	logic [7:0] r1, g1, b1, r2, g2, b2;
	logic [15:0] r1d, g1d, b1d, r2d, g2d, b2d;

	// Port A
	always@(posedge clk)
	begin
		if(we1)
			ram[addr1] <= data_in1;
		data_reg1 <= ram[addr1];
	end
	assign r1 = data_reg1[23:16];
	assign g1 = data_reg1[15:8];
	assign b1 = data_reg1[7:0];
	assign r1d = r1 * background_dim;
	assign g1d = g1 * background_dim;
	assign b1d = b1 * background_dim;
	assign data_out1 = {r1d[14:7], g1d[14:7], b1d[14:7]};

	// port B
	always@(posedge clk)
	begin
		if(we2)
			ram[addr2] <= data_in2;
		data_reg2 <= ram[addr2];
	end
	assign r2 = data_reg2[23:16];
	assign g2 = data_reg2[15:8];
	assign b2 = data_reg2[7:0];
	assign r2d = r2 * background_dim;
	assign g2d = g2 * background_dim;
	assign b2d = b2 * background_dim;
	assign data_out2 = {r2d[14:7], g2d[14:7], b2d[14:7]};

endmodule
