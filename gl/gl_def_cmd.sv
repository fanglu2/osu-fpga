`ifndef GL_DEF_CMD_SV_
`define GL_DEF_CMD_SV_

// Commands
`define GL_CMD_NOOP			4'h0
`define GL_CMD_RECT			4'h1
`define GL_CMD_CIRCLE		4'h2
`define GL_CMD_POLYGON		4'h3
`define GL_CMD_RING			4'h4
`define GL_CMD_IMAGE		4'h6
`define GL_CMD_FIN			4'hf

`endif
