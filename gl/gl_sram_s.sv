/**
 *	gl_sram
 *
 *	SRAM driver and IO arbitrator
 *
 */
module gl_sram_s (
	// Clock
	input logic CLK, RESET,

	// Read 1
	input logic read1_req,
	input logic [19:0] read1_addr,
	output logic [15:0] read1_data,
	output logic read1_ready,

	input logic read2_req,
	input logic [19:0] read2_addr,
	output logic [15:0] read2_data,
	output logic read2_ready,

	// Write
	input logic write1_req/*synthesis keep*/,
	input logic [19:0] write1_addr/*synthesis keep*/,
	input logic [15:0] write1_data/*synthesis keep*/,
	output logic write1_ready/*synthesis keep*/,

	input logic write2_req,
	input logic [19:0] write2_addr,
	input logic [15:0] write2_data,
	output logic write2_ready,

	// Export Conduit
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N
);
// Tristate
logic [15:0] SRAM_DQ_WR;
assign SRAM_DQ = (SRAM_CE_N | SRAM_WE_N) ? 16'hZZZZ : SRAM_DQ_WR;
// Enable whole 16 bits
assign SRAM_UB_N = 1'b0;
assign SRAM_LB_N = 1'b0;
assign SRAM_CE_N = SRAM_WE_N & SRAM_OE_N;

// Output registers
logic read1_ready_next, read2_ready_next, write1_ready_next, write2_ready_next;
logic [15:0] read1_data_next, read2_data_next;

always_ff @(posedge CLK) begin
	if(RESET) begin
		read1_ready <= 1'b0;
		read2_ready <= 1'b0;
		write1_ready <= 1'b0;
		write2_ready <= 1'b0;
		read1_data <= 16'hCCCC;
		read2_data <= 16'hCCCC;
	end else begin
		read1_ready <= read1_ready_next;
		read2_ready <= read2_ready_next;
		write1_ready <= write1_ready_next;
		write2_ready <= write2_ready_next;
		read1_data <= read1_data_next;
		read2_data <= read2_data_next;
	end
end

always_comb begin


	read1_ready_next = 1'b0;
	read2_ready_next = 1'b0;
	write1_ready_next = 1'b0;
	write2_ready_next = 1'b0;
	read1_data_next = read1_data;
	read2_data_next = read2_data;

	SRAM_WE_N = 1'b1;
	SRAM_OE_N = 1'b1;
	SRAM_ADDR = 20'hXXXXX;
	SRAM_DQ_WR = 16'hXXXX;

	// Priority: Read1 -> Read2 -> Write1 -> Write2
	if (!RESET) begin
		if (read1_req) begin
			SRAM_ADDR = read1_addr;
			SRAM_OE_N = 1'b0;
			read1_data_next = SRAM_DQ;
			read1_ready_next = 1'b1;
		end else if (read2_req) begin
			SRAM_ADDR = read2_addr;
			SRAM_OE_N = 1'b0;
			read2_data_next = SRAM_DQ;
			read2_ready_next = 1'b1;
		end else if (write1_req) begin
			SRAM_ADDR = write1_addr;
			SRAM_DQ_WR = write1_data;
			SRAM_WE_N = 1'b0;
			write1_ready_next = 1'b1;
		end else if (write2_req) begin
			SRAM_ADDR = write2_addr;
			SRAM_DQ_WR = write2_data;
			SRAM_WE_N = 1'b0;
			write2_ready_next = 1'b1;
		end
	end
end

endmodule
