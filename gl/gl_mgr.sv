`include "gl_def_cmd.sv"

module gl_mgr (
	// Clock
	input logic CLOCK, RESET,

	// Status Control
	output logic GL_FRAME_FINISHED, GL_TIMEOUT,
	output logic[9:0] GL_DRAWTIME,

	// GL Commands and arguments
	input logic[3:0] GL_CMD,
	input logic[31:0] GL_ARG1, GL_ARG2, GL_ARG3, GL_ARG4,
	input logic[31:0] GL_ARG5, GL_ARG6, GL_ARG7, GL_ARG8,
	input logic GL_EXEC,
	output logic GL_DONE,

	// Background processing
	input logic [7:0] BG_DIM,
	input logic [15:0] TRANSPARENCY,

	// Avalon connection
	input logic AVL_REQ,
	input logic [19:0] AVL_ADDR,
	input logic [15:0] AVL_DATA,
	output logic AVL_READY,

	input logic AVL_PLT_RD, AVL_PLT_WR,
	input logic[7:0] AVL_PLT_INDEX,
	output logic[23:0] AVL_PLT_RD_COLOR,
	input logic[23:0] AVL_PLT_WR_COLOR,

	// SRAM connection
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N,

	// VGA connection
	output logic VGA_HS, VGA_VS, VGA_BLANK_N, VGA_SYNC_N, VGA_CLK,
	output logic [7:0] VGA_R, VGA_G, VGA_B
);

	// Double-buffer switching connect
	logic paint_buffer, paint_buffer_next;
	logic [9:0] drawtime_in;

	always_ff @(posedge CLOCK) begin
		if(RESET) begin
			paint_buffer <= 0;
			GL_DRAWTIME <= 10'd0;
		end else begin
			paint_buffer <= paint_buffer_next;
			GL_DRAWTIME <= drawtime_in;
		end
	end

	// Redraw cache instance
	logic [19:0] rc_addr1, rc_addr2;
	logic rc_data_in1, rc_data_in2;
	logic rc_we1, rc_we2, rc_we1_san;
	logic rc_data_out1, rc_data_out2;
	gl_redraw_cache redraw_cache(
		.addr1    (rc_addr1),
		.addr2    (rc_addr2),
		.data_in1 (rc_data_in1),
		.data_in2 (rc_data_in2),
		.we1      (rc_we1_san),
		.we2      (rc_we2),
		.clk      (CLOCK),
		.data_out1(rc_data_out1),
		.data_out2(rc_data_out2)
	);

	// Palette instance
	logic [19:0] plt_addr1;
	logic plt_data_in1;
	logic plt_we1;
	logic [23:0] plt_data_out1;
	gl_palette palette_inst(
		.clk           (CLOCK),
		.background_dim(BG_DIM),
		.addr1         (plt_addr1),
		.addr2         (AVL_PLT_INDEX),
		.data_in1      (plt_data_in1),
		.data_in2      (AVL_PLT_WR_COLOR),
		.we1           (1'b0),
		.we2           (AVL_PLT_WR),
		.data_out1     (plt_data_out1),
		.data_out2     (AVL_PLT_RD_COLOR)
	);

	// Frame buffer instance

	//  Read
	logic fb_VGA_REQ;
	logic fb_VGA_FG;
	logic [9:0] fb_VGA_X, fb_VGA_Y;
	logic [23:0] fb_VGA_RGB;
	// GL Read
	logic fb_GL_REQ;
	logic fb_GL_FG;
	logic [9:0] fb_GL_X, fb_GL_Y;
	logic [19:0] fb_GL_ADDR;
	logic [15:0] fb_GL_RGB;
	logic fb_GL_READY;
	// Paint Write
	logic fb_PAINT_REQ;
	logic [9:0] fb_PAINT_X, fb_PAINT_Y;
	logic [9:0] fb_PAINT_X_san, fb_PAINT_Y_san;
	logic [15:0] fb_PAINT_RGB16;
	logic fb_PAINT_READY;
	logic paint_invalid;
	assign paint_invalid = (fb_PAINT_X>10'd639) || (fb_PAINT_Y>10'd479);
	assign fb_PAINT_X_san = paint_invalid ? 10'd639 : fb_PAINT_X;
	assign fb_PAINT_Y_san = paint_invalid ? 10'd479 : fb_PAINT_Y;
	assign rc_we1_san = paint_invalid ? 1'b0 : rc_we1;
	gl_frame_buffer frame_buffer(
		.CLK          (CLOCK),
		.RESET        (RESET),
		.VGA_REQ      (fb_VGA_REQ),
		.VGA_X        (fb_VGA_X),
		.VGA_Y        (fb_VGA_Y),
		.VGA_RGB      (fb_VGA_RGB),
		.GL_REQ       (fb_GL_REQ),
		.GL_X         (fb_GL_X),
		.GL_Y         (fb_GL_Y),
		.GL_ADDR      (fb_GL_ADDR),
		.GL_DATA      (fb_GL_RGB),
		.GL_READY     (fb_GL_READY),
		.PAINT_REQ    (fb_PAINT_REQ),
		.PAINT_X      (fb_PAINT_X_san),
		.PAINT_Y      (fb_PAINT_Y_san),
		.PAINT_RGB16  (fb_PAINT_RGB16),
		.PAINT_READY  (fb_PAINT_READY),
		.BUF_ACTIVE   (paint_buffer),
		.RC_ADDR      (rc_addr2),
		.RC_WE        (rc_we2),
		.RC_DATA_IN   (rc_data_in2),
		.RC_DATA_OUT  (rc_data_out2),
		.PL_ADDR      (plt_addr1),
		.PL_DATA_OUT   (plt_data_out1),
		.* // Avalon and SRAM connection
	);

	// VGA instance

	logic vga_interframe;
	gl_vga vga(
		.CLOCK_50      (CLOCK),
		.RESET         (RESET),
		.FB_REQ        (fb_VGA_REQ),
		.FB_X          (fb_VGA_X),
		.FB_Y          (fb_VGA_Y),
		.FB_RET        (fb_VGA_RGB),
		.RENDER_BUFFER (~paint_buffer),
		.VGA_INTERFRAME(vga_interframe),
		.* // VGA connection
	);

	// Buffer swap control
	logic prev_interframe;
	enum logic [1:0] {
		b_busy, b_standby, b_swapped, b_timeout
	} buffer_status, buffer_status_next;

	assign GL_FRAME_FINISHED = (buffer_status == b_swapped);
	assign GL_TIMEOUT = (buffer_status == b_timeout);

	always_ff @(posedge CLOCK) begin
		if (RESET) begin
			buffer_status <= vga_interframe ? b_busy : b_standby;
		end else begin
			buffer_status <= buffer_status_next;
		end
		prev_interframe <= vga_interframe;
	end

	always_comb begin
		buffer_status_next = buffer_status;
		paint_buffer_next = paint_buffer;
		drawtime_in = GL_DRAWTIME;
		if (~prev_interframe & vga_interframe) begin
			// posedge: VGA finished frame
			buffer_status_next = b_standby;
		end
		if (buffer_status == b_standby & (GL_CMD == `GL_CMD_FIN)) begin
			// Process swap
			buffer_status_next = b_swapped;
			paint_buffer_next = ~paint_buffer;
		end
		if (buffer_status == b_busy & (GL_CMD == `GL_CMD_FIN)) begin
			// Draw finished!
			drawtime_in = fb_VGA_Y;
		end
		if (prev_interframe & ~vga_interframe) begin
			// negedge: VGA started new frame
			if (buffer_status == b_swapped) begin
				buffer_status_next = b_busy;
			end else begin
				buffer_status_next = b_timeout;
				// Force swap
				paint_buffer_next = ~paint_buffer;
			end
		end
	end

	// GL Modules instances

	// Rectangle
	logic rect_en, rect_done;
	// Memory write connection
	logic rect_PAINT_REQ;
	logic [9:0] rect_PAINT_X, rect_PAINT_Y;
	logic [15:0] rect_PAINT_RGB16;
	// Redraw cache connection
	logic [19:0] rect_RC_ADDR;
	logic rect_RC_DATA_WR;
	logic rect_RC_WE;
	logic rect_RC_DATA_RD;
	gl_painter_rect painter_rect(
		.CLOCK         (CLOCK),
		.RESET         (RESET),
		.X0            (GL_ARG1[19:10]),
		.Y0            (GL_ARG1[9:0]),
		.X1            (GL_ARG2[19:10]),
		.Y1            (GL_ARG2[9:0]),
		.C0            (GL_ARG3[15:0]),
		.C1            (GL_ARG3[31:16]),
		.GRAD_HORIZ    (GL_ARG4[0]),
		.CSTROKE       (GL_ARG5[15:0]),
		.EN            (rect_en),
		.DONE          (rect_done),
		.PAINT_BUFFER  (paint_buffer),
		.fb_PAINT_REQ  (rect_PAINT_REQ),
		.fb_PAINT_X    (rect_PAINT_X),
		.fb_PAINT_Y    (rect_PAINT_Y),
		.fb_PAINT_RGB16(rect_PAINT_RGB16),
		.fb_PAINT_READY(fb_PAINT_READY),
		.RC_ADDR       (rect_RC_ADDR),
		.RC_DATA_WR    (rect_RC_DATA_WR),
		.RC_WE         (rect_RC_WE),
		.RC_DATA_RD    (rc_data_out1)
	);

	// Circle
	logic circle_en, circle_done;
	// Memory write connection
	logic circle_PAINT_REQ;
	logic [9:0] circle_PAINT_X, circle_PAINT_Y;
	logic [15:0] circle_PAINT_RGB16;
	// Redraw cache connection
	logic [19:0] circle_RC_ADDR;
	logic circle_RC_DATA_WR;
	logic circle_RC_WE;
	logic circle_RC_DATA_RD;
	gl_painter_circle painter_circle(
		.CLOCK         (CLOCK),
		.RESET         (RESET),
		.X0            ({6'b0, GL_ARG1[19:10]}),
		.Y0            ({6'b0, GL_ARG1[9:0]}),
		.R             ({6'b0, GL_ARG2[9:0]}),
		.C0            (GL_ARG3[15:0]),
		.C1            (GL_ARG3[31:16]),
		.GRAD_HORIZ    (GL_ARG4[0]),
		.CSTROKE       (GL_ARG5[15:0]),
		.EN            (circle_en),
		.DONE          (circle_done),
		.PAINT_BUFFER  (paint_buffer),
		.fb_PAINT_REQ  (circle_PAINT_REQ),
		.fb_PAINT_X    (circle_PAINT_X),
		.fb_PAINT_Y    (circle_PAINT_Y),
		.fb_PAINT_RGB16(circle_PAINT_RGB16),
		.fb_PAINT_READY(fb_PAINT_READY),
		.RC_ADDR       (circle_RC_ADDR),
		.RC_DATA_WR    (circle_RC_DATA_WR),
		.RC_WE         (circle_RC_WE),
		.RC_DATA_RD    (rc_data_out1)
	);

	// Ring
	logic ring_en, ring_done;
	// Memory write connection
	logic ring_PAINT_REQ;
	logic [9:0] ring_PAINT_X, ring_PAINT_Y;
	logic [15:0] ring_PAINT_RGB16;
	// Redraw cache connection
	logic [19:0] ring_RC_ADDR;
	logic ring_RC_DATA_WR;
	logic ring_RC_WE;
	logic ring_RC_DATA_RD;
	gl_painter_ring painter_ring(
		.CLOCK         (CLOCK),
		.RESET         (RESET),
		.X0            ({6'b0, GL_ARG1[19:10]}),
		.Y0            ({6'b0, GL_ARG1[9:0]}),
		.R             ({6'b0, GL_ARG2[9:0]}),
		.W             ({6'b0, GL_ARG2[19:10]}),
		.CSTROKE       (GL_ARG3[15:0]),
		.EN            (ring_en),
		.DONE          (ring_done),
		.PAINT_BUFFER  (paint_buffer),
		.fb_PAINT_REQ  (ring_PAINT_REQ),
		.fb_PAINT_X    (ring_PAINT_X),
		.fb_PAINT_Y    (ring_PAINT_Y),
		.fb_PAINT_RGB16(ring_PAINT_RGB16),
		.fb_PAINT_READY(fb_PAINT_READY),
		.RC_ADDR       (ring_RC_ADDR),
		.RC_DATA_WR    (ring_RC_DATA_WR),
		.RC_WE         (ring_RC_WE),
		.RC_DATA_RD    (rc_data_out1)
	);

	// Polygon
	logic polygon_en, polygon_done;
	// Memory write connection
	logic polygon_PAINT_REQ;
	logic [9:0] polygon_PAINT_X, polygon_PAINT_Y;
	logic [15:0] polygon_PAINT_RGB16;
	// Redraw cache connection
	logic [19:0] polygon_RC_ADDR;
	logic polygon_RC_DATA_WR;
	logic polygon_RC_WE;
	logic polygon_RC_DATA_RD;
	gl_painter_polygon painter_polygon(
		.CLOCK         (CLOCK),
		.RESET         (RESET),
		.XT            (GL_ARG1[19:10]),
		.YT            (GL_ARG1[9:0]),
		.XB            (GL_ARG2[19:10]),
		.YB            (GL_ARG2[9:0]),
		.XL            (GL_ARG3[19:10]),
		.YL            (GL_ARG3[9:0]),
		.XR            (GL_ARG4[19:10]),
		.YR            (GL_ARG4[9:0]),
		.C0            (GL_ARG5[15:0]),
		.C1            (GL_ARG5[31:16]),
		.GRAD_HORIZ    (GL_ARG6[0]),
		.EN            (polygon_en),
		.DONE          (polygon_done),
		.PAINT_BUFFER  (paint_buffer),
		.fb_PAINT_REQ  (polygon_PAINT_REQ),
		.fb_PAINT_X    (polygon_PAINT_X),
		.fb_PAINT_Y    (polygon_PAINT_Y),
		.fb_PAINT_RGB16(polygon_PAINT_RGB16),
		.fb_PAINT_READY(fb_PAINT_READY),
		.RC_ADDR       (polygon_RC_ADDR),
		.RC_DATA_WR    (polygon_RC_DATA_WR),
		.RC_WE         (polygon_RC_WE),
		.RC_DATA_RD    (rc_data_out1)
	);

	// Image
	logic image_en, image_done;
	// Memory write connection
	logic image_PAINT_REQ;
	logic [9:0] image_PAINT_X, image_PAINT_Y;
	logic [15:0] image_PAINT_RGB16;
	// Memory read connection
	logic image_GL_REQ;
	logic [19:0] image_GL_ADDR;
	// Redraw cache connection
	logic [19:0] image_RC_ADDR;
	logic image_RC_DATA_WR;
	logic image_RC_WE;
	logic image_RC_DATA_RD;
	// Destination coordinate set
	logic [9:0] image_X[6], image_Y[6];
	assign image_X[0] = GL_ARG3[19:10];
	assign image_X[1] = GL_ARG4[19:10];
	assign image_X[2] = GL_ARG5[19:10];
	assign image_X[3] = GL_ARG6[19:10];
	assign image_X[4] = GL_ARG7[19:10];
	assign image_X[5] = GL_ARG8[19:10];
	assign image_Y[0] = GL_ARG3[9:0];
	assign image_Y[1] = GL_ARG4[9:0];
	assign image_Y[2] = GL_ARG5[9:0];
	assign image_Y[3] = GL_ARG6[9:0];
	assign image_Y[4] = GL_ARG7[9:0];
	assign image_Y[5] = GL_ARG8[9:0];
	gl_painter_image painter_image(
		.CLOCK         (CLOCK),
		.RESET         (RESET),
		.IMG_BASE      (GL_ARG1),
		.W             (GL_ARG2[19:10]),
		.H             (GL_ARG2[9:0]),
		.TRANSPARENCY  (TRANSPARENCY),
		.X             (image_X),
		.Y             (image_Y),
		.EN            (image_en),
		.DONE          (image_done),
		.PAINT_BUFFER  (paint_buffer),
		.fb_PAINT_REQ  (image_PAINT_REQ),
		.fb_PAINT_X    (image_PAINT_X),
		.fb_PAINT_Y    (image_PAINT_Y),
		.fb_PAINT_RGB16(image_PAINT_RGB16),
		.fb_PAINT_READY(fb_PAINT_READY),
		.fb_GL_REQ     (image_GL_REQ),
		.fb_GL_ADDR    (image_GL_ADDR),
		.fb_GL_DATA    (fb_GL_RGB),
		.fb_GL_READY   (fb_GL_READY),
		.RC_ADDR       (image_RC_ADDR),
		.RC_DATA_WR    (image_RC_DATA_WR),
		.RC_WE         (image_RC_WE),
		.RC_DATA_RD    (rc_data_out1)
	);

	always_comb begin

		// Route memory access
		GL_DONE = 1'b1; // If nothing is running, nothing is blocking
		fb_PAINT_REQ = 1'b0;
		fb_PAINT_X = 10'hXXX;
		fb_PAINT_Y = 10'hXXX;
		fb_PAINT_RGB16 = 16'hXXXX;
		fb_GL_REQ = 1'b0;
		fb_GL_ADDR = 20'hXXXXX;
		fb_GL_X = 10'hXXX;
		fb_GL_Y = 10'hXXX;
		rc_addr1 = 20'hXXXXX;
		rc_data_in1 = 1'bX;
		rc_we1 = 1'b0;

		if(GL_CMD == `GL_CMD_RECT) begin
			rect_en = GL_EXEC;
			GL_DONE = rect_done;
			fb_PAINT_REQ = rect_PAINT_REQ;
			fb_PAINT_X = rect_PAINT_X;
			fb_PAINT_Y = rect_PAINT_Y;
			fb_PAINT_RGB16 = rect_PAINT_RGB16;
			rc_addr1 = rect_RC_ADDR;
			rc_data_in1 = rect_RC_DATA_WR;
			rc_we1 = rect_RC_WE;
		end else begin
			rect_en = 1'b0;
		end

		if(GL_CMD == `GL_CMD_CIRCLE) begin
			circle_en = GL_EXEC;
			GL_DONE = circle_done;
			fb_PAINT_REQ = circle_PAINT_REQ;
			fb_PAINT_X = circle_PAINT_X;
			fb_PAINT_Y = circle_PAINT_Y;
			fb_PAINT_RGB16 = circle_PAINT_RGB16;
			rc_addr1 = circle_RC_ADDR;
			rc_data_in1 = circle_RC_DATA_WR;
			rc_we1 = circle_RC_WE;
		end else begin
			circle_en = 1'b0;
		end

		if(GL_CMD == `GL_CMD_RING) begin
			ring_en = GL_EXEC;
			GL_DONE = ring_done;
			fb_PAINT_REQ = ring_PAINT_REQ;
			fb_PAINT_X = ring_PAINT_X;
			fb_PAINT_Y = ring_PAINT_Y;
			fb_PAINT_RGB16 = ring_PAINT_RGB16;
			rc_addr1 = ring_RC_ADDR;
			rc_data_in1 = ring_RC_DATA_WR;
			rc_we1 = ring_RC_WE;
		end else begin
			ring_en = 1'b0;
		end

		if (GL_CMD == `GL_CMD_POLYGON) begin
			polygon_en = GL_EXEC;
			GL_DONE = polygon_done;
			fb_PAINT_REQ = polygon_PAINT_REQ;
			fb_PAINT_X = polygon_PAINT_X;
			fb_PAINT_Y = polygon_PAINT_Y;
			fb_PAINT_RGB16 = polygon_PAINT_RGB16;
			rc_addr1 = polygon_RC_ADDR;
			rc_data_in1 = polygon_RC_DATA_WR;
			rc_we1 = polygon_RC_WE;
		end else begin
			polygon_en = 1'b0;
		end

		if (GL_CMD == `GL_CMD_IMAGE) begin
			image_en = 1'b1;
			GL_DONE = image_done;
			fb_PAINT_REQ = image_PAINT_REQ;
			fb_PAINT_X = image_PAINT_X;
			fb_PAINT_Y = image_PAINT_Y;
			fb_PAINT_RGB16 = image_PAINT_RGB16;
			fb_GL_REQ = image_GL_REQ;
			fb_GL_ADDR = image_GL_ADDR;
			rc_addr1 = image_RC_ADDR;
			rc_data_in1 = image_RC_DATA_WR;
			rc_we1 = image_RC_WE;
		end else begin
			image_en = 1'b0;
		end

	end
endmodule
