module gl_painter_circle (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[15:0] X0, Y0, R,
	input logic[15:0] C0, C1, CSTROKE,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);
	logic [15:0] paint_x, paint_y;
	logic [15:0] x, y, x_in, y_in, xlen, xlen_in;
	logic [15:0] color, color_in;
	logic paint_req_in;
	enum logic[3:0] {
		s_idle, s_axis, s_calc_xlen, s_paint, s_fin
	} state, state_next;
	logic[2:0] quadrant, quadrant_in;

	assign fb_PAINT_RGB16 = color;
	assign RC_ADDR = {fb_PAINT_X, fb_PAINT_Y[8:0], PAINT_BUFFER};
	assign RC_DATA_WR = 1'b1;
	assign RC_WE = fb_PAINT_REQ;

	always_ff @(posedge CLOCK) begin
		if(RESET | ~EN) begin
			state <= s_idle;
			quadrant <= 3'h0;
			x <= 0;
			y <= 0;
			fb_PAINT_X <= X0;
			fb_PAINT_Y <= Y0;
			fb_PAINT_REQ <= 1'b0;
			color <= C0;
			xlen <= R;
		end else begin
			state <= state_next;
			quadrant <= quadrant_in;
			x <= x_in;
			y <= y_in;
			fb_PAINT_X <= X0 + paint_x;
			fb_PAINT_Y <= Y0 + paint_y;
			fb_PAINT_REQ <= paint_req_in;
			color <= color_in;
			state <= state_next;
			xlen <= xlen_in;
		end
	end

	always_comb begin
		// Default values
		state_next = state;
		quadrant_in = quadrant;
		x_in = x;
		y_in = y;
		xlen_in = xlen;
		color_in = color;
		DONE = 1'b0;
		paint_req_in = 1'b0;

		case (state)
			s_idle: begin
				state_next = s_axis;
				quadrant_in = 3'h0;
				color_in = CSTROKE;
				x_in = -R;
				y_in = 0;
				xlen_in = R;
				paint_req_in = 1'b1;
			end

			s_axis: begin
				paint_req_in = 1'b1;
				if(fb_PAINT_READY) begin
					// Value written, move to next pixel
					x_in = x + 1;
					if (x == R) begin
						// Done. Draw next direction
						x_in = -R;
						y_in = 0;
						quadrant_in = quadrant + 3'h1;
						if (quadrant == 3'h1) begin
							quadrant_in = 3'h0;
							y_in = 1;
							paint_req_in = 1'b0;
							state_next = s_calc_xlen;
						end
					end
					if (x_in == -R || x_in == R)
						color_in = CSTROKE;
					else
						color_in = C0;
					end
			end

			s_calc_xlen: begin
				if ((xlen*xlen + y*y) < R*R) begin
					state_next = s_paint;
					x_in = y;
					paint_req_in = 1'b1;
				end else begin
					xlen_in = xlen - 1;
				end
				// End condition: went beyond 45 deg (has highest priority)
				if (xlen < y) begin
					state_next = s_fin;
				end
			end

			s_paint: begin
				paint_req_in = 1'b1;
				if(fb_PAINT_READY) begin
					// Value written, move to next pixel
					x_in = x + 1;
					if (x == xlen) begin
						// Done. draw next quadrant
						x_in = y;
						quadrant_in = quadrant + 1;
						if (quadrant == 3'h7) begin
							// Drawing finished. Advance Y
							y_in = y + 1;
							state_next = s_calc_xlen;
							paint_req_in = 1'b0;
						end
					end
					if (x_in == xlen)
						color_in = CSTROKE;
					else
						color_in = C0;
				end
			end

			s_fin: begin
				DONE = 1'b1;
			end
		endcase

		// Quadrant coordinate mapping
		unique case (quadrant)
			3'h0: begin
				// Upper-right horizontal
				paint_x = x_in;
				paint_y = y_in;
			end
			3'h1: begin
				// Upper-right vertical
				paint_x = y_in;
				paint_y = x_in;
			end
			3'h2: begin
				// Upper-left vertical
				paint_x = y_in;
				paint_y = -x_in;
			end
			3'h3: begin
				// Upper-left horizontal
				paint_x = -x_in;
				paint_y = y_in;
			end
			3'h4: begin
				// Lower-left horizontal
				paint_x = -x_in;
				paint_y = -y_in;
			end
			3'h5: begin
				// Lower-left vertical
				paint_x = -y_in;
				paint_y = -x_in;
			end
			3'h6: begin
				// Lower-right vertical
				paint_x = -y_in;
				paint_y = x_in;
			end
			3'h7: begin
				// Lower-right horizontal
				paint_x = x_in;
				paint_y = -y_in;
			end
		endcase

	end

endmodule
