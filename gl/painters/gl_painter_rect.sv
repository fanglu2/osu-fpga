module gl_painter_rect (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[9:0] X0, Y0, X1, Y1 /*synthesis keep*/,
	input logic[15:0] C0, C1, CSTROKE,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);

	logic [9:0] x, y, x_in, y_in;
	logic [15:0] color, color_in;
	logic paint_req_in;
	enum logic[3:0] {
		s_idle, s_paint, s_fin
	} state, state_next;

	assign fb_PAINT_X = x;
	assign fb_PAINT_Y = y;
	assign fb_PAINT_RGB16 = color;

	assign RC_ADDR = {x, y[8:0], PAINT_BUFFER};
	assign RC_DATA_WR = 1'b1;
	assign RC_WE = fb_PAINT_REQ;

	always_ff @(posedge CLOCK) begin
		if(RESET | ~EN) begin
			state <= s_idle;
			x <= X0;
			y <= Y0;
			fb_PAINT_REQ <= 1'b0;
			color <= CSTROKE;
		end else begin
			state <= state_next;
			x <= x_in;
			y <= y_in;
			fb_PAINT_REQ <= paint_req_in;
			color <= color_in;
			state <= state_next;
		end
	end

	always_comb begin
		// Default values
		state_next = state;
		x_in = x;
		y_in = y;
		color_in = color;
		DONE = 1'b0;
		paint_req_in = 1'b0;

		case (state)
			s_idle: begin
				state_next = s_paint;
				color_in = CSTROKE;
				x_in = X0;
				y_in = Y0;
				paint_req_in = 1'b1;
			end
			s_paint: begin
				paint_req_in = 1'b1;
				if(fb_PAINT_READY) begin
					// Value written, move to next pixel
					y_in = y + 1;
					if (y == Y1) begin
						y_in = Y0;
						x_in = x + 1;
						if (x == X1) begin
							// Done!!!
							paint_req_in = 1'b0;
							state_next = s_fin;
							DONE = 1'b1;
						end
					end
					if (x_in == X0 || x_in == X1 || y_in == Y0 || y_in == Y1)
						color_in = CSTROKE;
					else
						color_in = C0;
				end
			end
			s_fin: begin
				DONE = 1'b1;
			end
		endcase

	end

endmodule
