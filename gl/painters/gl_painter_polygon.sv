module gl_painter_polygon (
	// Clock
	input logic CLOCK, RESET,

	// Arguments
	input logic[15:0] XT, YT, XB, YB, XL, YL, XR, YR,
	input logic[15:0] C0, C1,
	input logic GRAD_HORIZ,

	// Status Control
	input logic EN,
	output logic DONE,

	input logic PAINT_BUFFER,

	// Memory write connection
	output logic fb_PAINT_REQ,
	output logic [9:0] fb_PAINT_X, fb_PAINT_Y,
	output logic [15:0] fb_PAINT_RGB16,
	input logic fb_PAINT_READY,

	// Redraw cache connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_WR,
	output logic RC_WE,
	input logic RC_DATA_RD
);

	logic [15:0] x, y, x_in, y_in;
	logic paint_req_in;
	enum logic[3:0] {
		s_idle, s_calc_xbeg, s_scan_left, s_scan_right, s_fin
	} state, state_next;

	assign fb_PAINT_RGB16 = C0;
	assign fb_PAINT_X = x;
	assign fb_PAINT_Y = y;

	assign RC_ADDR = {fb_PAINT_X, fb_PAINT_Y[8:0], PAINT_BUFFER};
	assign RC_DATA_WR = 1'b1;
	assign RC_WE = fb_PAINT_REQ;

	// Inclined line scanner instances
	logic [9:0] l_x1, l_y1, l_x2, l_y2, r_x1, r_y1, r_x2, r_y2;
	logic l_side, r_side;
	gl_line_scanner scanner_left(.X({6'h0, x}), .Y({6'h0, y}),
		.SIDE(l_side), .X1({6'h0,l_x1}), .Y1({6'h0,l_y1}),
		.X2({6'h0,l_x2}), .Y2({6'h0,l_y2}), .CLOCK(CLOCK));
	gl_line_scanner scanner_right(.X({6'h0, x}), .Y({6'h0, y}),
		.SIDE(r_side), .X1({6'h0,r_x1}), .Y1({6'h0,r_y1}),
		.X2({6'h0,r_x2}), .Y2({6'h0,r_y2}), .CLOCK(CLOCK));

	// Midpoint tracking
	logic [9:0] l_lim, l_lim_in, r_lim, r_lim_in, xbeg, xbeg_in;
	logic [10:0] midpt_2x;
	assign midpt_2x = {1'b0, l_lim} + {1'b0, r_lim};

	always_ff @(posedge CLOCK) begin
		if(RESET | ~EN) begin
			state <= s_idle;
			x <= XT;
			y <= YT;
			fb_PAINT_REQ <= 1'b0;
			l_lim <= XT;
			r_lim <= XT;
			xbeg <= XT;
		end else begin
			state <= state_next;
			x <= x_in;
			y <= y_in;
			fb_PAINT_REQ <= paint_req_in;
			state <= state_next;
			l_lim <= l_lim_in;
			r_lim <= r_lim_in;
			xbeg <= xbeg_in;
		end
	end

	always_comb begin
		// Default values
		state_next = state;
		x_in = x;
		y_in = y;
		xbeg_in = xbeg;
		DONE = 1'b0;
		paint_req_in = 1'b0;
		l_lim_in = l_lim;
		r_lim_in = r_lim;

		case (state)
			s_idle: begin
				state_next = s_scan_left;
				x_in = XT;
				y_in = YT;
				paint_req_in = 1'b1;
			end

			s_scan_left: begin
				paint_req_in = 1'b1;
				if(fb_PAINT_READY) begin
					// Value written, move to next pixel
					x_in = x - 1;
					l_lim_in = x_in;
					if (~l_side) begin
						// Done. draw right part
						x_in = xbeg + 1;
						state_next = s_scan_right;
					end
				end
			end

			s_scan_right: begin
				paint_req_in = 1'b1;
				if(fb_PAINT_READY) begin
					// Value written, move to next pixel
					x_in = x + 1;
					r_lim_in = x_in;
					if (r_side) begin
						// Done. draw next line
						y_in = y + 1;
						xbeg_in = midpt_2x[10:1];
						x_in = xbeg_in;
						state_next = s_scan_left;
						if (y == YB) begin
							// All finished!
							state_next = s_fin;
						end
					end
				end
			end

			s_fin: begin
				DONE = 1'b1;
			end
		endcase

		// Inclined line scanner segment mapping
		if (y < YL) begin
			// Left part upper segment
			l_x1 = XT;
			l_y1 = YT;
			l_x2 = XL;
			l_y2 = YL;
		end else begin
			// Left part lower segment
			l_x1 = XL;
			l_y1 = YL;
			l_x2 = XB;
			l_y2 = YB;
		end
		if (y < YR) begin
			// Right part upper segment
			r_x1 = XT;
			r_y1 = YT;
			r_x2 = XR;
			r_y2 = YR;
		end else begin
			// Right part lower segment
			r_x1 = XR;
			r_y1 = YR;
			r_x2 = XB;
			r_y2 = YB;
		end
	end

endmodule
