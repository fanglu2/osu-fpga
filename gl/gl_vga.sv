module gl_vga (
	// Clock
	input CLOCK_50, RESET,
	// VGA Signals
	output logic VGA_HS, VGA_VS, VGA_BLANK_N, VGA_SYNC_N, VGA_CLK,
	output logic [7:0] VGA_R, VGA_G, VGA_B,
	// Frame buffer
	output logic FB_REQ,
	output logic [9:0] FB_X, FB_Y,
	input logic [23:0] FB_RET,
	// Informational
	input logic RENDER_BUFFER,
	output logic VGA_INTERFRAME
);

	logic VGA_HS_in, VGA_VS_in, VGA_BLANK_N_in, clk_25;
	logic [7:0] VGA_R_in, VGA_G_in, VGA_B_in;
	logic [9:0] h_counter, v_counter, h_counter_in, v_counter_in;
	logic [9:0] h_plus1, h_plus1_in, v_plus1, v_plus1_in;
	logic [9:0] h_plus2, h_plus2_in, v_plus2, v_plus2_in;


	assign VGA_SYNC_N = 1'b0;
	assign VGA_CLK = clk_25;
	assign FB_X = h_counter;
	assign FB_Y = v_counter;
	assign VGA_INTERFRAME = (v_plus2 > 10'd480);

	// VGA control signals
	always_ff @ (posedge CLOCK_50) begin
		clk_25 <= ~clk_25;
		if (RESET) begin
			VGA_HS <= 1'b0;
			VGA_VS <= 1'b0;
			VGA_BLANK_N <= 1'b0;
			h_counter <= 10'd2;
			v_counter <= 10'd0;
			h_plus1 <= 10'd1;
			v_plus1 <= 10'd0;
			h_plus2 <= 10'd0;
			v_plus2 <= 10'd0;
			VGA_R <= 8'h00;
			VGA_G <= 8'h00;
			VGA_B <= 8'hff;
		end else begin
			VGA_HS <= VGA_HS_in;
			VGA_VS <= VGA_VS_in;
			VGA_BLANK_N <= VGA_BLANK_N_in;
			h_counter <= h_counter_in;
			v_counter <= v_counter_in;
			h_plus1 <= h_plus1_in;
			v_plus1 <= v_plus1_in;
			h_plus2 <= h_plus2_in;
			v_plus2 <= v_plus2_in;
			VGA_R <= VGA_R_in;
			VGA_G <= VGA_G_in;
			VGA_B <= VGA_B_in;
		end
	end

	always_comb begin
		// Default values
		VGA_HS_in = VGA_HS;
		VGA_VS_in = VGA_VS;
		VGA_BLANK_N_in = VGA_BLANK_N;
		h_counter_in = h_counter;
		v_counter_in = v_counter;
		h_plus1_in = h_plus1;
		v_plus1_in = v_plus1;
		h_plus2_in = h_plus2;
		v_plus2_in = v_plus2;
		VGA_R_in = VGA_R;
		VGA_G_in = VGA_G;
		VGA_B_in = VGA_B;
		FB_REQ = 1'b0;

		if (clk_25) begin // clk_25 posegde
			h_plus1_in = h_counter;
			v_plus1_in = v_counter;
			h_plus2_in = h_plus1;
			v_plus2_in = v_plus1;
			// Horizontal sync pulse is 96 pixels long at pixels 656-752
			// (Signal is registered to ensure clean output waveform)
			VGA_HS_in = 1'b1;
			if(h_plus2 >= 10'd656 && h_plus2 < 10'd752)
				VGA_HS_in = 1'b0;
			// Vertical sync pulse is 2 lines (800 pixels each) long at line 490-491
			// (Signal is registered to ensure clean output waveform)
			VGA_VS_in = 1'b1;
			if(v_plus2 >= 10'd490 && v_plus2 < 10'd492)
				VGA_VS_in = 1'b0;
			// Display pixels (inhibit blanking) between horizontal 0-639 and vertical 0-479 (640x480)
			VGA_BLANK_N_in = 1'b0;
			if(h_plus2 < 10'd640 && v_plus2 < 10'd480) begin
				// Display pixels
				VGA_BLANK_N_in = 1'b1;
				VGA_R_in = FB_RET[23:16];
				VGA_G_in = FB_RET[15:8];
				VGA_B_in = FB_RET[7:0];
			end


			if(h_counter < 10'd640 && v_counter < 10'd480) begin
				// Request new pixels
				FB_REQ = 1'b1;
			end

			// Advance counter
			h_counter_in = h_counter + 10'd1;

			if(h_counter == 10'd799) begin
				h_counter_in = 10'd0;
				if(v_counter == 10'd524)
					v_counter_in = 10'd0;
				else
					v_counter_in = v_counter + 10'd1;
			end
		end // Nothing happens on the negative edge
	end

endmodule
