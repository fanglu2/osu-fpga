module gl_blender (
	input logic [15:0] V0, V1, PCT,
	output logic [15:0] V
);

logic [15:0] pct_n;
logic [31:0] comp_0, comp_1;

assign pct_n = 16'hffff - PCT;
assign comp_0 = V0 * pct_n;
assign comp_1 = V1 * PCT;
assign V = comp_0[31:16] + comp_1[31:16];

endmodule
