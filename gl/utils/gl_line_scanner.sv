module gl_line_scanner (
	input logic CLOCK,
	input logic signed [15:0] X1, Y1, X2, Y2,
	input logic signed [15:0] X, Y,
	output logic SIDE
);

logic signed [31:0] prod1, prod2;
assign prod1 = (X-X1) * (Y2-Y1);
assign prod2 = (Y-Y1) * (X2-X1);
always_ff @(posedge CLOCK) begin
	SIDE <= (prod1 - prod2)>0;
end

endmodule
