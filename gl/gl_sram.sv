module gl_sram (
	// Clock
	input logic CLK, RESET,

	// Read 1
	input logic read1_req,
	input logic [19:0] read1_addr,
	output logic [15:0] read1_data,
	output logic read1_ready,

	input logic read2_req,
	input logic [19:0] read2_addr,
	output logic [15:0] read2_data,
	output logic read2_ready,

	// Write
	input logic write1_req,
	input logic [19:0] write1_addr,
	input logic [15:0] write1_data,
	output logic write1_ready,

	input logic write2_req,
	input logic [19:0] write2_addr,
	input logic [15:0] write2_data,
	output logic write2_ready,

	// Export Conduit
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N
);
// Tristate
logic [15:0] SRAM_DQ_WR;
assign SRAM_DQ = (SRAM_CE_N | SRAM_WE_N) ? 16'hZZZZ : SRAM_DQ_WR;
// Enable whole 16 bits
assign SRAM_UB_N = 1'b0;
assign SRAM_LB_N = 1'b0;

// OP1
logic[19:0] OP1_ADDR;
logic[15:0] OP1_DQ_RD_SYNC, OP1_DQ_WR, OP1_DQ_RD;
logic OP1_RD, OP1_WR, OP1_CS;
always_ff @(negedge CLK) begin
	// OP1 operation starts at rising edge and ends at falling edge
	OP1_DQ_RD_SYNC <= SRAM_DQ;
end
always_ff @(posedge CLK) begin
	// DQ need to be sync'ed to the rising edge
	OP1_DQ_RD <= OP1_DQ_RD_SYNC;
end

// OP2
logic[19:0] OP2_ADDR;
logic[15:0] OP2_DQ_RD, OP2_DQ_WR;
logic OP2_RD, OP2_WR, OP2_CS;
always_ff @(posedge CLK) begin
	// OP2 operation starts at falling edge and ends at rising edge
	OP2_DQ_RD <= SRAM_DQ;
end

// Split up clocks
assign SRAM_ADDR = CLK ? OP1_ADDR : OP2_ADDR;
assign SRAM_DQ_WR = CLK ? OP1_DQ_WR : OP2_DQ_WR;
assign SRAM_CE_N = CLK ? ~OP1_CS : ~OP2_CS;
assign SRAM_OE_N = CLK ? ~OP1_RD : ~OP2_RD;
assign SRAM_WE_N = CLK ? ~OP1_WR : ~OP2_WR;

// Output registers
logic read1_ready_next, read2_ready_next, write1_ready_next, write2_ready_next;
assign read1_data = OP1_DQ_RD;
assign read2_data = OP2_DQ_RD;

always_ff @(posedge CLK) begin
	if(RESET) begin
		read1_ready <= 1'b0;
		read2_ready <= 1'b0;
		write1_ready <= 1'b0;
		write2_ready <= 1'b0;
	end else begin
		read1_ready <= read1_ready_next;
		read2_ready <= read2_ready_next;
		write1_ready <= write1_ready_next;
		write2_ready <= write2_ready_next;
	end
end

always_comb begin

	OP1_RD = 1'b0;
	OP1_WR = 1'b0;
	OP1_CS = 1'b0;
	OP1_ADDR = 20'hXXXXX;
	OP1_DQ_WR = 16'hXXXX;
	OP2_RD = 1'b0;
	OP2_WR = 1'b0;
	OP2_CS = 1'b0;
	OP2_ADDR = 20'hXXXXX;
	OP2_DQ_WR = 16'hXXXX;

	read1_ready_next = 1'b0;
	read2_ready_next = 1'b0;
	write1_ready_next = 1'b0;
	write2_ready_next = 1'b0;

	// Priority: Read1 -> Write1 -> Read2 -> Write2
	if (!RESET) begin
		case ({read1_req, write1_req, read2_req, write2_req})
			4'b0001: begin
				// W2
				OP1_CS = 1'b1;
				OP1_WR = 1'b1;
				OP1_ADDR = write2_addr;
				OP1_DQ_WR = write2_data;
				write2_ready_next = 1'b1;
			end
			4'b0010: begin
				// R2
				OP2_CS = 1'b1;
				OP2_RD = 1'b1;
				OP2_ADDR = read2_addr;
				read2_ready_next = 1'b1;
			end
			4'b0011: begin
				// R2, W2
				OP2_CS = 1'b1;
				OP2_RD = 1'b1;
				OP2_ADDR = read2_addr;
				read2_ready_next = 1'b1;
				OP1_CS = 1'b1;
				OP1_WR = 1'b1;
				OP1_ADDR = write2_addr;
				OP1_DQ_WR = write2_data;
				write2_ready_next = 1'b1;
			end
			4'b0100: begin
				// W1
				OP1_CS = 1'b1;
				OP1_WR = 1'b1;
				OP1_ADDR = write1_addr;
				OP1_DQ_WR = write1_data;
				write1_ready_next = 1'b1;
			end
			4'b0101: begin
				// W1, W2
				OP1_CS = 1'b1;
				OP1_WR = 1'b1;
				OP1_ADDR = write1_addr;
				OP1_DQ_WR = write1_data;
				write1_ready_next = 1'b1;
				OP2_CS = 1'b1;
				OP2_WR = 1'b1;
				OP2_ADDR = write2_addr;
				OP2_DQ_WR = write2_data;
				write2_ready_next = 1'b1;
			end
			4'b0110, 4'b0111: begin
				// W1, R2
				OP1_CS = 1'b1;
				OP1_WR = 1'b1;
				OP1_ADDR = write1_addr;
				OP1_DQ_WR = write1_data;
				write1_ready_next = 1'b1;
				OP2_CS = 1'b1;
				OP2_RD = 1'b1;
				OP2_ADDR = read2_addr;
				read2_ready_next = 1'b1;
			end
			4'b1000: begin
				// R1
				OP1_CS = 1'b1;
				OP1_RD = 1'b1;
				OP1_ADDR = read1_addr;
				read1_ready_next = 1'b1;
			end
			4'b1001: begin
				// R1, W2
				OP1_CS = 1'b1;
				OP1_RD = 1'b1;
				OP1_ADDR = read1_addr;
				read1_ready_next = 1'b1;
				OP2_CS = 1'b1;
				OP2_WR = 1'b1;
				OP2_ADDR = write2_addr;
				OP2_DQ_WR = write2_data;
				write2_ready_next = 1'b1;
			end
			4'b1010, 4'b1011: begin
				// R1, R2
				OP1_CS = 1'b1;
				OP1_RD = 1'b1;
				OP1_ADDR = read1_addr;
				read1_ready_next = 1'b1;
				OP2_CS = 1'b1;
				OP2_RD = 1'b1;
				OP2_ADDR = read2_addr;
				read2_ready_next = 1'b1;
			end
			4'b1100, 4'b1101, 4'b1110, 4'b1111: begin
				// R1, W1
				OP1_CS = 1'b1;
				OP1_RD = 1'b1;
				OP1_ADDR = read1_addr;
				OP2_CS = 1'b1;
				OP2_WR = 1'b1;
				OP2_ADDR = write1_addr;
				OP2_DQ_WR = write1_data;
				write1_ready_next = 1'b1;
			end
		endcase
	end
end

endmodule
