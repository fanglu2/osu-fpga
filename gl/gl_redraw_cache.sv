/**
 *	gl_redraw_cache
 *
 *
 */

// Quartus Prime SystemVerilog Template
//
// True Dual-Port RAM with single clock and different data width on the two ports
//
// The first datawidth and the widths of the addresses are specified
// The second data width is equal to DATA_WIDTH1 * RATIO, where RATIO = (1 << (ADDRESS_WIDTH1 - ADDRESS_WIDTH2)
// RATIO must have value that is supported by the memory blocks in your target
// device.  Otherwise, no RAM will be inferred.
//
// Read-during-write behavior returns old data for all combinations of read and
// write on both ports
//
// This style of RAM cannot be used on certain devices, e.g. Stratix V; in that case use the template for Dual-Port RAM with new data on read-during write on the same port

module gl_redraw_cache
	#(parameter int
		DATA_WIDTH = 1,
		ADDRESS_WIDTH = 20
	 ) (
		input [ADDRESS_WIDTH-1:0] addr1,
		input [ADDRESS_WIDTH-1:0] addr2,
		input [DATA_WIDTH-1:0] data_in1,
		input [DATA_WIDTH-1:0] data_in2,
		input we1, we2, clk,
		output reg [DATA_WIDTH-1:0] data_out1,
		output reg [DATA_WIDTH-1:0] data_out2
);

	localparam RAM_DEPTH = 1 << ADDRESS_WIDTH;

	// Use a multi-dimensional packed array to model the different read/ram width
	reg [DATA_WIDTH-1:0] ram[0:RAM_DEPTH-1];

	reg [DATA_WIDTH-1:0] data_reg1;
	reg [DATA_WIDTH-1:0] data_reg2;

	// Port A
	always@(posedge clk)
	begin
		if(we1)
			ram[addr1] <= data_in1;
		data_reg1 <= ram[addr1];
	end
	assign data_out1 = data_reg1;

	// port B
	always@(posedge clk)
	begin
		if(we2)
			ram[addr2] <= data_in2;
		data_reg2 <= ram[addr2];
	end
	assign data_out2 = data_reg2;
endmodule
