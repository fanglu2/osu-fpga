/**
 *	GL frame buffer
 *
 */
module gl_frame_buffer (
	// Clock
	input logic CLK, RESET,

	// VGA Read
	input logic VGA_REQ,
	input logic [9:0] VGA_X, VGA_Y,
	output logic [23:0] VGA_RGB,

	// GL Read
	input logic GL_REQ,
	input logic [9:0] GL_X, GL_Y,
	input logic [19:0] GL_ADDR,
	output logic [15:0] GL_DATA,
	output logic GL_READY,

	// Paint Write
	input logic PAINT_REQ,
	input logic [9:0] PAINT_X, PAINT_Y,
	input logic [15:0] PAINT_RGB16,
	output logic PAINT_READY,

	// Avalon Write
	input logic AVL_REQ,
	input logic [19:0] AVL_ADDR,
	input logic [15:0] AVL_DATA,
	output logic AVL_READY,

	// Frame-switch control
	input logic BUF_ACTIVE, // Active: being drawn, Inactive: being displayed

	// Redraw control connection
	output logic [19:0] RC_ADDR,
	output logic RC_DATA_IN,
	output logic RC_WE,
	input logic RC_DATA_OUT,

	// Palette connection
	output logic [7:0] PL_ADDR,
	input logic [23:0] PL_DATA_OUT,

	// SRAM Connection
	output logic [19:0] SRAM_ADDR,
	inout wire [15:0] SRAM_DQ,
	output logic SRAM_UB_N, SRAM_LB_N, SRAM_CE_N, SRAM_OE_N, SRAM_WE_N
);

	// SRAM instance
	logic read1_req, read2_req, write1_req, write2_req;
	logic [19:0] read1_addr, read2_addr, write1_addr, write2_addr;
	logic [15:0] read1_data, read2_data, write1_data, write2_data /*synthesis keep*/;
	logic read1_ready, read2_ready, write1_ready, write2_ready;

	gl_sram_s sram_inst(.*);

	assign write1_req = PAINT_REQ;
	assign write1_addr = {PAINT_X, PAINT_Y[8:0], BUF_ACTIVE};
	assign write1_data = PAINT_RGB16;
	assign PAINT_READY = write1_ready;

	assign write2_req = AVL_REQ;
	assign write2_addr = AVL_ADDR;
	assign write2_data = AVL_DATA;
	assign AVL_READY = write2_ready;

	// VGA Pipelined continuous read

	logic vga_rc_en, vga_fb_en, vga_pl_en, vga_out_en;
	logic vga_rc_en_next, vga_fb_en_next, vga_pl_en_next, vga_out_en_next;
	// Pipeline step out data
	logic vga_fb_req, vga_fb_req_in;
	logic [19:0] vga_fb_addr, vga_fb_addr_in;
	logic [15:0] vga_fb_data, vga_fb_data_in;
	logic [23:0] vga_pl_data, vga_pl_data_in;
	// Pipeline step coordinates
	logic [19:0] vga_rc_coord, vga_fb_coord, vga_pl_coord;
	logic [19:0] vga_rc_coord_in, vga_fb_coord_in, vga_pl_coord_in;
	// PL usage
	logic vga_from_pl, vga_from_pl_next;
	logic vga_query_pl, vga_query_pl_next;
	// SRAM Cache usage
	logic vga_from_cache, vga_from_cache_next;
	// SRAM 8-bit reads cache
	logic [19:0] vga_fb_prev_addr, vga_fb_prev_addr_in;
	logic [15:0] vga_fb_prev_data, vga_fb_prev_data_in;

	// RC1 wirings
	assign RC_DATA_IN = 1'b0; // In case of write, always reset to background
	// FB read1 wiring
	assign read1_addr = vga_fb_addr;
	assign read1_req = vga_fb_req;

	always_ff @(posedge CLK) begin
		if(RESET) begin
			vga_rc_en <= 1'b0;
			vga_fb_en <= 1'b0;
			vga_pl_en <= 1'b0;
			vga_out_en <= 1'b0;
			vga_fb_req <= 1'b0;
			vga_fb_addr <= 0;
			vga_pl_data <= 0;
			vga_rc_coord <= 0;
			vga_fb_coord <= 0;
			vga_pl_coord <= 0;
			vga_fb_prev_addr <= 20'hFFFFF;
			vga_fb_prev_data <= 0;
			vga_from_pl <= 1'b0;
			vga_from_cache <= 1'b0;
			vga_query_pl <= 1'b0;
		end else begin
			vga_rc_en <= vga_rc_en_next;
			vga_fb_en <= vga_fb_en_next;
			vga_pl_en <= vga_pl_en_next;
			vga_out_en <= vga_out_en_next;
			vga_fb_req <= vga_fb_req_in;
			vga_fb_addr <= vga_fb_addr_in;
			vga_pl_data <= vga_pl_data_in;
			vga_rc_coord <= vga_rc_coord_in;
			vga_fb_coord <= vga_fb_coord_in;
			vga_pl_coord <= vga_pl_coord_in;
			vga_fb_prev_addr <= vga_fb_prev_addr_in;
			vga_fb_prev_data <= vga_fb_prev_data_in;
			vga_from_pl <= vga_from_pl_next;
			vga_from_cache <= vga_from_cache_next;
			vga_query_pl <= vga_query_pl_next;
		end

	end

	// GL read steps
	enum logic [2:0] {
		s_query_rc, s_query_pix, s_query_pl, s_got_pl, s_fin
	} gl_readstate, gl_readstate_next;

	logic gl_fb_req, gl_fb_req_in;
	logic [19:0] gl_fb_addr, gl_fb_addr_in;
	logic [15:0] gl_fb_data, gl_fb_data_in;
	logic [15:0] gl_pl_data, gl_pl_data_in;
	// Pipeline step coordinates
	logic [19:0] gl_rc_coord, gl_fb_coord, gl_pl_coord;
	logic [19:0] gl_rc_coord_in, gl_fb_coord_in, gl_pl_coord_in;
	// PL usage
	logic gl_from_pl, gl_from_pl_next;
	// SRAM 8-bit reads cache
	logic [19:0] gl_fb_prev_addr, gl_fb_prev_addr_in;
	logic [15:0] gl_fb_prev_data, gl_fb_prev_data_in;

	always_ff @(posedge CLK) begin
		if(RESET | !GL_REQ) begin
			gl_readstate <= s_query_rc;
			gl_fb_req <= 1'b0;
			gl_fb_addr <= 19'h00000;
			gl_fb_data <= 16'hCCCC;
			gl_from_pl <= 1'b0;
			gl_pl_data <= 16'hCCCC;
		end else begin
			gl_readstate <= gl_readstate_next;
			gl_fb_req <= gl_fb_req_in;
			gl_fb_addr <= gl_fb_addr_in;
			gl_fb_data <= gl_fb_data_in;
			gl_from_pl <= gl_from_pl_next;
			gl_pl_data <= gl_pl_data_in;
		end
		gl_fb_prev_addr <= gl_fb_prev_addr_in;
		gl_fb_prev_data <= gl_fb_prev_data_in;
	end

	logic rc_avail, pl_avail;

	always_comb begin
		// Queue of shared resources
		rc_avail = 1'b1;
		pl_avail = 1'b1;

		// VGA Combinational logic

		// Pipeline shifting
		vga_rc_en_next = VGA_REQ;
		vga_fb_en_next = vga_rc_en;
		vga_pl_en_next = vga_fb_en;
		vga_out_en_next = vga_pl_en;
		vga_rc_coord_in = {VGA_X, VGA_Y[8:0], ~BUF_ACTIVE};
		vga_fb_coord_in = vga_rc_coord;
		vga_pl_coord_in = vga_fb_coord;

		// Default values
		vga_fb_req_in = 1'b0;
		vga_fb_prev_addr_in = vga_fb_prev_addr;
		vga_fb_prev_data_in = vga_fb_prev_data;
		RC_ADDR = vga_rc_coord_in;
		RC_WE = 1'b0;
		PL_ADDR = 8'hXX;
		vga_from_pl_next = 1'b0;
		vga_from_cache_next = 1'b0;
		vga_query_pl_next = 1'b0;

		// VGA Pipeline

		// Step 1: read RC
		if (VGA_REQ) begin
			rc_avail = 1'b0;
			RC_WE = 1'b1;
		end
		// Step 2: combinationally generate FB address
		if (vga_rc_en) begin
			vga_fb_req_in = 1'b1;
			if (RC_DATA_OUT) begin
				// Read from drawn pixels
				vga_fb_addr_in = vga_rc_coord;
			end else begin
				// Read from background
				vga_fb_addr_in = {4'hA+{2'b0,vga_rc_coord[19:18]},
					vga_rc_coord[17:11], // X without last bit
					vga_rc_coord[9:1]
				};
				vga_fb_prev_addr_in = vga_fb_addr;
				if (vga_fb_addr_in == vga_fb_prev_addr) begin
					// The next addr to be read is the same as the current one
					vga_fb_req_in = 1'b0;
				end
			end
		end else begin
			vga_fb_addr_in = 20'hXXXXX;
			vga_fb_req_in = 1'b0;
		end
		// Step 3: read SRAM
		if (vga_fb_en) begin
			if (~vga_fb_req) begin
				// This should only happen when cached read occurs
				// Reading from the next adjacent location in background
				// vga_fb_data_in = vga_fb_prev_data;
				vga_from_cache_next = 1'b1;
			end else begin
				// vga_fb_data_in = read1_data;
				vga_from_cache_next = 1'b0;
				// vga_fb_prev_data_in = read1_data;
			end
			if (vga_fb_addr[19:16] >= 4'hA) begin
				vga_query_pl_next = 1'b1;
			end
		end
		// Step 4: read palette
		if (vga_pl_en) begin
			if (vga_from_cache) begin
				vga_fb_data = vga_fb_prev_data;
			end else begin
				vga_fb_data = read1_data;
				vga_fb_prev_data_in = read1_data;
			end

			if (vga_query_pl) begin
				// A palette access
				pl_avail = 1'b0;
				if (vga_pl_coord[10]) begin
					// Upper bits
					PL_ADDR = vga_fb_data[15:8];
				end else begin
					// Lower bits
					PL_ADDR = vga_fb_data[7:0];
				end
				vga_pl_data_in = 24'hff0000;
				vga_from_pl_next = 1'b1;
			end else begin
				// Keep data from memory
				// Up-sample RGB16 to RGB24: prioritize web-safe colors
				// E.g., 0xabc will be up-sampled to 0xaabbcc
				vga_pl_data_in = {vga_fb_data[15:11], vga_fb_data[14:12],
								  vga_fb_data[10:5],  vga_fb_data[8:7],
								  vga_fb_data[4:0],   vga_fb_data[3:1]};
			end
		end else begin
			vga_fb_data = 16'hXXXX;
			vga_pl_data_in = vga_pl_data;
		end

		// End: assign output to either SRAM output or PL output
		if (vga_out_en) begin
			if (vga_from_pl) begin
				VGA_RGB = PL_DATA_OUT;
			end else begin
				VGA_RGB = vga_pl_data;
			end
		end else
			VGA_RGB = 24'h0000ff;

		// GL Combinational logic

		// Default values
		gl_readstate_next = gl_readstate;
		gl_fb_req_in = 1'b0;
		gl_fb_addr_in = gl_fb_addr;
		gl_fb_data_in = gl_fb_data;
		gl_from_pl_next = gl_from_pl;
		gl_pl_data_in = gl_pl_data;
		gl_fb_prev_data_in = gl_fb_prev_data;
		gl_fb_prev_addr_in = gl_fb_prev_addr;

		GL_DATA = 16'hXXXX;
		GL_READY = 1'b0;

		case (gl_readstate)
			s_query_rc: begin
				if (~GL_ADDR[19] && rc_avail) begin
					RC_ADDR = {GL_X, GL_Y[8:0], BUF_ACTIVE};
					gl_readstate_next = s_query_pix;
				end
			end

			s_query_pix: begin
				if (!gl_fb_data) begin
					if (RC_DATA_OUT) begin
						// Read from drawn pixels
						gl_fb_addr_in = {GL_X, GL_Y[8:0], BUF_ACTIVE};
						gl_from_pl_next = 1'b0;
					end else begin
						// Read from background
						gl_fb_addr_in =	{4'hA+{2'b0,GL_X[9:8]},
										 GL_X[7:1], // X without last bit
										 GL_Y[8:0]};

						gl_from_pl_next = 1'b1;
					end
					if (gl_fb_addr_in == gl_fb_prev_addr) begin
						gl_fb_data_in = gl_fb_prev_data;
						gl_readstate_next = s_query_pl;
					end
				end
				gl_fb_req_in = 1'b1;
				if (read2_ready) begin
					gl_fb_data_in = read2_data;
					gl_readstate_next = s_query_pl;
				end
			end

			s_query_pl: begin
				gl_fb_prev_addr_in = gl_fb_addr;
				gl_fb_prev_data_in = gl_fb_data;

				if (gl_fb_addr[19:17] > 3'b100) begin
					// Need to lookup palette
					if (pl_avail) begin
						if (GL_X[0])
							PL_ADDR = vga_fb_data[15:8];
						else
							PL_ADDR = vga_fb_data[7:0];
						gl_readstate_next = s_query_pl;
					end
				end else begin
					// Done, just return
					GL_DATA = gl_fb_data;
					GL_READY = 1'b1;
					gl_readstate_next = s_fin;
				end
			end

			s_got_pl: begin
				gl_pl_data_in = {PL_DATA_OUT[23:19],
								 PL_DATA_OUT[15:10],
								 PL_DATA_OUT[7:3]};
				GL_DATA = gl_pl_data_in;
				gl_readstate_next = s_fin;
			end

			s_fin: begin
				GL_READY = 1'b1;
				if (gl_from_pl)
					GL_DATA = gl_pl_data;
				else
					GL_DATA = gl_fb_data;

				// Will leave this state on state-machine reset by falling REQ
			end
		endcase

		// Wiring
		if (GL_ADDR[19]) begin
			// Direct read from data region
			read2_req = GL_REQ;
			read2_addr = GL_ADDR;
			GL_READY = read2_ready;
			GL_DATA = read2_data;
		end else begin
			read2_req = gl_fb_req;
			read2_addr = gl_fb_addr;
		end

	end

endmodule
